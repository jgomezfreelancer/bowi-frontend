interface IServer {
  url: string;
}

interface IEnv {
  server: IServer;
}

export const ENV: IEnv = {
  server: { url: '' },
};
