import { IEnv } from './IEnv';

export const ENV: IEnv = {
  server: { url: '' },
  firebase: {
    apiKey: '',
    authDomain: '',
    databaseURL: '',
    projectId: '',
    storageBucket: '',
    messagingSenderId: '',
    appId: '',
  },
};
