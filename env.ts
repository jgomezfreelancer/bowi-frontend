import { IEnv } from './IEnv';

// const host = 'http://51.83.79.236:8000'; // dev
// const host = 'http://51.210.254.240:8000'; // prod
// const host = 'http://146.59.228.111:8000'; // qa
// const host = 'http:/localhost:8000';       // local

export const ENV: IEnv = {
    server: { url: 'http://146.59.228.111:8001' },
    firebase: {
        apiKey: 'AIzaSyCfjd0jJEbBcGHhJ7y1PxpiKW0rGGyL_vM',
        authDomain: 'fapp-bowi-e025b.firebaseapp.com',
        databaseURL: 'https://app-bowi-e025b.firebaseio.com',
        projectId: 'bowi-e025b',
        storageBucket: 'app-bowi-e025b.appspot.com',
        messagingSenderId: '1077134417856',
        appId: '1:1077134417856:android:8b95ee10cc5aa764633225',
    },
    websocket: {
        chat: {
            room: 'http://146.59.228.111:8001/chat-room',
            alert: 'http://146.59.228.111:8001/chat-alert',
        },
    },
};