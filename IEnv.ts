interface IServer {
  url: string;
}

interface IFirebase {
  apiKey: string;
  authDomain: string;
  databaseURL: string;
  projectId: string;
  storageBucket: string;
  messagingSenderId: string;
  appId: string;
}

interface IChat {
  room: string;
  alert: string;
}

interface IWebSocket {
  chat: IChat;
}

export interface IEnv {
  server: IServer;
  firebase: IFirebase;
  websocket: IWebSocket;
}
