import React from 'react';
import { View, Image, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Styles from './style';

import AllIcon from '../../assets/icons/icono-intereses-02.svg';
import Leisure from '../../assets/icons/icono-intereses-03.svg';
import Conversation from '../../assets/icons/icono-intereses-04.svg';
import LaborExecutive from '../../assets/icons/icono-intereses-05.svg';
import Animals from '../../assets/icons/icono-intereses-06.svg';
import Community from '../../assets/icons/icono-intereses-07.svg';
import Covid19 from '../../assets/icons/icono-intereses-08.svg';
import FunctionalDiversity from '../../assets/icons/icono-intereses-09.svg';
import Displacement from '../../assets/icons/icono-intereses-10.svg';
import EmotionalSupport from '../../assets/icons/icono-intereses-17.svg';
import LegalAdministrative from '../../assets/icons/icono-intereses-16.svg';
import Accommodation from '../../assets/icons/icono-intereses-15.svg';
import Family from '../../assets/icons/icono-intereses-14.svg';
import Sanitary from '../../assets/icons/icono-intereses-13.svg';
import Losses from '../../assets/icons/icono-intereses-12.svg';
import Others from '../../assets/icons/icono-intereses-11.svg';

interface IProps {
  name: string;
  icon?: boolean;
}

function FlagInterestLogo(props: IProps): JSX.Element {
  const { name, icon } = props;

  const styles = StyleSheet.create({
    container: {
      marginLeft: 10,
    }
  });
  
  if(name == 'ocio'){
    return (
      <View style={styles.container}>
        <Leisure width={20} height={20} />
      </View>)
  }
  if(name == 'conversacion'){
    return (
      <View style={styles.container}>
        <Conversation width={20} height={20} />
      </View>)
  }
  if(name == 'laboralYEducativo'){
    return (
      <View style={styles.container}>
        <Others width={20} height={20} />
        {/* <LaborExecutive width={20} height={20} /> */}
      </View>)
  }
  if(name == 'animales'){
    return (
      <View style={styles.container}>
        <Others width={20} height={20} />
        {/* <Animals width={20} height={20} /> */}
      </View>)
  }
  if(name == 'comunitaria'){
    return (
      <View style={styles.container}>
        <Community width={20} height={20} />
      </View>)
  }
  if(name == 'covid19'){
    return (
      <View style={styles.container}>
        <Covid19 width={20} height={20} />
      </View>)
  }
  if(name == 'diversidadFuncional'){
    return (
      <View style={styles.container}>
        <FunctionalDiversity width={20} height={20} />
      </View>)
  }
  if(name == 'desplazamiento'){
    return (
      <View style={styles.container}>
        <Displacement width={20} height={20} />
      </View>)
  }
  if(name == 'apoyoEmocional'){
    return (
      <View style={styles.container}>
        <Others width={20} height={20} />
        {/* <EmotionalSupport width={20} height={20} /> */}
      </View>)
  }
  if(name == 'legalYAdministrativo'){
    return (
      <View style={styles.container}>
        <Others width={20} height={20} />
        {/* <LegalAdministrative width={20} height={20} /> */}
      </View>)
  }
  if(name == 'alojamiento'){
    return (
      <View style={styles.container}>
        <Accommodation width={20} height={20} />
      </View>)
  }
  if(name == 'familiar'){
    return (
      <View style={styles.container}>
        <Family width={20} height={20} />
      </View>)
  }
  if(name == 'sanitario'){
    return (
      <View style={styles.container}>
        <Sanitary width={20} height={20} />
      </View>)
  }
  if(name == 'perdidas'){
    return (
      <View style={styles.container}>
        <Losses width={20} height={20} />
      </View>)
  }
  if(name == 'otros'){
    return (
      <View style={styles.container}>
        <Others width={20} height={20} />
      </View>)
  }
  else{
    return (
      <View style={styles.container}>
        <Others width={20} height={20} />
        {/* <AllIcon width={20} height={20} /> */}
      </View>)
  }
}

export default FlagInterestLogo;
