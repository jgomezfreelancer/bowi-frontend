import React from 'react';
import { View, Image } from 'react-native';

const logoImage = require('../../assets/images/logo.png');

function Logo(): JSX.Element {
  return (
    <View>
      <Image source={logoImage} width={50} height={50} />
    </View>
  );
}

export default Logo;
