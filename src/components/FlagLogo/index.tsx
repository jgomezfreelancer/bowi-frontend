import React from 'react';
import { View, Image, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

interface IProps {
  name: string;
  icon?: boolean;
}

function FlagLogo(props: IProps): JSX.Element {
  const { name, icon } = props;

  const pathFlag = '../../assets/icons/flagsN/';
  /*
  const getIcon = (slug: string) => {
    switch (slug) {
      case 'paraguay':
        return require(pathFlag + '001-paraguay.png');
      case 'senegal':
        return require(pathFlag + '002-senegal.png');
      case 'congo':
        return require(pathFlag + '003-democratic-republic-of-congo.png');
      case 'estonia':
        return require(pathFlag + '004-estonia.png');
      case 'georgia':
        return require(pathFlag + '005-georgia.png');
      case 'ghana':
        return require(pathFlag + '006-ghana.png');
      case 'jersey':
        return require(pathFlag + '007-jersey.png');
      case 'madagascar':
        return require(pathFlag + '008-madagascar.png');
      case 'malawi':
        return require(pathFlag + '009-malawi.png');
      case 'mongolia':
        return require(pathFlag + '010-mongolia.png');
      case 'china':
        return require(pathFlag + '011-china.png');
      case 'nepal':
        return require(pathFlag + '012-nepal.png');
      case 'samoa':
        return require(pathFlag + '013-samoa.png');
      case 'haiti':
        return require(pathFlag + '014-haiti.png');
      case 'islaMan':
        return require(pathFlag + '015-isle-of-man.png');
      case 'costaMarfil':
        return require(pathFlag + '016-ivory-coast.png');
      case 'lesoto':
        return require(pathFlag + '017-lesotho.png');
      case 'ruanda':
        return require(pathFlag + '018-rwanda.png');
      case 'zimbabue':
        return require(pathFlag + '019-zimbabwe.png');
      case 'guineaEcuatorial':
        return require(pathFlag + '020-equatorial-guinea.png');
      case 'congoRepublica':
        return require(pathFlag + '021-republic-of-the-congo.png');
      case 'brasil':
        return require(pathFlag + '022-brazil.png');
      case 'sriLanka':
        return require(pathFlag + '023-sri-lanka.png');
      case 'suazilandia':
        return require(pathFlag + '024-swaziland.png');
      case 'tayikistan':
        return require(pathFlag + '025-tajikistan.png');
      case 'trinidadTobago':
        return require(pathFlag + '026-trinidad-and-tobago.png');
      case 'polinesiaFrancesa':
        return require(pathFlag + '027-french-polynesia.png');
      case 'madeira':
        return require(pathFlag + '028-madeira.png');
      case 'maldivas':
        return require(pathFlag + '029-maldives.png');
      case 'moldavia':
        return require(pathFlag + '030-moldova.png');
      case 'montenegro':
        return require(pathFlag + '031-montenegro.png');
      case 'papua':
        return require(pathFlag + '032-papua-new-guinea.png');
      case 'mexico':
        return require(pathFlag + '033-mexico.png');
      case 'tibet':
        return require(pathFlag + '034-tibet.png');
      case 'timorOriental':
        return require(pathFlag + '035-East-Timor.png');
      case 'libia':
        return require(pathFlag + '036-libya.png');
      case 'macedonia':
        return require(pathFlag + '038-republic-of-macedonia.png');
      case 'arabeSaharaui':
        return require(pathFlag + '039-sahrawi-arab-democratic-republic.png');
      case 'sudan':
        return require(pathFlag + '040-sudan.png');
      case 'sudan':
        return require(pathFlag + '040-sudan.png');
      case 'uzbekistan':
        return require(pathFlag + '041-uzbekistán.png');
      case 'yemen':
        return require(pathFlag + '042-yemen.png');
      case 'liberia':
        return require(pathFlag + '043-liberia.png');
      case 'rusia':
        return require(pathFlag + '044-russia.png');
      case 'liechtenstein':
        return require(pathFlag + '045-liechtenstein.png');
      case 'norteChipre':
        return require(pathFlag + '046-northern-cyprus.png');
      case 'sanMarino':
        return require(pathFlag + '047-san-marino.png');
      case 'sanBartolome':
        return require(pathFlag + '048-st-barts.png');
      case 'curazao':
        return require(pathFlag + '049-curacao.png');
      case 'yibuti':
        return require(pathFlag + '050-djibouti.png');
      case 'eritrea':
        return require(pathFlag + '051-eritrea.png');
      case 'guineaBasau':
        return require(pathFlag + '052-guinea-bissau.png');
      case 'kirguistan':
        return require(pathFlag + '053-kyrgyzstan.png');
      case 'mali':
        return require(pathFlag + '054-mali.png');
      case 'india':
        return require(pathFlag + '055-india.png');
      case 'islaMarshall':
        return require(pathFlag + '056-marshall-island.png');
      case 'micronesia':
        return require(pathFlag + '057-micronesia.png');
      case 'niger':
        return require(pathFlag + '058-niger.png');
      case 'islaNorfolk':
        return require(pathFlag + '059-norfolk-island.png');
      case 'santoTomePrincipe':
        return require(pathFlag + '060-sao-tome-and-prince.png');
      case 'sardinia':
        return require(pathFlag + '061-sardinia.png');
      case 'butan':
        return require(pathFlag + '062-bhutan.png');
      case 'somalia':
        return require(pathFlag + '063-somalia.png');
      case 'surinam':
        return require(pathFlag + '064-suriname.png');
      case 'islasAzores':
        return require(pathFlag + '065-azores-islands.png');
      case 'canada':
        return require(pathFlag + '066-canada.png');
      case 'bahamas':
        return require(pathFlag + '067-bahamas.png');
      case 'togo':
        return require(pathFlag + '068-togo.png');
      case 'turkmenistan':
        return require(pathFlag + '069-turkmenistan.png');
      case 'tuvalu':
        return require(pathFlag + '070-tuvalu.png');
      case 'vaticano':
        return require(pathFlag + '071-vatican-city.png');
      case 'austria':
        return require(pathFlag + '072-austria.png');
      case 'islasVirgenes':
        return require(pathFlag + '073-virgin-islands.png');
      case 'islasMalvinas':
        return require(pathFlag + '074-falkland-islands.png');
      case 'gabon':
        return require(pathFlag + '075-gabon.png');
      case 'gibraltar':
        return require(pathFlag + '076-gibraltar.png');
      case 'paisesBajos':
        return require(pathFlag + '077-netherlands.png');
      case 'groenlandia':
        return require(pathFlag + '078-greenland.png');
      case 'granada':
        return require(pathFlag + '079-grenada.png');
      case 'guam':
        return require(pathFlag + '080-guam.png');
      case 'guernsey':
        return require(pathFlag + '081-guernsey.png');
      case 'guyana':
        return require(pathFlag + '082-guyana.png');
      case 'kiribati':
        return require(pathFlag + '083-kiribati.png');
      case 'kuwait':
        return require(pathFlag + '084-kuwait.png');
      case 'mauritania':
        return require(pathFlag + '085-mauritania.png');
      case 'melilla':
        return require(pathFlag + '086-melilla.png');
      case 'montserrat':
        return require(pathFlag + '087-montserrat.png');
      case 'tailandia':
        return require(pathFlag + '088-thailand.png');
      case 'nauru':
        return require(pathFlag + '089-nauru.png');
      case 'islasMarianasNorte':
        return require(pathFlag + '090-northern-marianas-islands.png');
      case 'osetia':
        return require(pathFlag + '091-ossetia.png');
      case 'palestina':
        return require(pathFlag + '092-palestine.png');
      case 'islasPitcairn':
        return require(pathFlag + '093-pitcairn-islands.png');
      case 'saba':
        return require(pathFlag + '094-saba-island.png');
      case 'sanCristobalNieves':
        return require(pathFlag + '095-saint-kitts-and-nevis.png');
      case 'sanEustaquio':
        return require(pathFlag + '096-sint-eustatius.png');
      case 'sanMartin':
        return require(pathFlag + '097-sint-maarten.png');
      case 'portugal':
        return require(pathFlag + '098-portugal.png');
      case 'islasSalomon':
        return require(pathFlag + '099-solomon-islands.png');
      case 'somalilandia':
        return require(pathFlag + '100-somaliland.png');
      case 'sudanSur':
        return require(pathFlag + '101-south-sudan.png');
      case 'sanVicenteGranadinas':
        return require(pathFlag + '102-st-vincent-and-the-grenadines.png');
      case 'gambia':
        return require(pathFlag + '103-gambia.png');
      case 'tokelau':
        return require(pathFlag + '104-tokelau.png');
      case 'transnistria':
        return require(pathFlag + '105-transnistria.png');
      case 'islasTurcasCaicos':
        return require(pathFlag + '106-turks-and-caicos.png');
      case 'islasOrcadas':
        return require(pathFlag + '107-orkney-islands.png');
      case 'guinea':
        return require(pathFlag + '108-guinea.png');
      case 'vietnam':
        return require(pathFlag + '109-vietnam.png');
      case 'reino-unido':
        return require(pathFlag + '110-united-kingdom.png');
      case 'kosovo':
        return require(pathFlag + '111-kosovo.png');
      case 'seychelles':
        return require(pathFlag + '112-seychelles.png');
      case 'sierraLeona':
        return require(pathFlag + '113-sierra-leone.png');
      case 'vanuatu':
        return require(pathFlag + '114-vanuatu.png');
      case 'dominica':
        return require(pathFlag + '115-dominica.png');
      case 'islasFaroe':
        return require(pathFlag + '116-faroe-islands.png');
      case 'fiji':
        return require(pathFlag + '117-fiji.png');
      case 'macao':
        return require(pathFlag + '118-macao.png');
      case 'niue':
        return require(pathFlag + '119-niue.png');
      case 'palaos':
        return require(pathFlag + '120-palau.png');
      case 'inglaterra':
        return require(pathFlag + '121-england.png');
      case 'islaPascua':
        return require(pathFlag + '122-Rapa-Nui.png');
      case 'santaLucia':
        return require(pathFlag + '123-st-lucia.png');
      case 'tonga':
        return require(pathFlag + '124-tonga.png');
      case 'martinica':
        return require(pathFlag + '125-martinique.png');
      case 'islasGalapagos':
        return require(pathFlag + '126-galapagos-islands.png');
      case 'bulgaria':
        return require(pathFlag + '127-bulgaria.png');
      case 'croacia':
        return require(pathFlag + '128-croatia.png');
      case 'bolivia':
        return require(pathFlag + '129-bolivia.png');
      case 'camboya':
        return require(pathFlag + '130-cambodia.png');
      case 'costaRica':
        return require(pathFlag + '131-costa-rica.png');
      case 'singapur':
        return require(pathFlag + '132-singapore.png');
      case 'cuba':
        return require(pathFlag + '133-cuba.png');
      case 'bangladesh':
        return require(pathFlag + '134-bangladesh.png');
      case 'botsuana':
        return require(pathFlag + '135-botswana.png');
      case 'argelia':
        return require(pathFlag + '136-Algeria.png');
      case 'azerbaiyan':
        return require(pathFlag + '137-azerbaijan.png');
      case 'angola':
        return require(pathFlag + '138-angola.png');
      case 'columbiaBritanica':
        return require(pathFlag + '139-british-columbia.png');
      case 'afganistan':
        return require(pathFlag + '140-afghanistan.png');
      case 'armenia':
        return require(pathFlag + '141-armenia.png');
      case 'paisVasco':
        return require(pathFlag + '142-basque-country.png');
      case 'australia':
        return require(pathFlag + '143-australia.png');
      case 'burkinaFaso':
        return require(pathFlag + '144-burkina-faso.png');
      case 'albania':
        return require(pathFlag + '145-albania.png');
      case 'barein':
        return require(pathFlag + '146-bahrain.png');
      case 'belice':
        return require(pathFlag + '147-belize.png');
      case 'bermudas':
        return require(pathFlag + '148-bermuda.png');
      case 'bosniaHerzegovina':
        return require(pathFlag + '149-bosnia-and-herzegovina.png');
      case 'islasVirgenesBritanicas':
        return require(pathFlag + '150-british-virgin-islands.png');
      case 'barbados':
        return require(pathFlag + '151-barbados.png');
      case 'bielorrusia':
        return require(pathFlag + '152-belarus.png');
      case 'bonaire':
        return require(pathFlag + '153-bonaire.png');
      case 'turquia':
        return require(pathFlag + '154-turkey.png');
      case 'brunei':
        return require(pathFlag + '155-brunei.png');
      case 'camerun':
        return require(pathFlag + '156-cameroon.png');
      case 'islasAland':
        return require(pathFlag + '157-aland-islands.png');
      case 'antiguaBarbuda':
        return require(pathFlag + '158-antigua-and-barbuda.png');
      case 'burundi':
        return require(pathFlag + '159-burundi.png');
      case 'andorra':
        return require(pathFlag + '160-andorra.png');
      case 'islasBaleares':
        return require(pathFlag + '161-balearic-islands.png');
      case 'abjasia':
        return require(pathFlag + '162-abkhazia.png');
      case 'aruba':
        return require(pathFlag + '163-aruba.png');
      case 'benin':
        return require(pathFlag + '164-benin.png');
      case 'polonia':
        return require(pathFlag + '165-poland.png');
      case 'caboVerde':
        return require(pathFlag + '166-cape-verde.png');
      case 'chad':
        return require(pathFlag + '167-chad.png');
      case 'territorioBritanicoOceanoIndico':
        return require(pathFlag + '169-british-indian-ocean-territory.png');
      case 'islasCaiman':
        return require(pathFlag + '170-cayman-islands.png');
      case 'republicaCentroafricana':
        return require(pathFlag + '171-central-african-republic.png');
      case 'comoras':
        return require(pathFlag + '172-comoros.png');
      case 'islasCanarias':
        return require(pathFlag + '173-canary-islands.png');
      case 'corcega':
        return require(pathFlag + '174-corsica.png');
      case 'anguila':
        return require(pathFlag + '175-anguilla.png');
      case 'suiza':
        return require(pathFlag + '176-switzerland.png');
      case 'samoaAmericana':
        return require(pathFlag + '177-american-samoa.png');
      case 'ceuta':
        return require(pathFlag + '178-ceuta.png');
      case 'islaNavidad':
        return require(pathFlag + '179-christmas-island.png');
      case 'islasCocos':
        return require(pathFlag + '180-cocos-island.png');
      case 'islasCook':
        return require(pathFlag + '181-cook-islands.png');
      case 'marruecos':
        return require(pathFlag + '182-morocco.png');
      case 'taiwan':
        return require(pathFlag + '183-taiwan.png');
      case 'egipto':
        return require(pathFlag + '184-egypt.png');
      case 'indonesia':
        return require(pathFlag + '185-indonesia.png');
      case 'estadosUnidos':
        return require(pathFlag + '186-united-states.png');
      case 'filipinas':
        return require(pathFlag + '187-philippines.png');
      case 'surAfrica':
        return require(pathFlag + '188-south-africa.png');
      case 'peru':
        return require(pathFlag + '189-peru.png');
      case 'suecia':
        return require(pathFlag + '190-sweden.png');
      case 'dinamarca':
        return require(pathFlag + '191-denmark.png');
      case 'grecia':
        return require(pathFlag + '192-greece.png');
      case 'irlanda':
        return require(pathFlag + '193-ireland.png');
      case 'laos':
        return require(pathFlag + '194-laos.png');
      case 'emiratosArabesUnidos':
        return require(pathFlag + '195-united-arab-emirates.png');
      case 'hongKong':
        return require(pathFlag + '196-hong-kong.png');
      case 'francia':
        return require(pathFlag + '197-france.png');
      case 'ukrania':
        return require(pathFlag + '198-ukraine.png');
      case 'argentina':
        return require(pathFlag + '199-argentina.png');
      case 'iran':
        return require(pathFlag + '200-iran.png');
      case 'colombia':
        return require(pathFlag + '201-colombia.png');
      case 'republicaCheca':
        return require(pathFlag + '202-czech-republic.png');
      case 'israel':
        return require(pathFlag + '203-israel.png');
      case 'arabiaSaudita':
        return require(pathFlag + '204-saudi-arabia.png');
      case 'noruega':
        return require(pathFlag + '205-norway.png');
      case 'venezuela':
        return require(pathFlag + '206-venezuela.png');
      case 'malasia':
        return require(pathFlag + '207-malaysia.png');
      case 'alemania':
        return require(pathFlag + '208-germany.png');
      case 'belgica':
        return require(pathFlag + '209-belgium.png');
      case 'hungria':
        return require(pathFlag + '210-hungary.png');
      case 'finlandia':
        return require(pathFlag + '211-finland.png');
      case 'chile':
        return require(pathFlag + '212-chile.png');
      case 'rumania':
        return require(pathFlag + '213-romania.png');
      case 'ecuador':
        return require(pathFlag + '214-ecuador.png');
      case 'nuevaZelanda':
        return require(pathFlag + '215-new-zealand.png');
      case 'panama':
        return require(pathFlag + '216-panama.png');
      case 'eslovaquia':
        return require(pathFlag + '218-slovakia.png');
      case 'coreaSur':
        return require(pathFlag + '219-south-korea.png');
      case 'monaco':
        return require(pathFlag + '220-monaco.png');
      case 'jordania':
        return require(pathFlag + '221-jordan.png');
      case 'islandia':
        return require(pathFlag + '222-iceland.png');
      case 'guatemala':
        return require(pathFlag + '223-guatemala.png');
      case 'siria':
        return require(pathFlag + '224-syria.png');
      case 'uruguay':
        return require(pathFlag + '225-uruguay.png');
      case 'coreaNorte':
        return require(pathFlag + '226-north-korea.png');
      case 'escocia':
        return require(pathFlag + '227-scotland.png');
      case 'serbia':
        return require(pathFlag + '228-serbia.png');
      case 'kazajistan':
        return require(pathFlag + '229-kazakhstan.png');
      case 'espana':
        return require(pathFlag + '230-spain.png');
      case 'letonia':
        return require(pathFlag + '231-latvia.png');
      case 'pakistan':
        return require(pathFlag + '232-pakistan.png');
      case 'chipre':
        return require(pathFlag + '233-cyprus.png');
      case 'kenia':
        return require(pathFlag + '234-kenya.png');
      case 'myanmar':
        return require(pathFlag + '235-myanmar.png');
      case 'honduras':
        return require(pathFlag + '236-honduras.png');
      case 'jamaica':
        return require(pathFlag + '237-jamaica.png');
      case 'lituania':
        return require(pathFlag + '238-lithuania.png');
      case 'hawaii':
        return require(pathFlag + '239-hawaii.png');
      case 'luxemburgo':
        return require(pathFlag + '240-luxembourg.png');
      case 'japon':
        return require(pathFlag + '241-japan.png');
      case 'mauricio':
        return require(pathFlag + '242-mauritius.png');
      case 'namibia':
        return require(pathFlag + '243-namibia.png');
      case 'qatar':
        return require(pathFlag + '244-qatar.png');
      case 'republicaDominicana':
        return require(pathFlag + '245-dominican-republic.png');
      case 'gales':
        return require(pathFlag + '246-wales.png');
      case 'zambia':
        return require(pathFlag + '247-zambia.png');
      case 'elSalvador':
        return require(pathFlag + '248-el-salvador.png');
      case 'nicaragua':
        return require(pathFlag + '249-nicaragua.png');
      case 'tunez':
        return require(pathFlag + '250-tunisia.png');
      case 'malta':
        return require(pathFlag + '251-malta.png');
      case 'nigeria':
        return require(pathFlag + '253-nigeria.png');
      case 'uganda':
        return require(pathFlag + '254-uganda.png');
      case 'libano':
        return require(pathFlag + '255-lebanon.png');
      case 'iraq':
        return require(pathFlag + '256-iraq.png');
      case 'mozambique':
        return require(pathFlag + '257-mozambique.png');
      case 'puertoRico':
        return require(pathFlag + '258-puerto-rico.png');
      case 'eslovenia':
        return require(pathFlag + '259-slovenia.png');
      case 'tanzania':
        return require(pathFlag + '260-tanzania.png');
      case 'oman':
        return require(pathFlag + '261-oman.png');
      case 'etiopia':
        return require(pathFlag + '262-ethiopia.png');
      case 'italia':
        return require(pathFlag + '263-italy.png');
  
        

    }
  };
  */
  //const flag = getIcon(name);
  const flags = {
    paraguay: require(pathFlag + '001-paraguay.png'),
    senegal: require(pathFlag + '002-senegal.png'),
    congo: require(pathFlag + '003-democratic-republic-of-congo.png'),
    estonia: require(pathFlag + '004-estonia.png'),
    georgia: require(pathFlag + '005-georgia.png'),
    ghana: require(pathFlag + '006-ghana.png'),
    jersey: require(pathFlag + '007-jersey.png'),
    madagascar: require(pathFlag + '008-madagascar.png'),
    malawi: require(pathFlag + '009-malawi.png'),
    mongolia: require(pathFlag + '010-mongolia.png'),
    china: require(pathFlag + '011-china.png'),
    nepal: require(pathFlag + '012-nepal.png'),
    samoa: require(pathFlag + '013-samoa.png'),
    haiti: require(pathFlag + '014-haiti.png'),
    islaMan: require(pathFlag + '015-isle-of-man.png'),
    costaMarfil: require(pathFlag + '016-ivory-coast.png'),
    lesoto: require(pathFlag + '017-lesotho.png'),
    ruanda: require(pathFlag + '018-rwanda.png'),
    zimbabue: require(pathFlag + '019-zimbabwe.png'),
    guineaEcuatorial: require(pathFlag + '020-equatorial-guinea.png'),
    congoRepublica: require(pathFlag + '021-republic-of-the-congo.png'),
    brasil: require(pathFlag + '022-brazil.png'),
    sriLanka: require(pathFlag + '023-sri-lanka.png'),
    suazilandia: require(pathFlag + '024-swaziland.png'),
    tayikistan: require(pathFlag + '025-tajikistan.png'),
    trinidadTobago: require(pathFlag + '026-trinidad-and-tobago.png'),
    polinesiaFrancesa: require(pathFlag + '027-french-polynesia.png'),
    madeira: require(pathFlag + '028-madeira.png'),
    maldivas: require(pathFlag + '029-maldives.png'),
    moldavia: require(pathFlag + '030-moldova.png'),
    montenegro: require(pathFlag + '031-montenegro.png'),
    papua: require(pathFlag + '032-papua-new-guinea.png'),
    mexico: require(pathFlag + '033-mexico.png'),
    tibet: require(pathFlag + '034-tibet.png'),
    timorOriental: require(pathFlag + '035-East-Timor.png'),
    libia: require(pathFlag + '036-libya.png'),
    macedonia: require(pathFlag + '038-republic-of-macedonia.png'),
    arabeSaharaui: require(pathFlag + '039-sahrawi-arab-democratic-republic.png'),
    sudan: require(pathFlag + '040-sudan.png'),
    uzbekistan: require(pathFlag + '041-uzbekistán.png'),
    yemen: require(pathFlag + '042-yemen.png'),
    liberia: require(pathFlag + '043-liberia.png'),
    rusia: require(pathFlag + '044-russia.png'),
    liechtenstein: require(pathFlag + '045-liechtenstein.png'),
    norteChipre: require(pathFlag + '046-northern-cyprus.png'),
    sanMarino: require(pathFlag + '047-san-marino.png'),
    sanBartolome: require(pathFlag + '048-st-barts.png'),
    curazao: require(pathFlag + '049-curacao.png'),
    yibuti: require(pathFlag + '050-djibouti.png'),
    eritrea: require(pathFlag + '051-eritrea.png'),
    guineaBasau: require(pathFlag + '052-guinea-bissau.png'),
    kirguistan: require(pathFlag + '053-kyrgyzstan.png'),
    mali: require(pathFlag + '054-mali.png'),
    india: require(pathFlag + '055-india.png'),
    islaMarshall: require(pathFlag + '056-marshall-island.png'),
    micronesia: require(pathFlag + '057-micronesia.png'),
    niger: require(pathFlag + '058-niger.png'),
    islaNorfolk: require(pathFlag + '059-norfolk-island.png'),
    santoTomePrincipe: require(pathFlag + '060-sao-tome-and-prince.png'),
    sardinia: require(pathFlag + '061-sardinia.png'),
    butan: require(pathFlag + '062-bhutan.png'),
    somalia: require(pathFlag + '063-somalia.png'),
    surinam: require(pathFlag + '064-suriname.png'),
    islasAzores: require(pathFlag + '065-azores-islands.png'),
    canada: require(pathFlag + '066-canada.png'),
    bahamas: require(pathFlag + '067-bahamas.png'),
    togo: require(pathFlag + '068-togo.png'),
    turkmenistan: require(pathFlag + '069-turkmenistan.png'),
    tuvalu: require(pathFlag + '070-tuvalu.png'),
    vaticano: require(pathFlag + '071-vatican-city.png'),
    austria: require(pathFlag + '072-austria.png'),
    islasVirgenes: require(pathFlag + '073-virgin-islands.png'),
    islasMalvinas: require(pathFlag + '074-falkland-islands.png'),
    gabon: require(pathFlag + '075-gabon.png'),
    gibraltar: require(pathFlag + '076-gibraltar.png'),
    paisesBajos: require(pathFlag + '077-netherlands.png'),
    groenlandia: require(pathFlag + '078-greenland.png'),
    granada: require(pathFlag + '079-grenada.png'),
    guam: require(pathFlag + '080-guam.png'),
    guernsey: require(pathFlag + '081-guernsey.png'),
    guyana: require(pathFlag + '082-guyana.png'),
    kiribati: require(pathFlag + '083-kiribati.png'),
    kuwait: require(pathFlag + '084-kuwait.png'),
    mauritania: require(pathFlag + '085-mauritania.png'),
    melilla: require(pathFlag + '086-melilla.png'),
    montserrat: require(pathFlag + '087-montserrat.png'),
    tailandia: require(pathFlag + '088-thailand.png'),
    nauru: require(pathFlag + '089-nauru.png'),
    islasMarianasNorte: require(pathFlag + '090-northern-marianas-islands.png'),
    osetia: require(pathFlag + '091-ossetia.png'),
    palestina: require(pathFlag + '092-palestine.png'),
    islasPitcairn: require(pathFlag + '093-pitcairn-islands.png'),
    saba: require(pathFlag + '094-saba-island.png'),
    sanCristobalNieves: require(pathFlag + '095-saint-kitts-and-nevis.png'),
    sanEustaquio: require(pathFlag + '096-sint-eustatius.png'),
    sanMartin: require(pathFlag + '097-sint-maarten.png'),
    portugal: require(pathFlag + '098-portugal.png'),
    islasSalomon: require(pathFlag + '099-solomon-islands.png'),
    somalilandia: require(pathFlag + '100-somaliland.png'),
    sudanSur: require(pathFlag + '101-south-sudan.png'),
    sanVicenteGranadinas: require(pathFlag + '102-st-vincent-and-the-grenadines.png'),
    gambia: require(pathFlag + '103-gambia.png'),
    tokelau: require(pathFlag + '104-tokelau.png'),
    transnistria: require(pathFlag + '105-transnistria.png'),
    islasTurcasCaicos: require(pathFlag + '106-turks-and-caicos.png'),
    islasOrcadas: require(pathFlag + '107-orkney-islands.png'),
    guinea: require(pathFlag + '108-guinea.png'),
    vietnam: require(pathFlag + '109-vietnam.png'),
    reinoUnido: require(pathFlag + '110-united-kingdom.png'),
    kosovo: require(pathFlag + '111-kosovo.png'),
    seychelles: require(pathFlag + '112-seychelles.png'),
    sierraLeona: require(pathFlag + '113-sierra-leone.png'),
    vanuatu: require(pathFlag + '114-vanuatu.png'),
    dominica: require(pathFlag + '115-dominica.png'),
    islasFaroe: require(pathFlag + '116-faroe-islands.png'),
    fiji: require(pathFlag + '117-fiji.png'),
    macao: require(pathFlag + '118-macao.png'),
    niue: require(pathFlag + '119-niue.png'),
    palaos: require(pathFlag + '120-palau.png'),
    inglaterra: require(pathFlag + '121-england.png'),
    islaPascua: require(pathFlag + '122-Rapa-Nui.png'),
    santaLucia: require(pathFlag + '123-st-lucia.png'),
    tonga: require(pathFlag + '124-tonga.png'),
    martinica: require(pathFlag + '125-martinique.png'),
    islasGalapagos: require(pathFlag + '126-galapagos-islands.png'),
    bulgaria: require(pathFlag + '127-bulgaria.png'),
    croacia: require(pathFlag + '128-croatia.png'),
    bolivia: require(pathFlag + '129-bolivia.png'),
    camboya: require(pathFlag + '130-cambodia.png'),
    costaRica: require(pathFlag + '131-costa-rica.png'),
    singapur: require(pathFlag + '132-singapore.png'),
    cuba: require(pathFlag + '133-cuba.png'),
    bangladesh: require(pathFlag + '134-bangladesh.png'),
    botsuana: require(pathFlag + '135-botswana.png'),
    argelia: require(pathFlag + '136-Algeria.png'),
    azerbaiyan: require(pathFlag + '137-azerbaijan.png'),
    angola: require(pathFlag + '138-angola.png'),
    columbiaBritanica: require(pathFlag + '139-british-columbia.png'),
    afganistan: require(pathFlag + '140-afghanistan.png'),
    armenia: require(pathFlag + '141-armenia.png'),
    paisVasco: require(pathFlag + '142-basque-country.png'),
    australia: require(pathFlag + '143-australia.png'),
    burkinaFaso: require(pathFlag + '144-burkina-faso.png'),
    albania: require(pathFlag + '145-albania.png'),
    barein: require(pathFlag + '146-bahrain.png'),
    belice: require(pathFlag + '147-belize.png'),
    bermudas: require(pathFlag + '148-bermuda.png'),
    bosniaHerzegovina: require(pathFlag + '149-bosnia-and-herzegovina.png'),
    islasVirgenesBritanicas: require(pathFlag + '150-british-virgin-islands.png'),
    barbados: require(pathFlag + '151-barbados.png'),
    bielorrusia: require(pathFlag + '152-belarus.png'),
    bonaire: require(pathFlag + '153-bonaire.png'),
    turquia: require(pathFlag + '154-turkey.png'),
    brunei: require(pathFlag + '155-brunei.png'),
    camerun: require(pathFlag + '156-cameroon.png'),
    islasAland: require(pathFlag + '157-aland-islands.png'),
    antiguaBarbuda: require(pathFlag + '158-antigua-and-barbuda.png'),
    burundi: require(pathFlag + '159-burundi.png'),
    andorra: require(pathFlag + '160-andorra.png'),
    islasBaleares: require(pathFlag + '161-balearic-islands.png'),
    abjasia: require(pathFlag + '162-abkhazia.png'),
    aruba: require(pathFlag + '163-aruba.png'),
    benin: require(pathFlag + '164-benin.png'),
    polonia: require(pathFlag + '165-poland.png'),
    caboVerde: require(pathFlag + '166-cape-verde.png'),
    chad: require(pathFlag + '167-chad.png'),
    territorioBritanicoOceanoIndico: require(pathFlag + '169-british-indian-ocean-territory.png'),
    islasCaiman: require(pathFlag + '170-cayman-islands.png'),
    republicaCentroafricana: require(pathFlag + '171-central-african-republic.png'),
    comoras: require(pathFlag + '172-comoros.png'),
    islasCanarias: require(pathFlag + '173-canary-islands.png'),
    corcega: require(pathFlag + '174-corsica.png'),
    anguila: require(pathFlag + '175-anguilla.png'),
    suiza: require(pathFlag + '176-switzerland.png'),
    samoaAmericana: require(pathFlag + '177-american-samoa.png'),
    ceuta: require(pathFlag + '178-ceuta.png'),
    islaNavidad: require(pathFlag + '179-christmas-island.png'),
    islasCocos: require(pathFlag + '180-cocos-island.png'),
    islasCook: require(pathFlag + '181-cook-islands.png'),
    marruecos: require(pathFlag + '182-morocco.png'),
    taiwan: require(pathFlag + '183-taiwan.png'),
    egipto: require(pathFlag + '184-egypt.png'),
    indonesia: require(pathFlag + '185-indonesia.png'),
    estadosUnidos: require(pathFlag + '186-united-states.png'),
    filipinas: require(pathFlag + '187-philippines.png'),
    surAfrica: require(pathFlag + '188-south-africa.png'),
    peru: require(pathFlag + '189-peru.png'),
    suecia: require(pathFlag + '190-sweden.png'),
    dinamarca: require(pathFlag + '191-denmark.png'),
    grecia: require(pathFlag + '192-greece.png'),
    irlanda: require(pathFlag + '193-ireland.png'),
    laos: require(pathFlag + '194-laos.png'),
    emiratosArabesUnidos: require(pathFlag + '195-united-arab-emirates.png'),
    hongKong: require(pathFlag + '196-hong-kong.png'),
    francia: require(pathFlag + '197-france.png'),
    ukrania: require(pathFlag + '198-ukraine.png'),
    argentina: require(pathFlag + '199-argentina.png'),
    iran: require(pathFlag + '200-iran.png'),
    colombia: require(pathFlag + '201-colombia.png'),
    republicaCheca: require(pathFlag + '202-czech-republic.png'),
    israel: require(pathFlag + '203-israel.png'),
    arabiaSaudita: require(pathFlag + '204-saudi-arabia.png'),
    noruega: require(pathFlag + '205-norway.png'),
    venezuela: require(pathFlag + '206-venezuela.png'),
    malasia: require(pathFlag + '207-malaysia.png'),
    alemania: require(pathFlag + '208-germany.png'),
    belgica: require(pathFlag + '209-belgium.png'),
    hungria: require(pathFlag + '210-hungary.png'),
    finlandia: require(pathFlag + '211-finland.png'),
    chile: require(pathFlag + '212-chile.png'),
    rumania: require(pathFlag + '213-romania.png'),
    ecuador: require(pathFlag + '214-ecuador.png'),
    nuevaZelanda: require(pathFlag + '215-new-zealand.png'),
    panama: require(pathFlag + '216-panama.png'),
    eslovaquia: require(pathFlag + '218-slovakia.png'),
    coreaSur: require(pathFlag + '219-south-korea.png'),
    monaco: require(pathFlag + '220-monaco.png'),
    jordania: require(pathFlag + '221-jordan.png'),
    islandia: require(pathFlag + '222-iceland.png'),
    guatemala: require(pathFlag + '223-guatemala.png'),
    siria: require(pathFlag + '224-syria.png'),
    uruguay: require(pathFlag + '225-uruguay.png'),
    coreaNorte: require(pathFlag + '226-north-korea.png'),
    escocia: require(pathFlag + '227-scotland.png'),
    serbia: require(pathFlag + '228-serbia.png'),
    kazajistan: require(pathFlag + '229-kazakhstan.png'),
    espana: require(pathFlag + '230-spain.png'),
    letonia: require(pathFlag + '231-latvia.png'),
    pakistan: require(pathFlag + '232-pakistan.png'),
    chipre: require(pathFlag + '233-cyprus.png'),
    kenia: require(pathFlag + '234-kenya.png'),
    myanmar: require(pathFlag + '235-myanmar.png'),
    honduras: require(pathFlag + '236-honduras.png'),
    jamaica: require(pathFlag + '237-jamaica.png'),
    lituania: require(pathFlag + '238-lithuania.png'),
    hawaii: require(pathFlag + '239-hawaii.png'),
    luxemburgo: require(pathFlag + '240-luxembourg.png'),
    japon: require(pathFlag + '241-japan.png'),
    mauricio: require(pathFlag + '242-mauritius.png'),
    namibia: require(pathFlag + '243-namibia.png'),
    qatar: require(pathFlag + '244-qatar.png'),
    republicaDominicana: require(pathFlag + '245-dominican-republic.png'),
    gales: require(pathFlag + '246-wales.png'),
    zambia: require(pathFlag + '247-zambia.png'),
    elSalvador: require(pathFlag + '248-el-salvador.png'),
    nicaragua: require(pathFlag + '249-nicaragua.png'),
    tunez: require(pathFlag + '250-tunisia.png'),
    malta: require(pathFlag + '251-malta.png'),
    nigeria: require(pathFlag + '253-nigeria.png'),
    uganda: require(pathFlag + '254-uganda.png'),
    libano: require(pathFlag + '255-lebanon.png'),
    iraq: require(pathFlag + '256-iraq.png'),
    mozambique: require(pathFlag + '257-mozambique.png'),
    puertoRico: require(pathFlag + '258-puerto-rico.png'),
    eslovenia: require(pathFlag + '259-slovenia.png'),
    tanzania: require(pathFlag + '260-tanzania.png'),
    oman: require(pathFlag + '261-oman.png'),
    etiopia: require(pathFlag + '262-ethiopia.png'),
    italia: require(pathFlag + '263-italy.png'),
  };
  const flag = flags[name];
  return (
    <View>
      {icon ? (
        <Icon size={15} color="#007E86" name="bookmark" solid />
      ) : (
        <Image source={flag} style={styles.logo} />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  logo: {
    width: 40,
    height: 40,
    marginLeft: 10,
  },
});

export default FlagLogo;
