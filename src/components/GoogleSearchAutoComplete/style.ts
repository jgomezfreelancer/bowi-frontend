import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    textInputContainer: {
      marginTop: 3,
      backgroundColor: 'rgba(0,0,0,0)',
      borderTopWidth: 0,
      borderBottomWidth: 0,
    },
    textInput: {
      marginLeft: 0,
      marginRight: 0,
      height: 28,
      color: '#5d5d5d',
      fontSize: 16,
    },
    listView: {
      backgroundColor: 'white',
    },
  });
