import React, { FunctionComponent } from 'react';
import {
  GooglePlaceData,
  GooglePlaceDetail,
  GooglePlacesAutocomplete,
} from 'react-native-google-places-autocomplete';
import { customTraslate } from '../../utils/language';

import Styles from './style';

type Iprops = {
  onChange: (data: GooglePlaceData, detail: GooglePlaceDetail | null) => void;
};

const GoogleSearchAutoComplete: FunctionComponent<Iprops> = ({ onChange }) => {
  return (
    <GooglePlacesAutocomplete
      fetchDetails
      placeholder={customTraslate('buscar')}
      styles={Styles}
      onPress={onChange}
      query={{
        key: 'AIzaSyCfjd0jJEbBcGHhJ7y1PxpiKW0rGGyL_vM',
        language: 'es',
      }}
    />
  );
};

export default GoogleSearchAutoComplete;
