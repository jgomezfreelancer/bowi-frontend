import { head } from 'lodash';
import { StyleSheet, TextStyle, ViewStyle } from 'react-native';

interface Styles {
  google: ViewStyle;
  facebook: ViewStyle;
  apple: ViewStyle;
  buttonText: TextStyle;
  socialIcon: ViewStyle;
  containerButton: ViewStyle;
  containerIcon: ViewStyle;
  containerText: ViewStyle;
}

const buttonStyles: ViewStyle = {
  borderRadius: 30,
  borderWidth: 1,
  alignItems: 'center',
  justifyContent: 'center',
  width: '100%',
  margin: 5
};

const SocialLoginButtonStyles: Styles = {
  google: {
    ...buttonStyles,
    backgroundColor: '#EF6860',
    borderColor: '#EF6860',
  },
  facebook: {
    ...buttonStyles,
    backgroundColor: '#365C9E',
    borderColor: '#365C9E',
  },
  apple: {
    ...buttonStyles,
    backgroundColor: '#C4C4C4',
    borderColor: '#C4C4C4',
  },
  buttonText: {
    color: '#fff',
    textAlign: 'center',
    fontWeight: '800',
    fontSize: 14,
  },
  socialIcon: {
    width: 22,
    height: 22,
  },
  containerButton: {
    flexDirection: 'row',
  },
  containerIcon: {
    flex: 1,
    alignItems: 'center'
  },
  containerText: {
    flex: 3,
  }
};

export default StyleSheet.create(SocialLoginButtonStyles);
