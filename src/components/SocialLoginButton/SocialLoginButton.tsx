import React from 'react';

import { Text, TouchableHighlight, Image, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import style from '../common/form/Button/style';
const appleLogoImage = require('../../assets/images/apple-icon.png');
const facebookLogoImage = require('../../assets/images/facebook-icon.png');
const googlePlusLogoLogoImage = require('../../assets/images/google-plus-icon.png');

import Style from './style';

interface IProps {
  socialNetwork: 'google' | 'facebook' | 'apple';
  handleAction: () => void;
  loading?: boolean;
}

export const SocialLoginButton = (props: IProps) => {
  const { handleAction, socialNetwork, loading = false } = props;
  const formatedStrings = () => {
    return socialNetwork !== 'apple' ? 'Continuar con ' + socialNetwork.charAt(0).toUpperCase() + socialNetwork.slice(1) : socialNetwork.charAt(0).toUpperCase() + socialNetwork.slice(1) + ' ID'
  };
  const getUnderlayColor= () => {
    if(socialNetwork === 'google'){
      return '#e65b49'
    }else if(socialNetwork === 'facebook'){
      return '#4669b3'
    }else if(socialNetwork === 'apple'){
      return '#454545'
    }
  };
  const getSocialImageIcon= () => {
    if(socialNetwork === 'google'){
      return googlePlusLogoLogoImage
    }else if(socialNetwork === 'facebook'){
      return facebookLogoImage
    }else if(socialNetwork === 'apple'){
      return appleLogoImage
    }
  };

  return (
    <TouchableHighlight
      underlayColor={getUnderlayColor()}
      style={Style[socialNetwork]}
      onPress={handleAction}
      disabled={loading}
    >
      <View style={Style.containerButton}>
        <View style={Style.containerIcon}>
          <Image style={Style.socialIcon} source={getSocialImageIcon()}/>
        </View>
        <View style={Style.containerText}>
          <Text style={Style.buttonText}>
            {formatedStrings()}
          </Text>
        </View>
        <View style={Style.containerIcon}></View>
      </View>
    </TouchableHighlight>
  );
};
