import React, { useEffect, FunctionComponent } from 'react';

import { GoogleSignin, statusCodes, User } from '@react-native-google-signin/google-signin';
import { useDispatch } from 'react-redux';

import setToast from '../../actions/toast.action';
import { SocialLoginButton } from '../SocialLoginButton/SocialLoginButton';
import { configureGoogleSignIn } from './configureGoogleSignIn';

interface IProps {
  handleLogin: (user: User) => void;
  loading: boolean;
}

const GoogleLogin: FunctionComponent<IProps> = ({ handleLogin, loading = false }) => {
  const dispatch = useDispatch();

  useEffect(configureGoogleSignIn, []);

  const signIn = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      handleLogin(userInfo);
    } catch (error) {
      console.log("catch error");
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
        setToast({
          payload: {
            message: 'play services not available or outdated',
            status: true,
            type: 'error',
          },
        })(dispatch);
      } else {
        console.log("error line 40", error.code);

      }
    }
  };

  return <SocialLoginButton loading={loading} socialNetwork="google" handleAction={signIn} />;
};

export default GoogleLogin;
