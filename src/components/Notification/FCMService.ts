import messaging, { FirebaseMessagingTypes } from '@react-native-firebase/messaging';
import firebase from '@react-native-firebase/app';
import { Alert, Platform } from 'react-native';
import { localNotificationService } from './LocalNotificationService';
import { ENV } from '../../../env';
import { IOpenNotification } from '../../reducers/notification.reducer';

const firebaseConfig = {
  apiKey: ENV.firebase.apiKey,
  authDomain: ENV.firebase.authDomain,
  databaseURL: ENV.firebase.databaseURL,
  projectId: ENV.firebase.projectId,
  storageBucket: ENV.firebase.storageBucket,
  messagingSenderId: ENV.firebase.messagingSenderId,
  appId: ENV.firebase.appId,
};

interface RegisterService {
  onRegister: (token: string) => void;
  onNotification: (remoteMessage: FirebaseMessagingTypes.RemoteMessage) => void;
  onOpenNotification: (remoteMessage: FirebaseMessagingTypes.RemoteMessage) => void;
  backgroundMessageHandler: (remoteMessage: FirebaseMessagingTypes.RemoteMessage) => void;
}

class FCMService {
  messageListener: any;

  register = (
    onRegister: RegisterService['onRegister'],
    onNotification: RegisterService['onNotification'],
    onOpenNotification: RegisterService['onOpenNotification'],
    backgroundMessageHandler: RegisterService['backgroundMessageHandler'],
  ) => {
    this.checkPermission(onRegister);
    this.createNotificationListeners(
      onRegister,
      onNotification,
      onOpenNotification,
      backgroundMessageHandler,
    );
  };

  registerAppWithFCM = async () => {
    await messaging().registerDeviceForRemoteMessages();
    if (Platform.OS === 'ios') {
      (await !firebase.apps.length) ? firebase.initializeApp(firebaseConfig) : firebase.app();
      await messaging().setAutoInitEnabled();
    }
  };

  checkPermission = (onRegister: RegisterService['onRegister']) => {
    messaging()
      .hasPermission()
      .then(enabled => {
        if (enabled) {
          // User has permission
          this.getToken(onRegister);
        } else {
          // User don't have permission
          this.requestPermission(onRegister);
        }
      })
      .catch(error => new Error(error));
  };

  getToken = (onRegister: RegisterService['onRegister']) => {
    messaging()
      .getToken()
      .then(fcmToken => {
        if (fcmToken) {
          onRegister(fcmToken);
        } else {
          console.log('[FCMService] User does not have a devices token');
        }
      })
      .catch(error => new Error(error));
  };

  requestPermission = (onRegister: RegisterService['onRegister']) => {
    messaging()
      .requestPermission()
      .then(() => {
        this.getToken(onRegister);
      })
      .catch(error => new Error(error));
  };

  deleteToken = () => {
    messaging()
      .deleteToken()
      .catch(error => new Error(error));
  };

  createNotificationListeners = (
    onRegister: RegisterService['onRegister'],
    onNotification: RegisterService['onNotification'] ,
    onOpenNotification: RegisterService['onOpenNotification'] ,
    backgroundMessageHandler: RegisterService['backgroundMessageHandler'],
  ) => {
    // When Application Running on Background
    messaging().onNotificationOpenedApp((remoteMessage: FirebaseMessagingTypes.RemoteMessage)  => {
      if (remoteMessage) {
        const notification = remoteMessage;
        onOpenNotification(notification);
      }
    });

    // Recieve notification when the device is silent
    messaging().setBackgroundMessageHandler(async (remoteMessage: FirebaseMessagingTypes.RemoteMessage) =>
      backgroundMessageHandler(remoteMessage),
    );

    // When Application open from quit state
    messaging()
      .getInitialNotification()
      .then(remoteMessage => {
        if (remoteMessage) {
          const notification = remoteMessage;
          localNotificationService.cancelAllLocalNotifications();
          onOpenNotification(notification);
        }
      })
      .catch(err => err);


    // Forground state message
    this.messageListener = messaging().onMessage(async (remoteMessage: FirebaseMessagingTypes.RemoteMessage)  => {
      onNotification(remoteMessage);
    });

    // Triggered when have new Token
    messaging().onTokenRefresh(fcmToken => {
      onRegister(fcmToken);
    });
  };

  unRegister = () => {
    this.messageListener();
  };
}

export const fcmService = new FCMService();
