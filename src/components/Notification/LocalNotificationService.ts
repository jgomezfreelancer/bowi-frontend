// import PushNotification, {
//   PushNotification as IPushNotification,
// } from 'react-native-push-notification';
import { INotification, INotify } from '../../reducers/notification.reducer';
import PushNotification, { PushNotification as IPushNotification } from "react-native-push-notification";

interface ILocalNotificationService {
  onOpenNotification: (notify: INotify) => void;
}

class LocalNotificationService {
  configure = async (onOpenNotification: ILocalNotificationService['onOpenNotification']) => {
    PushNotification.configure({
      onNotification: (notification: IPushNotification) => {
        if (!notification?.data) {
          return;
        }
        notification.userInteraction = true;
        onOpenNotification(notification.data as INotify);
      },
      // IOS ONLY (optional): default: all - Permissions to register.
      permissions: {
        alert: true,
        badge: true,
        sound: true,
      },

      // Should the initial notification be popped automatically
      // default: showNotification
      popInitialNotification: true,

      /**
       * (optional) default: true
       * - Specified if permissions (ios) and token (android and ios) will requested or not,
       * - if not, you must call PushNotificationsHandler.requestPermissions() later
       * - if you are not using remote notification or do not have Firebase installed, use this:
       *     requestPermissions: Platform.OS === 'ios'
       */
      requestPermissions: true,
    });
  };

  unregister = () => {
    PushNotification.unregister();
  };

  // Function to Show the Push Notification
  showNotification = (
    id?: number,
    title?: string,
    message?: string,
    data: INotification = {},
    options: INotification = {},
  ) => {
    PushNotification.localNotification({
      /* Android Only Properties */
      ...this.buildAndroidNotification(id, title, message, data, options),
      title: title || '',
      message: message || '',
      playSound: options.playSound || false,
      soundName: options.soundName || 'default',
      channelId: '32',
    });
  };

  buildAndroidNotification = (
    id?: number,
    title?: string,
    message?: string,
    data: INotification = {},
    options: INotification = {},
  ) => {
    return {
      id,
      autoCancel: true,
      largeIcon: options.largeIcon || 'ic_launcher',
      smallIcon: options.smallIcon || 'ic_notification',
      bigText: message || '',
      subText: title || '',
      vibrate: options.vibrate || true,
      vibration: options.vibration || 300,
      priority: 'high',
      importance: 'high',
      data,
    };
  };

  cancelAllLocalNotifications = () => {
    PushNotification.cancelAllLocalNotifications();
  };

  removeDeliveredNotificationByID = (notificationId: number) => {
    PushNotification.cancelLocalNotifications({ id: `${notificationId}` });
  };

  // applicationBadge = () => {
  //     // PushNotification.setApplicationIconBadgeNumber(2);
  //     // const ShortcutBadger = NativeModules.ShortcutBadger;
  //     // let count = 1;
  //     // ShortcutBadger.applyCount(count);
  // }
}

export const localNotificationService = new LocalNotificationService();
