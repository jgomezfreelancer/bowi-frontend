import React, { useCallback } from 'react';

import { View, Text, TouchableHighlight, TextInput } from 'react-native';
import { Header, Footer, FooterTab } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import ArrowIcon from '../../../assets/icons/flecha.svg';
import SearchIcon from '../../../assets/icons/icono-mapa-10.svg';
import { customTraslate } from '../../../utils/language';

import Styles from './style';
import { is } from '@babel/types';

interface IProps {
  handleLeft?: (() => void) | null;
  handleRigth?: (() => void) | null;
  onChangeText?: ((val: string) => void) | null;
  leftColor?: string;
  leftTitle?: string;
  rightTitle?: string;
  rightColor?: string;
  leftIcon?: string;
  rightIcon?: string;
  title?: string;
  borderBottom?: boolean;
  isInput?: boolean;
}

const CustomHeader = (props: IProps) => {
  const {
    handleLeft,
    handleRigth,
    onChangeText,
    leftColor = '#44c7d5',
    leftTitle,
    rightTitle,
    rightColor = '#e74c3c',
    leftIcon = 'user',
    rightIcon = 'power-off',
    title,
    borderBottom = true,
    isInput = false
  } = props;

  const left = useCallback(() => {
    if (handleLeft) {
      handleLeft();
    }
  }, [handleLeft]);

  const right = useCallback(() => {
    if (handleRigth) {
      handleRigth();
    }
  }, [handleRigth]);

  const changeText= useCallback((val: string) => {
    if (onChangeText) {
      onChangeText(val);
    }
  }, [onChangeText]);

  const renderInputHeader = () => {
    return (
      <Header style={Styles.inputHeaderContainer}>
        <View style={Styles.itemTabContainer}>
          <View style={Styles.textCommentContainer}>
            <TouchableHighlight style={Styles.sendIconContainer} onPress={left} underlayColor="transparent">
              <Text style={Styles.clipIconContainer}>
                <ArrowIcon width={21} height={21}/>
              </Text>
            </TouchableHighlight>
            <TextInput
              style={Styles.textComment}
              placeholder={customTraslate('buscar')}
              onChangeText={changeText}
            />
            <TouchableHighlight style={Styles.sendIconContainer} underlayColor="transparent">
              <Text style={Styles.clipIconContainer}>
                <Icon color="#b3b3b3" size={21} name="search" />
              </Text>
            </TouchableHighlight>
          </View>
        </View>
      </Header>
    )
  };

  const renderHeader = () => {
    const flag = isInput;
    return (
      flag != true ? (
        <Header style={borderBottom != true ? Styles.headerNoBottom : Styles.header}>
          <View style={Styles.headerContainer}>
            <View style={Styles.itemContainer}>
              {handleLeft && (
                <TouchableHighlight style={Styles.leftIcon} onPress={left} underlayColor="transparent">
                  {leftTitle ? (
                    <Text style={Styles.cornerLeftTitle} >{leftTitle}</Text>
                  ) : (
                    <ArrowIcon width={25} height={25}/>
                  )}
                </TouchableHighlight>
              )}
            </View>

            <View style={Styles.itemContainer}>
              <Text style={Styles.title}>{title}</Text>
            </View>
            <View style={Styles.itemContainer}>
              {handleRigth && (
                <TouchableHighlight
                  style={Styles.rightIcon}
                  onPress={right}
                  underlayColor="transparent"
                >
                  {rightTitle ? (
                    <Text style={{...Styles.cornerRightTitle, color: rightColor }} >{rightTitle}</Text>
                  ) : (
                    <Icon name={rightIcon} size={25} color={rightColor} />
                  )}
                </TouchableHighlight>
              )}
            </View>
          </View>
        </Header>
      ) : (
        renderInputHeader()
      )
    );
  };

  return renderHeader();
};

export default CustomHeader;
