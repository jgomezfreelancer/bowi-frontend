import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  messageContainer: {marginBottom: 20, justifyContent: 'center'},
  messageText: {textAlign: 'center', fontSize: 18, fontWeight: 'bold'},
  optionsContainer: {
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  halfGrid: {
    width: '50%',
  },
  optionsText: {
    textAlign: 'center',
    color: '#076dfa',
    fontWeight: 'bold',
  },
});
