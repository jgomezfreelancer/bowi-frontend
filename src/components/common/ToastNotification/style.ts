import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  messageContainer: { flex: 1, flexDirection: 'row', alignItems: 'center' },
  userTitle: {
    marginLeft: 10,
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  title2: {
    color: 'white',
    fontSize: 16,
    marginLeft: 4,
  },
  title3: {
    color: '#c0392b',
    fontSize: 17,
    marginLeft: 4,
    textAlign: 'center',
    fontWeight: 'bold',
    fontStyle: 'italic'
  },
  closeIcon: {
    marginLeft: 30,
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    width: '10%',
    textAlign: 'right',
  },
  animatedViewContainer: {
    transform: [{ translateY: -70 }],
    height: 70,
    position: 'absolute',
    left: '5%',
    top: -550,
    right: 0,
    width: '90%',
    paddingLeft: 10,
    paddingRight: 10,
    alignItems: 'center',
    borderRadius: 20,
  },
  messageContainer1 : { flexDirection: 'column', justifyContent: 'center' },
  messageTextContainer: { flexDirection: 'row', justifyContent: 'center' }
});
