import React, { useEffect, useRef, useCallback } from 'react';

import { View, Animated, Text, TouchableHighlight } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome';

import { setToastNotification } from '../../../reducers/toastNotification.reducer';
import { AppRootReducer } from '../../../reducers';
import Style from './style';
import { customTraslate } from '../../../utils/language';

const fadeDuration = 300;

export default function ToastNotification(): JSX.Element {
  const {
    toastNotificationReducer: {
      settings: { status, userNotification, type, handle },
    },
  } = useSelector((state: AppRootReducer) => state);
  const dispatch = useDispatch();
  const opacity = useRef(new Animated.Value(0)).current;

  const fadeIn = useCallback(() => {
    Animated.timing(opacity, {
      toValue: 1,
      duration: fadeDuration,
      useNativeDriver: true,
    }).start();
  }, [opacity]);

  const fadeOut = useCallback(() => {
    Animated.timing(opacity, {
      toValue: 0,
      duration: fadeDuration,
      useNativeDriver: true,
    }).start();
  }, [opacity]);

  useEffect(() => {
    if (status) {
      fadeIn();
      const timer = setTimeout(() => {
        setToastNotification({
          status: false,
          handle: () => null,
        })(dispatch);
      }, 3000);
      return () => clearTimeout(timer);
    } else {
      fadeOut();
    }
  }, [status]);

  const getTypeColor = (color: string) => {
    switch (color) {
      case 'success':
        return '#27ae60';
      case 'error':
        return '#e74c3c';
      default:
        return '#27ae60';
    }
  };

  const close = () => {
    fadeOut();
    setToastNotification({ status: false, handle: () => null })(dispatch);
  };

  return (
    <View style={{ display: status ? 'flex' : 'none' }}>
      <Animated.View
        style={{
          ...Style.animatedViewContainer,
          backgroundColor: getTypeColor(type!),
          opacity,
        }}
      >
        <TouchableHighlight
          underlayColor={getTypeColor(type!)}
          onPress={handle ? handle : () => null}
        >
          <View style={Style.messageContainer}>
            <View style={Style.messageContainer1}>
              <View style={Style.messageTextContainer}>
                <Text style={Style.userTitle}>!{userNotification}</Text>
                <Text style={Style.title2}>{customTraslate('estaPidiendoAyuda')}!</Text>
              </View>
              <Text style={Style.title3}>{customTraslate('estaPidiendoAyuda').toUpperCase()}!</Text>
            </View>
            <Icon style={Style.closeIcon} name="times-circle" onPress={close} />
          </View>
        </TouchableHighlight>
      </Animated.View>
    </View>
  );
}
