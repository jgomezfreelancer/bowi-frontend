import React, { useCallback } from 'react';
import { Modal, View } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';

import Icon from 'react-native-vector-icons/FontAwesome';
import setModal from '../../../actions/modal.action';
import Styles from './style';

function CustomModal(): JSX.Element {
  const {
    modalReducer: { children, status },
  } = useSelector((state: any) => state);

  const dispatch = useDispatch();

  const close = useCallback(() => {
    setModal({
      payload: {
        status: false,
        children: false,
      },
    })(dispatch);
  }, [dispatch]);

  return (
    status && (
      <View style={Styles.centeredView}>
        <Modal animationType="slide" transparent={true} visible={status}>
          <View style={Styles.centeredView}>
            <View style={Styles.modalView}>
              <View style={Styles.header}>
                <Icon style={Styles.closeIcon} name="times" onPress={close} />
              </View>
              <View>{children}</View>
            </View>
          </View>
        </Modal>
      </View>
    )
  );
}

export default CustomModal;
