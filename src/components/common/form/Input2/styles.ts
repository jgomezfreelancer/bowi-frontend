import { StyleSheet, TextStyle, ViewStyle } from 'react-native';
import { isIOS } from '../../../../helpers/isIOS';

interface Styles {
  customInputContainer: ViewStyle;
  container: ViewStyle;
  inputContainer: ViewStyle;
  messageContainer: ViewStyle;
  error: TextStyle;
  input: TextStyle;
  fieldRequired: TextStyle;
  iconContainer: ViewStyle;
}

export const inputStyles: Styles = {
  customInputContainer: { position: 'relative' },
  container: {
    width: '100%',
  },
  inputContainer: { position: 'relative' },
  messageContainer: {
    margin: 10,
    marginTop: 0,
    marginBottom: 0,
  },
  error: {
    color: 'red',
    fontWeight: 'bold',
  },
  input: {
    color: '#a3a3a3',
    fontSize: 14,
    backgroundColor: '#EFEFEF',
    borderRadius: 5,
    paddingLeft: 15,
    paddingRight: 20,
    paddingTop: isIOS ? 0 : 15,
    height: 45,
    textAlign: 'left',
    width: '100%',
    borderColor: '#EFEFEF'
  },
  iconContainer: {
    position: 'absolute',
    top: '28%',
    left: '4%',
  },
  fieldRequired: {
    borderColor: '#e74c3c',
    borderWidth: 1,
  },
}

export default StyleSheet.create(inputStyles);
