import React, { FunctionComponent } from 'react';
import { View, Text } from 'react-native';
import { Controller } from 'react-hook-form';
// import { Picker as MyPicker } from '@react-native-community/picker';
import { Picker as MyPicker } from '@react-native-picker/picker';

import styles from './styles';

type PropFields = {
  control: any;
  name: string;
  required?: boolean;
  error: any;
  placeholder?: string;
  secureText?: boolean;
  message?: boolean;
  iconName?: string;
  children: JSX.Element[];
  enabled?: boolean;
};

const Select: FunctionComponent<PropFields> = ({
  control,
  name,
  required = false,
  error = null,
  message,
  children,
  enabled,
}) => {
  const SelectContainer = (props: any) => {
    const { onChange, value } = props;
    return (
      <View style={styles.container}>
        {error && message && (
          <View style={styles.messageContainer}>
            <Text style={styles.error}>{error && error}</Text>
          </View>
        )}
        <View
          style={
            !message && error
              ? { ...styles.selectContainer, ...styles.fieldRequired }
              : styles.selectContainer
          }
        >
          <MyPicker enabled={enabled} mode="dropdown" onValueChange={onChange} selectedValue={value}>
            <MyPicker.Item label="Seleccione" value="" />
            {children}
          </MyPicker>
        </View>
      </View>
    );
  };
  return (
    <Controller
      control={control}
      render={SelectContainer}
      name={name}
      rules={{ required: required ? 'Requerido' : false }}
      defaultValue=""
    />
  );
};

export default Select;
