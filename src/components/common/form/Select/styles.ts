import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    width: '100%',
  },
  messageContainer: {
    margin: 10,
    marginTop: 0,
    marginBottom: 0,
  },
  error: {
    color: 'red',
    fontWeight: 'bold',
  },
  selectContainer: {
    position: 'relative',
  },
  iconContainer: {
    position: 'absolute',
    top: '28%',
    left: '4%',
  },
  icon: {
    color: '#c8c8c8',
  },
  fieldRequired: {
    borderColor: 'red',
    borderRadius: 10,
    borderWidth: 2,
  },
});
