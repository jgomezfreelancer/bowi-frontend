import React, { FunctionComponent } from 'react';
import { View, Text, NativeSyntheticEvent, TextInputFocusEventData, TextInputEndEditingEventData } from 'react-native';
import { Controller } from 'react-hook-form';
import Icon from 'react-native-vector-icons/FontAwesome';
import { TextInput } from 'react-native-gesture-handler';
import styles from './styles';

type PropFields = {
  control: any;
  name: string;
  required?: boolean;
  error: any;
  placeholder?: string;
  secureText?: boolean;
  message?: boolean;
  iconName?: string;
  align?: 'left' | 'right';
  editable?: boolean;
  onInputBlur?: ((e: NativeSyntheticEvent<TextInputFocusEventData>) => void) | undefined
};

const Input: FunctionComponent<PropFields> = ({
  control,
  name,
  required = false,
  error = null,
  placeholder = '',
  secureText = false,
  message,
  iconName,
  align = 'left',
  editable,
  onInputBlur
}) => {
  const getInputStyles = () => {
    let inputStyles = styles.input;
    if (!message && error) {
      inputStyles = { ...styles.input, ...styles.fieldRequired };
    }
    if (align === 'right') {
      inputStyles = { ...inputStyles, textAlign: 'right' };
    }
    return inputStyles;
  };
  const InputContainer = (props: any) => {
    const { onChange, value } = props;
    return (
      <View style={styles.container}>
        {error && message && (
          <View style={styles.messageContainer}>
            <Text style={styles.error}>{error && error}</Text>
          </View>
        )}
        <View style={styles.customInputContainer}>
          <View style={styles.inputContainer}>
            <TextInput
              onEndEditing={onInputBlur}
              onChangeText={onChange}
              value={value}
              placeholder={placeholder}
              secureTextEntry={secureText}
              style={getInputStyles()}
              placeholderTextColor="#c8c8c8"
              editable={editable}
            />
          </View>
          {iconName && (
            <View style={styles.iconContainer}>
              <Icon color="#c8c8c8" size={20} name={iconName} />
            </View>
          )}
        </View>
      </View>
    );
  };

  return (
    <Controller
      control={control}
      render={InputContainer}
      name={name}
      rules={{ required: required ? 'Requerido' : false }}
      defaultValue=""
    />
  );
};

export default Input;
