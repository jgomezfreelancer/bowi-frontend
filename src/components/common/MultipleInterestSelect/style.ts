import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    containerItemList:{
        paddingVertical: 5,
    },
    listText: {
        color: '#000000',
        fontSize: 17,
        fontWeight: '400',
        marginLeft: 10,
    },
    image: {
        width: 20,
        height: 20,
        borderRadius: 50,
        marginRight: 10,
        marginTop: 1,
      },
    icon: { marginRight: 10, marginTop: 2 },
    checkbox: { borderRadius: 50, paddingLeft: -20, paddingRight: 3 },
    listItem: {
        marginLeft: '5%',
        marginRight: '5%'
    }
});
