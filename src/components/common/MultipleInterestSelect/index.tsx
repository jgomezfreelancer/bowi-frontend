import React, { useEffect, useState } from 'react';
import { Content, ListItem, CheckBox, Body } from 'native-base';
import { Text } from 'react-native';

import Styles from './style';
import FlagInterestLogo from '../../FlagInterestLogo';
import { BLUE } from '../../../styles/colors';
import { customTraslate } from '../../../utils/language';

type IList = {
  id: number;
  description: string;
  language?: string;
};
interface IProps {
  list: IList[];
  selectedList?: number[];
  onChange: (list: number[]) => void;
  icon?: boolean;
  getLabel?: (object: { language: string; description: string }) => void;
}

export default function MultipleInteresSelect(props: IProps): JSX.Element {
  const { list, onChange, selectedList, icon, getLabel } = props;
  const [selection, setSelection] = useState<number[]>([]);

  useEffect(() => {
    if (selectedList!.length > 0) {
      setSelection(selectedList!);
    }
  }, [selectedList]);

  const onSelect = (id: number) => {
    const exist = selection.find(element => element === -1);

    if (exist) {
      setSelection([]);
      onChange([]);
    } else if (id === -1) {
      const newArray = [...list];
      const newList = newArray.map(element => element.id);
      setSelection([-1, ...newList]);
      onChange(newList);
    } else {
      const selected = selection.find(element => element === id);
      if (selected) {
        const newArray = selection.filter(element => element !== id);
        setSelection(newArray);
        onChange(newArray);
      } else {
        const arrayToAdd = [...selection];
        arrayToAdd.push(id);
        setSelection(arrayToAdd);
        onChange(arrayToAdd);
      }
    }
  };

  const getList = (element: any, i: number) => {
    const current = selection.find(row => row === element.id);
    const checked = !!current;
    return (
      <ListItem style={Styles.listItem} key={i} onPress={() => onSelect(element.id)}>
        {icon !== false ? <FlagInterestLogo icon={icon} name={element.language} /> : null}
        <Body style={Styles.containerItemList}>
          <Text style={Styles.listText}>
            {customTraslate(element.language)}
          </Text>
        </Body>
        <CheckBox style={Styles.checkbox} color={BLUE} checked={checked}
          onPress={() => onSelect(element.id)} />
      </ListItem>
    );
  };

  return (
    <Content>
      {list.length > 0 &&
        [
          {
            id: -1,
            description: 'todos',
            slug: 'todas',
            image: '',
            language: 'todos'
          },
          ...list,
        ].map(getList)}
    </Content>
  );
}
