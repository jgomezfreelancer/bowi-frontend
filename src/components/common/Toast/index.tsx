import React, { useEffect, useRef } from 'react';

import { View, Animated, Text } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome';

import setToast from '../../../actions/toast.action';
import { AppRootReducer } from '../../../reducers';
import Style from './style';

const fadeDuration = 300;

export default function Toast(): JSX.Element {
  const {
    toastReducer: { status, message, type },
  } = useSelector((state: AppRootReducer) => state);
  const dispatch = useDispatch();
  const opacity = useRef(new Animated.Value(0)).current;

  const fadeIn = React.useCallback(() => {
    Animated.timing(opacity, {
      toValue: 1,
      duration: fadeDuration,
      useNativeDriver: true,
    }).start();
  }, [opacity]);

  const fadeOut = React.useCallback(() => {
    Animated.timing(opacity, {
      toValue: 0,
      duration: fadeDuration,
      useNativeDriver: true,
    }).start();
  }, [opacity]);

  useEffect(() => {
    if (status) {
      fadeIn();
      setTimeout(() => {
        setToast({
          payload: {
            status: false,
          },
        })(dispatch);
      }, 5000);
    } else {
      fadeOut();
      setToast({
        payload: {
          status: false,
          type: '',
        },
      })(dispatch);
    }
  }, [status]);

  const getTypeColor = (color: string) => {
    switch (color) {
      case 'success':
        return '#27ae60';
      case 'error':
        return '#e74c3c';
      default:
        return '#27ae60';
    }
  };

  const close = () => {
    fadeOut();
    setToast({
      payload: {
        status: false,
      },
    })(dispatch);
  };

  return (
    <View style={{ display: status ? 'flex' : 'none' }}>
      <Animated.View
        style={{
          ...Style.animatedViewContainer,
          backgroundColor: getTypeColor(type),
          opacity,
        }}
      >
        <View style={Style.messageContainer}>
          <Text style={Style.message}>{message}</Text>
          <Icon style={Style.closeIcon} name="times-circle" onPress={close} />
        </View>
      </Animated.View>
    </View>
  );
}
