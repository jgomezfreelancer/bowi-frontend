import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  messageContainer: { flex: 1, flexDirection: 'row', alignItems: 'center' },
  message: {
    marginLeft: 10,
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
    width: '90%',
    textAlign: 'left',
  },
  closeIcon: {
    marginLeft: 10,
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    width: '10%',
    textAlign: 'right',
  },
  animatedViewContainer: {
    transform: [{ translateY: -70 }],
    height: 70,
    position: 'absolute',
    left: '5%',
    top: -10,
    right: 0,
    bottom: 0,
    width: '90%',
    paddingLeft: 20,
    paddingRight: 20,
    alignItems: 'center',
    borderRadius: 20,
  }
});
