import React from 'react';

import {
  LoginManager,
  AccessToken,
  GraphRequest,
  GraphRequestManager,
  LoginResult,
  GraphRequestCallback,
} from 'react-native-fbsdk';
import { IFB } from '../../screens/Login';
import { SocialLoginButton } from '../SocialLoginButton/SocialLoginButton';

interface IProps {
  handle: (result: IFB) => void;
  loading: boolean;
}

const AppleLogin = (props: IProps) => {
  const { handle, loading } = props;

  const responseInfoCallback: GraphRequestCallback = (error, result) => {
    if (!error && result) {
      handle(result as IFB);
    }
  };

  const handleAppleLogin = () => {
    LoginManager.logInWithPermissions(['public_profile', 'email'])
      .then((result: LoginResult) =>
        !result.isCancelled ? AccessToken.getCurrentAccessToken() : null,
      )
      .then((data: AccessToken | null | undefined) => {
        if (data) {
          const accessToken = data.accessToken;
          const infoRequest = new GraphRequest(
            '/me',
            {
              accessToken,
              parameters: {
                fields: {
                  string: 'email,name,first_name,middle_name,last_name',
                },
              },
            },
            responseInfoCallback,
          );
          new GraphRequestManager().addRequest(infoRequest).start();
        }
      })
      .catch(err => new Error(err));
  };

  return <SocialLoginButton loading={loading} socialNetwork="apple" handleAction={handleAppleLogin} />;
};

export default AppleLogin;
