import React from 'react';
import { Text, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

import { IUser } from '../../reducers/login.reducer';

import Style from './style';

interface Iprops {
  user: IUser;
  handleGo: () => void;
  type: 'request' | 'accept';
}

function HelpRequest(props: Iprops): JSX.Element {
  const { user, handleGo, type } = props;
  const km = user.meters! <= 1000 ? `${user.meters} Mtrs` : `${user.meters! / 1000} Km`;

  const requestMessage = 'Tiene problemas, quieres ayudarlo?';
  const acceptMessage = 'Esta dispuesto a ayudarte, quieres hablar con el?';

  return (
    <View style={Style.root}>
      <View style={Style.userIconContainer}>
        <Icon size={100} color="#818181" name="user-circle" solid />
      </View>
      <View style={Style.userDetailContainer}>
        <Text style={Style.userDetail}>
          {user.first_name} {user.last_name?.charAt(0)}{' '}
        </Text>
        <Text>a {km} Km</Text>
      </View>
      <View style={Style.rankingContainer}>
        <Icon size={15} color="#42c2cf" name="star" solid />
        <Icon size={15} color="#42c2cf" name="star" solid />
        <Icon size={15} color="#42c2cf" name="star" solid />
        <Icon size={15} color="#42c2cf" name="star" light />
        <Text> (45)</Text>
      </View>
      <View style={Style.divider} />
      <View style={Style.commentContainer}>
        <Text style={Style.comment}>{type === 'request' ? requestMessage : acceptMessage}</Text>
        <Text style={Style.button} onPress={handleGo}>
          {type === 'request' ? 'Ayudar' : 'Hablar'}
        </Text>
      </View>
    </View>
  );
}

export default HelpRequest;
