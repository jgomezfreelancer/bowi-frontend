import { StyleSheet, Dimensions } from 'react-native';

const { width } = Dimensions.get('window');

export default StyleSheet.create({
  root: {
    justifyContent: 'center',
  },
  userIconContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  userDetailContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 10,
  },
  userDetail: { color: '#2c3e50', fontWeight: 'bold', fontSize: 16 },
  button: {
    marginTop: 15,
    borderRadius: 10,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: '#42c2cf',
    color: 'white',
    width: '30%',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 17,
  },
  rankingContainer: { flexDirection: 'row', justifyContent: 'center' },
  divider: {
    borderBottomColor: '#d3d3d3',
    borderBottomWidth: 1,
    marginTop: 20,
    marginBottom: 10,
    width: width - 60,
  },
  commentContainer: { justifyContent: 'center', alignItems: 'center', marginBottom: 15 },
  comment: {
    color: '#707070',
    fontWeight: 'bold',
    fontSize: 16,
    width: '70%',
    textAlign: 'center',
  },
});
