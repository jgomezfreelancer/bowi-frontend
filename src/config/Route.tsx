import React, { useCallback, useEffect, useRef } from 'react';

import { useSelector, useDispatch } from 'react-redux';
import { NavigationContainer, NavigationContainerRef } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import firebase from '@react-native-firebase/app';
import socketIOClient from 'socket.io-client';
import { AppState, AppStateStatus } from 'react-native';

import { localNotificationService } from '../components/Notification/LocalNotificationService';
import {
  acceptHelp,
  checkLogin,
  getUsersByLocation,
  IUser,
  setLoadingRequest,
  updateFcm,
} from '../reducers/login.reducer';
import BottomTabNavigator from '../navigation/BottomTabNavigatior';
import { setModalPetition, setModalRequest } from '../reducers/location.reducer';
import Toast from '../components/common/Toast';
import Modal from '../components/common/Modal';
import MainLoader from '../screens/MainLoader';
import MyLocation from '../screens/MyLocation';
import Main from '../screens/Main';
import { AppRootReducer } from '../reducers';
import {
  INotification,
  IOpenNotification,
  remove,
  setNotificationIcon,
} from '../reducers/notification.reducer';
import Login from '../screens/Login';
import { ENV } from '../../env';
import Chat from '../screens/Chat';
import { getUserRooms, IUserRooms, setChat, setChatLoading } from '../reducers/room.reducer';
import Profile from '../screens/Profile';
import EditProfile from '../screens/EditProfile';
import setToast from '../actions/toast.action';
import Message from '../helpers/message';
import Register from '../screens/Register';
import Starter from '../screens/Starter';
import ChooseLanguage from '../screens/ChooseLanguage';
import ChooseNationality from '../screens/ChooseNationality';
import ChooseSettings from '../screens/ChooseSettings';
import ForgotPassword from '../screens/ForgotPassword';
import Languages from '../screens/Languages';
import Nastionalities from '../screens/Nationalities';
import Petition from '../screens/Petition';
import ToastNotification from '../components/common/ToastNotification';
import { setToastNotification } from '../reducers/toastNotification.reducer';
import { fcmService } from '../components/Notification/FCMService';
import PetitionDetail from '../screens/PetitionDetail';
import ListOnlyHelp from '../screens/ListOnlyHelp';
import ChangeSettings from '../screens/ChangeSettings';
import { FirebaseMessagingTypes } from '@react-native-firebase/messaging';
import ChooseInterest from '../screens/ChooseInterest';
import ChooseGender from '../screens/ChooseGender';
import Interest from '../screens/Interest';
import { setI18nConfig } from '../utils/language';

const { Screen, Navigator } = createStackNavigator();
const ENDPOINT = ENV.websocket.chat.alert;
let chatAlertSocket: any;

if (!firebase.apps.length) {
  firebase.initializeApp(ENV.firebase);
}

export const Route = (): JSX.Element => {
  const navigationRef = useRef<NavigationContainerRef | null>(null);
  const {
    loginReducer: {
      checkUser,
      checkUserLoading,
      user: { id, first_time, limit_distance },
    },
    locationReducer: { meters },
  } = useSelector((state: AppRootReducer) => state);
  const dispatch = useDispatch();

  const checkAlertNotificationRoute = () => {
    const route =
      navigationRef && navigationRef.current ? navigationRef.current.getCurrentRoute() : null;
    if (route!.name === 'Rooms' || route!.name === 'Chat') {
      return false;
    }
    return true;
  };

  const getCurrentNameRoute = () => {
    const route =
      navigationRef && navigationRef.current ? navigationRef.current.getCurrentRoute() : null;
    return route!.name;
  };

  const init = useCallback(() => {
    if (checkUser && id! > 0) {
      const options = {
        soundName: 'default',
        playSound: true,
      };
      chatAlertSocket = socketIOClient(ENDPOINT);
      chatAlertSocket.on('connect', () => {
        chatAlertSocket.emit('joinUserAlert', { userId: id });
      });

      // New Notification Chat Message
      chatAlertSocket.on('clientIncommingMessage', (notify: INotification) => {
        if (checkAlertNotificationRoute()) {
          dispatch(getUserRooms(id!));
          dispatch(setNotificationIcon(true));
          // dispatch(getUsersByLocation(id!, meters === 0 ? limit_distance! : meters));
          localNotificationService.showNotification(
            notify.id,
            notify.title,
            notify.text,
            notify,
            options,
          );
        }
      });

      // Notification to refresh rooms list
      chatAlertSocket.on('clientRefreshRoomList', (rooms: IUserRooms[]) => {
        dispatch(getUserRooms(id!));
        if (checkAlertNotificationRoute()) {
          dispatch(setNotificationIcon(true));
        }
      });

      // Notification to refresh rooms list
      chatAlertSocket.on('refreshLocations', (notify: { refresh: boolean }) => {
        if (notify.refresh) {
          dispatch(getUsersByLocation(id!, meters === 0 ? limit_distance! : meters));
        }
      });
    }
  }, [checkUser, dispatch, id, limit_distance, meters]);

  const handleAcceptHelp = useCallback(
    (requestId: number, acceptId: number, message: string, isPetition: boolean) => {
      dispatch(setLoadingRequest(true));
      acceptHelp({ requestId, acceptId, message, isPetition })(dispatch)
        .then(() => {
          dispatch(setLoadingRequest(false));
          dispatch(
            setModalRequest({
              status: false,
              handleRequest: null,
              user: {},
            }),
          );
          setModalPetition({
            status: false,
            user: {},
            handleRequest: () => null,
          })(dispatch);
        })
        .catch(err => new Error(err));
    },
    [dispatch],
  );

  // Start chat when user accept request
  const handleChat = useCallback(
    (participant: IUser) => {
      dispatch(setChatLoading(true));
      setChat(
        participant.petition ? participant.id! : id!,
        participant.petition ? id! : participant.id!,
        participant.petition ? participant.petition.id : null,
      )(dispatch)
        .then((room: IUserRooms) => {
          return remove(
            id!,
            participant.id!,
          )(dispatch).then(() => {
            dispatch(
              setModalRequest({
                status: false,
                handleRequest: null,
                user: {},
              }),
            );
            dispatch(getUserRooms(id!));
            dispatch(getUsersByLocation(id!, meters === 0 ? limit_distance! : meters));
            dispatch(setChatLoading(false));
            navigationRef.current!.navigate('Chat', { room, sender: room.user_participant });
          });
        })
        .catch(error => {
          const errorMessage = Message.exception(error);
          setToast({
            payload: {
              message: errorMessage,
              status: true,
              type: 'error',
            },
          })(dispatch);
        });
    },
    [dispatch, id, limit_distance, meters],
  );

  const handleRequest = useCallback(
    (type: string, requestUser: IUser, acceptId: number, isPetition: boolean) => () =>
      type === 'request'
        ? handleAcceptHelp(requestUser.id!, acceptId, 'Quiere ayudarte', isPetition)
        : handleChat(requestUser),
    [handleAcceptHelp, handleChat],
  );

  const handleNotification = useCallback(
    (notify: INotification) => {
      navigationRef.current!.navigate('Location');
      const user = notify.user!;
      setToastNotification({
        userNotification: user.first_name,
        status: false,
        type: 'error',
        handle: () => null,
      })(dispatch);
      setModalPetition({
        status: true,
        user,
        handleRequest: handleRequest('request', user!, id!, true),
      })(dispatch);
    },
    [dispatch, handleRequest, id],
  );

  useEffect(() => {
    init();
    setI18nConfig();
    return () => {
      if (checkUser && id! > 0) {
        chatAlertSocket.emit('leaveUserAlert', { userId: id });
        chatAlertSocket.disconnect();
      }
    };
  }, [checkUser, id, init]);

  // Kill state open aswell
  const onOpenNotification = useCallback(
    (noti: IOpenNotification & INotification) => {
      if (noti.isAlert) {
        navigationRef.current!.navigate('Chat', { room: noti.room, sender: noti.user });
      }

      if (noti.data) {
        const notify = JSON.parse(String(noti.data.notification));
        if (getCurrentNameRoute() === 'Location') {
          const user = notify.user!;
          // dispatch(setRecievedNotification(notify.notificationId!));
          if (notify.type === 'request' && notify.isPetition) {
            setModalPetition({
              status: true,
              user,
              handleRequest: handleRequest(notify.type, user!, id!, true),
            })(dispatch);
          } else {
            dispatch(
              setModalRequest({
                status: true,
                type: notify.type,
                handleRequest: handleRequest(notify.type!, user!, id!, false),
                user: user!,
                isPetition: notify.isPetition,
              }),
            );
          }
        } else {
          const user = notify.user;
          if (notify.type === 'request') {
            setToastNotification({
              userNotification: user?.first_name,
              status: true,
              type: 'error',
              handle: () => handleNotification(notify),
            })(dispatch);
          }
          if (notify.type === 'accept') {
            navigationRef.current!.navigate('Location');
            dispatch(
              setModalRequest({
                status: true,
                type: notify.type,
                handleRequest: handleRequest(notify.type!, user!, id!, false),
                user: user!,
                isPetition: notify.isPetition,
              }),
            );
          }
        }
      }
    },
    [dispatch, handleNotification, handleRequest, id],
  );

  // Open Notification with Background aswell
  const onOpenFirebaseNotification = useCallback(
    async (noti: IOpenNotification) => {
      if (noti.data) {
        const notify = JSON.parse(String(noti.data.notification));
        if (notify.isAlert) {
          navigationRef.current!.navigate('Chat', { room: notify.room, sender: notify.user });
        } else {
          if (getCurrentNameRoute() === 'Location') {
            const user = notify.user!;
            if (notify.type === 'request' && notify.isPetition) {
              setModalPetition({
                status: true,
                user,
                handleRequest: handleRequest(notify.type, user!, id!, true),
              })(dispatch);
            } else {
              dispatch(
                setModalRequest({
                  status: true,
                  type: notify.type,
                  handleRequest: handleRequest(notify.type!, user!, id!, false),
                  user: user!,
                  isPetition: notify.isPetition,
                }),
              );
            }
          } else {
            const user = notify.user;
            if (notify.type === 'request') {
              setToastNotification({
                userNotification: user?.first_name,
                status: true,
                type: 'error',
                handle: () => handleNotification(notify),
              })(dispatch);
            }
            if (notify.type === 'accept') {
              navigationRef.current!.navigate('Location');
              dispatch(
                setModalRequest({
                  status: true,
                  type: notify.type,
                  handleRequest: handleRequest(notify.type!, user!, id!, false),
                  user: user!,
                  isPetition: notify.isPetition,
                }),
              );
            }
          }
        }
      }
    },
    [dispatch, handleNotification, handleRequest, id],
  );

  // Notifications when App is Active
  const onNotification = useCallback(
    (notify: IOpenNotification) => {
      const options = {
        soundName: 'default',
        playSound: true,
      };
      const notification = JSON.parse(String(notify.data.notification));
      const user = notification.user!;
      if (notification.isAlert) {
        if (checkAlertNotificationRoute()) {
          dispatch(getUserRooms(id!));
          dispatch(setNotificationIcon(true));
          dispatch(getUsersByLocation(id!, meters!));
          localNotificationService.showNotification(
            notification.id,
            notification.title,
            notification.text,
            notification,
            options,
          );
        }
      } else {
        if (getCurrentNameRoute() === 'Location') {
          if (notification.type === 'request' && notification.isPetition) {
            setModalPetition({
              status: true,
              user,
              handleRequest: handleRequest(notification.type, user!, id!, true),
            })(dispatch);
          } else {
            dispatch(
              setModalRequest({
                status: true,
                type: notification.type,
                handleRequest: handleRequest(notification.type!, user!, id!, false),
                user: user!,
                isPetition: notification.isPetition,
              }),
            );
          }
        } else {
          if (notification.type === 'request') {
            setToastNotification({
              userNotification: user?.first_name,
              status: true,
              type: 'error',
              handle: () => handleNotification(notification),
            })(dispatch);
          }
          if (notification.type === 'accept') {
            localNotificationService.showNotification(
              notification.id,
              notification.title,
              notification.body,
              notify,
              options,
            );
          }
        }
      }
    },
    [dispatch, handleNotification, handleRequest, id, meters],
  );

  const onRegister = useCallback(
    (token: string) => {
      if (id! > 0) {
        dispatch(updateFcm(id!, { fcm: token }));
      }
    },
    [dispatch, id],
  );

  const backgroundMessageHandler = (_: FirebaseMessagingTypes.RemoteMessage) => {};

  useEffect(() => {
    if (checkUser && id! > 0) {
      fcmService
        .registerAppWithFCM()
        .then(res => res)
        .catch(err => new Error(err));
      fcmService.register(
        onRegister,
        onNotification,
        onOpenFirebaseNotification,
        backgroundMessageHandler,
      );
      localNotificationService
        .configure(onOpenNotification)
        .then(res => res)
        .catch(err => new Error(err));
    }
  }, [checkUser, id, onNotification, onOpenFirebaseNotification, onOpenNotification, onRegister]);

  useEffect(() => {
    checkLogin()(dispatch)
      .then(res => res)
      .catch(err => err);
  }, [dispatch]);

  const generalRefresh = useCallback(() => {
    dispatch(getUserRooms(id!));
    dispatch(getUsersByLocation(id!, meters === 0 ? limit_distance! : meters));
  }, [dispatch, id, limit_distance, meters]);

  const handleAppStateChange = useCallback(
    (nextAppState: AppStateStatus) => {
      if (checkUser && id! > 0 && nextAppState === 'active') {
        // Notification to refresh rooms list
        generalRefresh();
      }
    },
    [checkUser, generalRefresh, id],
  );

  useEffect(() => {
    AppState.addEventListener('change', handleAppStateChange);
    return () => {
      AppState.removeEventListener('change', handleAppStateChange);
    };
  }, [checkUser, handleAppStateChange, id]);

  const getInitialRoute = (): string => {
    if (checkUser) {
      if (first_time) {
        return 'Starter';
      }
      return 'TabNavigator';
    }
    return 'Main';
  };

  return checkUserLoading ? (
    <MainLoader />
  ) : (
    <NavigationContainer ref={navigationRef}>
      <Navigator screenOptions={{ headerShown: false }} initialRouteName={getInitialRoute()}>
        <Screen name="Login" component={Login} />
        <Screen name="Main" component={Main} />
        <Screen name="Register" component={Register} />
        <Screen name="ForgotPassword" component={ForgotPassword} />
        <Screen name="MyLocation" component={MyLocation} />
        <Screen name="TabNavigator" component={BottomTabNavigator} />
        <Screen name="Chat" component={Chat} />
        <Screen name="Profile" component={Profile} />
        <Screen name="EditProfile" component={EditProfile} />
        <Screen name="Starter" component={Starter} />
        <Screen name="ChooseLanguage" component={ChooseLanguage} />
        <Screen name="ChooseNationality" component={ChooseNationality} />
        <Screen name="ChooseInterest" component={ChooseInterest} />
        <Screen name="ChooseSettings" component={ChooseSettings} />
        <Screen name="ChooseGender" component={ChooseGender} />
        <Screen name="Languages" component={Languages} />
        <Screen name="Nationalities" component={Nastionalities} />
        <Screen name="Petition" component={Petition} />
        <Screen name="PetitionDetail" component={PetitionDetail} />
        <Screen name="Interest" component={Interest} />
        <Screen name="ListOnlyHelp" component={ListOnlyHelp} />
        <Screen name="ChangeSettings" component={ChangeSettings} />
      </Navigator>
      <Toast />
      <ToastNotification />
      <Modal />
    </NavigationContainer>
  );
};
