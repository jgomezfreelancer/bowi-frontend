export const ACTIONS = {
  STATUS: 'modal/status',
};

interface IStore {
  dispatch: (someFunc: any) => void;
}

type Payload = {
  status: boolean;
  children: React.ReactNode;
};

type SetMotalArg = {
  payload: Payload;
};

export const setModal = (value: SetMotalArg) => (dispatch: IStore['dispatch']) => {
  dispatch({ type: ACTIONS.STATUS, ...value });
};

export default setModal;
