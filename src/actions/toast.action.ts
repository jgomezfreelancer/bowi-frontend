export const ACTIONS = {
  STATUS: 'toast/status',
};

interface IStore {
  dispatch: (someFunc: any) => void;
}

export const setToast = (value: object) => (dispatch: IStore['dispatch']) => {
  dispatch({ type: ACTIONS.STATUS, ...value });
};

export default setToast;
