declare module '*.svg' {
  import React from 'react';
  import { SvgProps } from 'react-native-svg';
  const content: React.FC<SvgProps>;
  export default content;
}

declare module 'random-location';
declare module 'react-native-stars';
declare module 'react-native-image-picker';
