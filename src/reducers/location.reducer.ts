import { LatLng } from 'react-native-maps';
import { IDispatch } from '../interfaces/IDispatch';
import { IUser } from './login.reducer';

const SET_LOCATION = 'location/set_location';
const SET_MARKER_LOCATION = 'location/set_marker_location';
const SET_CAMERA = 'location/set_camera';
const SET_MODAL_REQUEST = 'location/set_modal_request';
const SET_MODAL_PETITION = 'location/set_modal_petition';

interface SetLocation {
  type: typeof SET_LOCATION;
  payload: { location: ILocation; meters: number };
}

interface SetMarkerLocation {
  type: typeof SET_MARKER_LOCATION;
  payload: ILocation;
}

interface SetCamera {
  type: typeof SET_CAMERA;
  payload: number;
}

interface SetModalRequest {
  type: typeof SET_MODAL_REQUEST;
  payload: IModalRequest;
}

interface SetModalPetition {
  type: typeof SET_MODAL_PETITION;
  payload: IModalPetition;
}

type ActionTypes = SetLocation | SetModalRequest | SetCamera | SetModalPetition | SetMarkerLocation;

export interface ILocation extends LatLng {
  latitude: number;
  longitude: number;
  latitudeDelta: number;
  longitudeDelta: number;
}
export interface IModalRequest {
  status: boolean;
  type?: 'request' | 'accept' | 'alert';
  user: IUser;
  isPetition?: boolean;
  handleRequest?: null | (() => void);
}

export interface IModalPetition {
  status: boolean;
  type?: 'request' | 'accept' | 'alert';
  user: IUser;
  handleRequest?: null | (() => void);
}

export type IState = {
  location: ILocation;
  markerLocation: ILocation;
  meters: number;
  modalRequest: IModalRequest;
  modalPetition: IModalPetition;
  cameraZoom: number;
};

const initialStateUser = {
  id: 0,
  username: '',
  first_name: '',
  last_name: '',
  social_auth: 0,
  location: 0,
  profile: 0,
  help: 0,
  language_id: 0,
  nationality_id: 0,
  latitude: 0,
  longitude: 0,
  meters: 0,
  petition: null,
};

const initialState: IState = {
  location: {
    latitude: 0,
    longitude: 0,
    latitudeDelta: 0,
    longitudeDelta: 0,
  },
  markerLocation: {
    latitude: 0,
    longitude: 0,
    latitudeDelta: 0,
    longitudeDelta: 0,
  },
  meters: 0,
  modalRequest: {
    status: false,
    type: 'request',
    user: initialStateUser,
    isPetition: false,
    handleRequest: () => null
  },
  modalPetition: {
    status: false,
    type: 'request',
    user: initialStateUser,
    handleRequest: () => null
  },
  cameraZoom: 18,
};

export const setLocation = (location: ILocation, meters: number) => ({
  type: SET_LOCATION,
  payload: { location, meters },
});

export const setMarkerLocation = (location: ILocation) => ({
  type: SET_MARKER_LOCATION,
  payload: location,
});


export const setCamera = (camera: number) => ({
  type: SET_CAMERA,
  payload: camera,
});

export const setModalRequest = (payload: IModalRequest) => (dispatch: IDispatch<ActionTypes>) =>
  dispatch({
    type: SET_MODAL_REQUEST,
    payload,
  });

export const setModalPetition = (payload: IModalPetition) => (dispatch: IDispatch<ActionTypes>) =>
  dispatch({
    type: SET_MODAL_PETITION,
    payload,
  });

const locationReducer = (state = initialState, action: ActionTypes) => {
  switch (action.type) {
    case SET_LOCATION:
      return {
        ...state,
        location: action.payload.location,
        meters: action.payload.meters,
      };
    case SET_CAMERA:
      return {
        ...state,
        cameraZoom: action.payload,
      };
      case SET_MARKER_LOCATION:
        return {
          ...state,
          markerLocation: action.payload,
        };
    case SET_MODAL_REQUEST:
      return {
        ...state,
        modalRequest: action.payload,
      };
          case SET_MODAL_PETITION:
      return {
        ...state,
        modalPetition: action.payload,
      };
    default:
      return state;
  }
};

export default locationReducer;
