import API from '../api/Nationality';
import { IDispatch } from '../interfaces/IDispatch';

const GET_ALL = 'nationality/get_all';
const LOADING = 'nationality/loading';


interface INationality {
  id: number;
  description: string;
  slug: string;
  image: string;
}


interface GetAll {
  type: typeof GET_ALL;
  payload: INationality[];
}

interface Loading {
  type: typeof LOADING;
  payload: boolean;
}

type ActionTypes = GetAll | Loading;

export type IState = {
  list: INationality[];
  loading: boolean;
};

const initialState: IState = {
  list: [],
  loading: false,
};

export const getAll = () => async (dispatch: IDispatch<ActionTypes>) => {
  dispatch({
    type: LOADING,
    payload: true,
  });
  try {
    const { data } = await API.getAll();
    dispatch({
      type: GET_ALL,
      payload: data,
    });
    dispatch({
      type: LOADING,
      payload: false,
    });
    return data;
  } catch (error) {
    return error;
  }
};

const nationalityReducer = (state = initialState, action: ActionTypes) => {
  switch (action.type) {
    case GET_ALL:
      return {
        ...state,
        list: action.payload,
      };

    case LOADING:
      return {
        ...state,
        loading: action.payload,
      };
    default:
      return state;
  }
};

export default nationalityReducer;
