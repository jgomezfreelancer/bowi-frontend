import setToast from '../actions/toast.action';
import API from '../api/Notification';
import { IDispatch } from '../interfaces/IDispatch';
import { IUser } from './login.reducer';
import Message from '../helpers/message';

const GET_FAILURE_NOTIFICATION = 'notification/get_failure_notification';
const SET_STATUS_ICON_NOTIFICATION = 'notification/set_status_icon_notification';

export interface IFailureNotification {
  id: string;
  user_id: number;
  data: INotification;
  message_id: string;
  recieved: number;
  type: 'request' | 'accept' | 'alert' | 'petition';
}

export interface INotification {
  id?: number;
  title?: string;
  type?: 'request' | 'accept' | 'alert' | 'petition';
  text?: string;
  user?: IUser;
  smallIcon?: string;
  largeIcon?: string;
  autoCancel?: boolean;
  color?: string;
  headsUp?: boolean;
  sound?: string;
  vibration?: number;
  vibrate?: boolean;
  playSound?: boolean;
  soundName?: string;
  isRequest?: boolean;
  isAlert?: boolean;
  notificationId?: number;
  room?: number;
  isPetition?: boolean;
}

export interface IDataNotification {
  notification: INotification;
  priority: string;
}

export interface INotificationBody {
  android: any;
  body: string;
  title: string;
}

export interface IOpenNotification {
  notification: INotificationBody;
  sentTime: number;
  data: IDataNotification
  from: string;
  messageId: string;
  ttl: number;
  collapseKey: string
}

export interface INotify {
  id?: number;
  alertMessage?: boolean;
  messageId?: string;
  ttl?: number;
  from?: string;
  sentTime?: number;
  notification: any;
  threadId?: any;
  data?: { notification: INotification };
}

interface GetFailureNotification {
  type: typeof GET_FAILURE_NOTIFICATION;
  payload: INotification;
}

interface SetStatusIconNotification {
  type: typeof SET_STATUS_ICON_NOTIFICATION;
  payload: boolean;
}

type ActionTypes = GetFailureNotification | SetStatusIconNotification;

export type IState = {
  failureNotification: INotification;
  statusIconNotification: boolean;
};

const initialState: IState = {
  failureNotification: {},
  statusIconNotification: false,
};

export const setRecievedNotification = (id: number) => async (dispatch: IDispatch<ActionTypes>) => {
  try {
    const { data } = await API.setRecievedNotification(id);
    return data;
  } catch (error) {
    const message = Message.exception(error);
    setToast({
      payload: {
        message,
        status: true,
        type: 'error',
      },
    })(dispatch);
    return error;
  }
};

export const remove = (requestId: number, acceptId: number) => async (dispatch: IDispatch<ActionTypes>) => {
  try {
    const { data } = await API.remove(requestId, acceptId);
    return data;
  } catch (error) {
    const message = Message.exception(error);
    setToast({
      payload: {
        message,
        status: true,
        type: 'error',
      },
    })(dispatch);
    return error;
  }
};

export const getFailureNotification = (id: number) => async (dispatch: IDispatch<ActionTypes>) => {
  try {
    const { data } = await API.getFailureNotification(id);
    return data;
  } catch (error) {
    const message = Message.exception(error);
    setToast({
      payload: {
        message,
        status: true,
        type: 'error',
      },
    })(dispatch);
    return error;
  }
};

export const setNotificationIcon = (active: boolean) => async (
  dispatch: IDispatch<ActionTypes>,
) => {
  dispatch({
    type: SET_STATUS_ICON_NOTIFICATION,
    payload: active,
  });
};

export const checkPendingNotification = (acceptId: number, requestId: number) => async (dispatch: IDispatch<ActionTypes>) => {
  try {
    const { data } = await API.checkPendingNotification(acceptId, requestId);
    return data;
  } catch (error) {
    const message = Message.exception(error);
    setToast({
      payload: {
        message,
        status: true,
        type: 'error',
      },
    })(dispatch);
    return error;
  }
};

const notificationReducer = (state = initialState, action: ActionTypes) => {
  switch (action.type) {
    case GET_FAILURE_NOTIFICATION:
      return {
        ...state,
        failureNotification: action.payload,
      };
    case SET_STATUS_ICON_NOTIFICATION:
      return {
        ...state,
        statusIconNotification: action.payload,
      };
    default:
      return state;
  }
};

export default notificationReducer;
