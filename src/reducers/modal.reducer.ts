import { ACTIONS } from '../actions/modal.action';

export interface IState {
  status: boolean;
  children: JSX.Element | null;
}
interface SetModal {
  type: typeof ACTIONS.STATUS;
  payload: IState;
}

type ActionTypes = SetModal;

const initialState: IState = {
  status: false,
  children: null,
};

const modalReducer = (state = initialState , action: ActionTypes) => {
  switch (action.type) {
    case ACTIONS.STATUS:
      return {
        ...state,
        ...action.payload,
      };
    default:
      return state;
  }
};

export default modalReducer;