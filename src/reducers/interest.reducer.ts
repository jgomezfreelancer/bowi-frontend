import API from '../api/Interest';
import { IDispatch } from '../interfaces/IDispatch';

const GET_ALL = 'interest/get_all';
const LOADING = 'interest/loading';

interface IInterest {
  id: number;
  description: string;
  slug: string;
  image: string;
}

interface GetAll {
  type: typeof GET_ALL;
  payload: IInterest[];
}

interface Loading {
  type: typeof LOADING;
  payload: boolean;
}

type ActionTypes = GetAll | Loading;

export type IState = {
  list: IInterest[];
  loading: boolean;
};

const initialState: IState = {
  list: [],
  loading: false,
};

export const getAll = () => async (dispatch: IDispatch<ActionTypes>) => {
  dispatch({
    type: LOADING,
    payload: true,
  });
  try {
    const { data } = await API.getAll();
    dispatch({
      type: GET_ALL,
      payload: data,
    });
    dispatch({
      type: LOADING,
      payload: false,
    });
    return data;
  } catch (error) {
    return error;
  }
};

const interestReducer = (state = initialState, action: ActionTypes) => {
  switch (action.type) {
    case GET_ALL:
      return {
        ...state,
        list: action.payload,
      };

    case LOADING:
      return {
        ...state,
        loading: action.payload,
      };
    default:
      return state;
  }
};

export default interestReducer;
