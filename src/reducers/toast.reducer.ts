import { ACTIONS } from '../actions/toast.action';

export interface IState {
  status: boolean;
  message: string;
  type: string;
}
interface SetToast {
  type: typeof ACTIONS.STATUS;
  payload: IState;
}

export type ActionTypes = SetToast;

const initialState: IState = {
  status: false,
  message: '',
  type: '',
};

const toastReducer = (state = initialState, action: ActionTypes) => {
  switch (action.type) {
    case ACTIONS.STATUS:
      return {
        ...state,
        ...action.payload,
      };
    default:
      return state;
  }
};

export default toastReducer;
