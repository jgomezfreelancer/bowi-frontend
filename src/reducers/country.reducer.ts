import API from '../api/Country';
import { IDispatch } from '../interfaces/IDispatch';

const GET_ALL = 'country/get_all';
const LOADING = 'country/loading';


export interface ICountry {
  id: number;
  description: string;
}


interface GetAll {
  type: typeof GET_ALL;
  payload: ICountry[];
}

interface Loading {
  type: typeof LOADING;
  payload: boolean;
}

type ActionTypes = GetAll | Loading;

export type IState = {
  list: ICountry[];
  loading: boolean;
};

const initialState: IState = {
  list: [],
  loading: false,
};

export const getAll = () => async (dispatch: IDispatch<ActionTypes>) => {
  dispatch({
    type: LOADING,
    payload: true,
  });
  try {
    const { data } = await API.getAll();
    dispatch({
      type: GET_ALL,
      payload: data,
    });
    dispatch({
      type: LOADING,
      payload: false,
    });
    return data;
  } catch (error) {
    return error;
  }
};

const countryReducer = (state = initialState, action: ActionTypes) => {
  switch (action.type) {
    case GET_ALL:
      return {
        ...state,
        list: action.payload,
      };

    case LOADING:
      return {
        ...state,
        loading: action.payload,
      };
    default:
      return state;
  }
};

export default countryReducer;
