import API from '../api/Room';
import { IDispatch } from '../interfaces/IDispatch';
import { IUser, IUserPetition } from './login.reducer';
import Message from '../helpers/message';
import setToast from '../actions/toast.action';
import { IMessage } from './message.reducer';

const GET_USER_ROOMS = 'room/get_user_rooms';
const LOADING = 'room/loading';
const GET_EXPIRATION = 'room/get_expiration';
const SET_CHAT_LOADING = 'room/set_chat_loading';
const SET_ROOM_OPTIONS_MODAL = 'room/set_room_options_modal';
const SET_STATUS_OPTION_MODAL = 'room/set_status_option_modal';

export interface IUserRooms {
  id: number;
  description: string;
  responsible_id: number;
  participant_id: number;
  responsible: IUser;
  user_participant: IUser;
  user: IUser;
  lastMessage: IMessage;
  pendingViews: number;
  petition_id: number;
  petition: IUserPetition;
  disable: boolean;
  userPetition: IUser;
  userImage: string;
}

interface IBottomModal {
  statusModal: boolean;
  selectedRoom: IUserRooms;
}
interface GetUserRooms {
  type: typeof GET_USER_ROOMS;
  payload: IUserRooms[];
}

interface SetStatusOptionsModal {
  type: typeof SET_STATUS_OPTION_MODAL;
  payload: boolean;
}

interface SetRoomOptionsModal {
  type: typeof SET_ROOM_OPTIONS_MODAL;
  payload: IBottomModal;
}

interface Loading {
  type: typeof LOADING;
  payload: boolean;
}

interface SetChatLoading {
  type: typeof SET_CHAT_LOADING;
  payload: boolean;
}

interface GetExpiration {
  type: typeof GET_EXPIRATION;
  payload: number;
}

type ActionTypes =
  | GetUserRooms
  | Loading
  | GetExpiration
  | SetChatLoading
  | SetRoomOptionsModal
  | SetStatusOptionsModal;

export type IState = {
  userRooms: IUserRooms[];
  loading: boolean;
  roomExpiration: number;
  statusOptionModal: boolean;
  setChatLoading: boolean;
  roomModalOptions: IBottomModal;
};

const initialState: IState = {
  userRooms: [],
  loading: false,
  roomExpiration: 0,
  setChatLoading: false,
  statusOptionModal: false,
  roomModalOptions: {
    statusModal: false,
    selectedRoom: {},
  },
};

export const getUserRooms = (id: number) => async (dispatch: IDispatch<ActionTypes>) => {
  dispatch({
    type: LOADING,
    payload: true,
  });
  try {
    const { data } = await API.getUserRooms(id);
    dispatch({
      type: GET_USER_ROOMS,
      payload: data,
    });
    dispatch({
      type: LOADING,
      payload: false,
    });
    return data;
  } catch (error) {
    return error;
  }
};

export const setUserRooms = (rooms: IUserRooms[]) => async (dispatch: IDispatch<ActionTypes>) => {
  dispatch({
    type: GET_USER_ROOMS,
    payload: rooms,
  });
};

export const setRoomOptionsModal = (payload: IBottomModal) => async (
  dispatch: IDispatch<ActionTypes>,
) => {
  dispatch({
    type: SET_ROOM_OPTIONS_MODAL,
    payload,
  });
};

export const setChatLoading = (loading: boolean) => async (dispatch: IDispatch<ActionTypes>) => {
  dispatch({
    type: SET_CHAT_LOADING,
    payload: loading,
  });
};

export const setChat = (
  responsible: number,
  participant: number,
  petitionId: number | null,
) => async (dispatch: IDispatch<ActionTypes>) => {
  try {
    const { data } = await API.setChat(responsible, participant, petitionId);

    return data;
  } catch (error) {
    const message = Message.exception(error);
    setToast({
      payload: {
        message,
        status: true,
        type: 'error',
      },
    })(dispatch);
    return error;
  }
};

export const remove = (id: number, showMessage: boolean = false) => async (
  dispatch: IDispatch<ActionTypes>,
) => {
  try {
    const { data, status } = await API.remove(id);
    let response: any = [];
    if (status === 200) {
      response = {
        data,
        status,
      };
      if (showMessage) {
        setToast({
          payload: {
            message: 'Chat finalizado',
            status: true,
            type: 'success',
          },
        })(dispatch);
      }
    }
    return response;
  } catch (error) {
    const message = Message.exception(error);
    setToast({
      payload: {
        message,
        status: true,
        type: 'error',
      },
    })(dispatch);
    return error;
  }
};

export const disableChat = (id: number) => async (dispatch: IDispatch<ActionTypes>) => {
  try {
    const { data, status } = await API.disable(id);
    let response: any = [];
    if (status === 200) {
      response = {
        data,
        status,
      };
    }
    return response;
  } catch (error) {
    const message = Message.exception(error);
    setToast({
      payload: {
        message,
        status: true,
        type: 'error',
      },
    })(dispatch);
    return error;
  }
};

export const checkExpiration = (id: number) => async (dispatch: IDispatch<ActionTypes>) => {
  dispatch({
    type: LOADING,
    payload: true,
  });
  try {
    const { data } = await API.checkExpiration(id);
    dispatch({
      type: GET_EXPIRATION,
      payload: data,
    });
    dispatch({
      type: LOADING,
      payload: false,
    });
    return data;
  } catch (error) {
    return error;
  }
};

export const blockRoomUser = (body: { authUser: number; blockUser: number }) => async (
  dispatch: IDispatch<ActionTypes>,
) => {
  dispatch({
    type: LOADING,
    payload: false,
  });
  try {
    const { data } = await API.blockRoomUser(body);
    dispatch({
      type: LOADING,
      payload: false,
    });
    return data;
  } catch (error) {
    return error;
  }
};

export const reportRoomUser = (body: {
  authUser: number;
  reportUser: number;
  reason: string;
}) => async (dispatch: IDispatch<ActionTypes>) => {
  dispatch({
    type: LOADING,
    payload: false,
  });
  try {
    const { data } = await API.reportRoomUser(body);
    dispatch({
      type: LOADING,
      payload: false,
    });
    return data;
  } catch (error) {
    return error;
  }
};

const roomReducer = (state = initialState, action: ActionTypes) => {
  switch (action.type) {
    case GET_USER_ROOMS:
      return {
        ...state,
        userRooms: action.payload,
      };
    case GET_EXPIRATION:
      return {
        ...state,
        roomExpiration: action.payload,
      };
    case LOADING:
      return {
        ...state,
        loading: action.payload,
      };
    case SET_CHAT_LOADING:
      return {
        ...state,
        setChatLoading: action.payload,
      };
    case SET_STATUS_OPTION_MODAL:
      return {
        ...state,
        statusOptionModal: action.payload,
      };
    case SET_ROOM_OPTIONS_MODAL:
      return {
        ...state,
        roomModalOptions: action.payload,
      };
    default:
      return state;
  }
};

export default roomReducer;
