import { IDispatch } from '../interfaces/IDispatch';

const STATUS = 'toast-notification/status';

export interface IToastSettings {
  status?: boolean;
  userNotification?: string;
  type?: string;
  handle?: () => void | null
}
interface SetToast {
  type: typeof STATUS;
  payload: IToastSettings;
}

export type ActionTypes = SetToast;

export interface IState {
  settings: IToastSettings;
}

const initialState: IState = {
  settings: {
    status: false,
    userNotification: '',
    type: '',
    handle: () => null
  }
};

export const setToastNotification = (settings: IToastSettings) => (dispatch: IDispatch<ActionTypes>) => {
  dispatch({ type: STATUS, payload: settings });
};


const toastNotificationReducer = (state = initialState, action: ActionTypes) => {
  switch (action.type) {
    case STATUS:
      return {
        ...state,
        settings: action.payload,
      };
    default:
      return state;
  }
};

export default toastNotificationReducer;
