import { combineReducers } from 'redux';

import loginReducer, { IState as LoginState } from './login.reducer';
import toastReducer, { IState as ToastState } from './toast.reducer';
import modalReducer, { IState as ModaltState } from './modal.reducer';
import languageReducer, { IState as LanguageState } from './language.reducer';
import nationalityReducer, { IState as NationalityState } from './nationality.reducer';
import locationReducer, { IState as LocationState } from './location.reducer';
import notificationReducer, { IState as NotificationState } from './notification.reducer';
import roomReducer, { IState as RoomState } from './room.reducer';
import messageReducer, { IState as MessageState } from './message.reducer';
import countryReducer, { IState as CountryState } from './country.reducer';
import rankingReducer, { IState as RankingState } from './ranking.reducer';
import toastNotificationReducer, {
  IState as ToastNotificationState,
} from './toastNotification.reducer';
import interestReducer, { IState as InterestState } from './interest.reducer';

const rootReducer = combineReducers({
  loginReducer,
  toastReducer,
  modalReducer,
  languageReducer,
  nationalityReducer,
  locationReducer,
  notificationReducer,
  roomReducer,
  messageReducer,
  countryReducer,
  rankingReducer,
  toastNotificationReducer,
  interestReducer,
});

export type AppRootReducer = {
  loginReducer: LoginState;
  toastReducer: ToastState;
  modalReducer: ModaltState;
  languageReducer: LanguageState;
  nationalityReducer: NationalityState;
  locationReducer: LocationState;
  notificationReducer: NotificationState;
  roomReducer: RoomState;
  messageReducer: MessageState;
  countryReducer: CountryState;
  rankingReducer: RankingState;
  toastNotificationReducer: ToastNotificationState;
  interestReducer: InterestState;
};

export default rootReducer;
