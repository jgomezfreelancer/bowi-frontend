import API from '../api/Ranking';
import Message from '../helpers/message';
import setToast from '../actions/toast.action';
import { IDispatch } from '../interfaces/IDispatch';

const GET_RANKING_BY_USER = 'ranking/get_ranking_by_user';

export interface IRanking {
  user_id: number | null;
  type: number;
}

interface GetRankingByUser {
  type: typeof GET_RANKING_BY_USER;
  payload: number;
}

type ActionTypes = GetRankingByUser;

export type IState = {
  ranking: number;
};

const initialState: IState = {
  ranking: 0,
};

export const create = (body: IRanking) => async (dispatch: IDispatch<ActionTypes>) => {
  try {
    const { data } = await API.create(body);
    return data;
  } catch (error) {
    const message = Message.exception(error);
    setToast({
      payload: {
        message,
        status: true,
        type: 'error',
      },
    })(dispatch);
    return error;
  }
};

export const getRankingByUser = (id: number) => async (dispatch: IDispatch<ActionTypes>) => {
  try {
    const { data } = await API.getRankingByUser(id);
    dispatch({
      type: GET_RANKING_BY_USER,
      payload: data,
    });
    return data;
  } catch (error) {
    const message = Message.exception(error);
    setToast({
      payload: {
        message,
        status: true,
        type: 'error',
      },
    })(dispatch);
    return error;
  }
};

const rankingReducer = (state = initialState, action: ActionTypes) => {
  switch (action.type) {
    case GET_RANKING_BY_USER:
      return {
        ...state,
        ranking: action.payload,
      };
    default:
      return state;
  }
};

export default rankingReducer;
