import API from '../api/Message';
import { IDispatch } from '../interfaces/IDispatch';
import { IUser } from './login.reducer';

const GET = 'message/get';
const LOADING = 'message/loading';

export interface IMessage {
  id: number;
  description: string;
  room_id: number;
  user_id: number;
  created: string;
  user: IUser;
  lastMessage: string;
  isMultimedia: boolean;
}

interface Get {
  type: typeof GET;
  payload: IMessage[];
}

interface Loading {
  type: typeof LOADING;
  payload: boolean;
}

type ActionTypes = Get | Loading;

export type IState = {
  messages: IMessage[];
  loading: boolean;
};

const initialState: IState = {
  messages: [],
  loading: false,
};

export const getRoomMessages = (room: number, sender: number) => async (
  dispatch: IDispatch<ActionTypes>,
) => {
  dispatch({
    type: LOADING,
    payload: true,
  });
  try {
    const { data } = await API.getRoomMessages(room, sender);
    dispatch({
      type: GET,
      payload: data,
    });
    dispatch({
      type: LOADING,
      payload: false,
    });
    return data;
  } catch (error) {
    return error;
  }
};

export const setRoomMessages = (messages: IMessage[]) => ({
  type: GET,
  payload: messages,
});

const messageReducer = (state = initialState, action: ActionTypes) => {
  switch (action.type) {
    case GET:
      return {
        ...state,
        messages: action.payload,
      };

    case LOADING:
      return {
        ...state,
        loading: action.payload,
      };
    default:
      return state;
  }
};

export default messageReducer;
