import AsyncStorage from '@react-native-community/async-storage';

import API from '../api/Login';
import Message from '../helpers/message';
import setToast from '../actions/toast.action';
import { IDispatch } from '../interfaces/IDispatch';
import { User } from '@react-native-google-signin/google-signin';
import { customTraslate } from '../utils/language';

/* Actions */

const SET_USER = 'login/set_user';
const GET_USER_IMAGE = 'login/get_user_image';
const GET_USER_PROFILE_IMAGE = 'login/get_user_profile_image';
const LOGOUT = 'login/logout';
const LOADING = 'login/loading';
const CHECK_USER = 'login/check_user';
const CHECK_USER_LOADING = 'login/check_user_loading';
const SET_USERS_BY_LOCATION = 'login/set_users_by_location';
const UPDATE_STARTER_SETTINGS = 'login/update_starter_settings';
const GET_SERVER_DATE = 'login/get_server_date';
const GET_USER_PETITION = 'login/get_user_petition';
const ACCEPT_LOADING = 'login/accept_loading';

export type IUserPetition = {
  id?: number;
  user_id?: number;
  description?: string;
  status?: number;
  created?: string;
};

export type IUser = {
  id?: number;
  username?: string;
  first_name?: string;
  last_name?: string;
  social_auth?: number;
  location?: number;
  profile?: number;
  help?: number;
  language_id?: number | null;
  nationality_id?: number | null;
  latitude?: number;
  longitude?: number;
  meters?: number;
  age?: number;
  address?: string;
  cp?: string;
  country_id?: number;
  password?: string;
  country_location?: string;
  ranking?: number | string;
  profileImage?: string;
  first_time?: boolean;
  languages?: number[];
  nationalities?: number[];
  limit_distance?: number;
  petition?: IUserPetition;
  notificationId?: number;
  userInterestKeys?: number[];
};

export interface IStarterSettings {
  languages?: number[];
  nationalities?: number[];
  profile?: number;
  location?: number;
  limit_distance?: number;
  nationality_id?: number;
  interests?: number[];
}

interface SetUser {
  type: typeof SET_USER;
  payload: IUser;
}

interface GetServerDate {
  type: typeof GET_SERVER_DATE;
  payload: string;
}

interface GetUserPetition {
  type: typeof GET_USER_PETITION;
  payload: IUserPetition;
}

interface Logout {
  type: typeof LOGOUT;
}

interface Loading {
  type: typeof LOADING;
  payload: boolean;
}

interface AcceptLoading {
  type: typeof ACCEPT_LOADING;
  payload: boolean;
}

interface Check {
  type: typeof CHECK_USER;
  payload: boolean;
}

interface CheckLoading {
  type: typeof CHECK_USER_LOADING;
  payload: boolean;
}

interface SetUsersByLocation {
  type: typeof SET_USERS_BY_LOCATION;
  payload: User[];
}

interface GetUserImage {
  type: typeof GET_USER_IMAGE;
  payload: string;
}

interface GetUserProfileImage {
  type: typeof GET_USER_PROFILE_IMAGE;
  payload: string | null;
}

export interface UpdateStarterSettings {
  type: typeof UPDATE_STARTER_SETTINGS;
  payload: IStarterSettings;
}

type ActionTypes =
  | SetUser
  | Logout
  | Loading
  | Check
  | CheckLoading
  | SetUsersByLocation
  | GetUserImage
  | UpdateStarterSettings
  | GetServerDate
  | GetUserPetition
  | GetUserProfileImage
  | AcceptLoading;

export type IState = {
  user: IUser;
  status: boolean;
  loading: boolean;
  checkUser: boolean;
  checkUserLoading: boolean;
  usersByLocation: User[];
  userImage: string;
  starterSettings: IStarterSettings;
  serverDate: string;
  userPetition: IUserPetition;
  userProfileImage: string | null;
  acceptLoading: boolean;
};

const initialState: IState = {
  user: {},
  status: false,
  loading: false,
  checkUser: false,
  checkUserLoading: false,
  usersByLocation: [],
  userImage: '',
  starterSettings: {
    languages: [],
    nationalities: [],
    location: 0,
    profile: 0,
    limit_distance: 0,
    interests: [],
  },
  serverDate: '',
  userPetition: {},
  userProfileImage: null,
  acceptLoading: false,
};

export interface ISocialLogin {
  email: string;
  first_name: string | null;
  last_name: string | null;
}

export interface IFcm {
  fcm: string;
}

export interface ICoord {
  latitude: number;
  longitude: number;
}

export interface ISendHelp {
  id?: number;
  message: string;
  km: number;
  interests: number[];
}

export interface IAcceptHelp {
  requestId: number;
  acceptId: number;
  message: string;
  isPetition: boolean;
}

export interface IRegister {
  first_name: string;
  username: string;
  password: string;
}

export interface IUserInterest {
  userId: number;
  interests: number[];
}

export const login = (body: object) => async (dispatch: IDispatch<ActionTypes>) => {
  dispatch({ type: LOADING, payload: true });
  try {
    const { data, status } = await API.login(body);
    let authResponse: any = [];
    if (status === 200 || status === 201) {
      authResponse = {
        data,
        status,
      };
      const { accessToken, user } = data;
      await AsyncStorage.setItem('token', accessToken);
      dispatch({ type: SET_USER, payload: user });
      dispatch({ type: LOADING, payload: false });
    }
    return authResponse;
  } catch (error) {
    const message = Message.exception(error);
    setToast({
      payload: {
        message,
        status: true,
        type: 'error',
      },
    })(dispatch);
    dispatch({ type: LOADING, payload: false });
    throw message;
  }
};

export const signup = (body: IRegister) => async (dispatch: IDispatch<ActionTypes>) => {
  dispatch({ type: LOADING, payload: true });
  try {
    const { data, status } = await API.signup(body);
    let authResponse: any = [];
    if (status === 200 || status === 201) {
      authResponse = {
        data,
        status,
      };
      const { accessToken, user } = data;
      await AsyncStorage.setItem('token', accessToken);
      dispatch({ type: SET_USER, payload: user });
      dispatch({ type: LOADING, payload: false });
    }
    return authResponse;
  } catch (error) {
    dispatch({ type: LOADING, payload: false });
    throw error;
  }
};

export const sociaLogin = (body: ISocialLogin) => async (dispatch: IDispatch<ActionTypes>) => {
  dispatch({ type: LOADING, payload: true });
  try {
    const { data, status } = await API.socialLogin(body);
    if (status === 200 || status === 201) {
      const { accessToken, user } = data;
      await AsyncStorage.setItem('token', accessToken);
      dispatch({ type: SET_USER, payload: user });
      dispatch({ type: CHECK_USER, payload: true });
      dispatch({ type: LOADING, payload: false });
      return user;
    }
    return {};
  } catch (error) {
    const message = Message.exception(error);
    setToast({
      payload: {
        message,
        status: true,
        type: 'error',
      },
    })(dispatch);
    dispatch({ type: LOADING, payload: false });
    dispatch({ type: CHECK_USER, payload: false });
    throw message;
  }
};

export const logout = (token: string) => async (dispatch: IDispatch<ActionTypes>) => {
  try {
    const { status, data } = await API.logout(token!);
    if (status === 200 || status === 201) {
      await AsyncStorage.clear();
      dispatch({ type: LOGOUT });
    }
    return data;
  } catch (error) {
    return error;
  }
};

export const checkLogin = () => async (dispatch: IDispatch<ActionTypes>) => {
  dispatch({ type: CHECK_USER_LOADING, payload: true });
  try {
    const { data, status } = await API.checkLogin();
    if (status === 200 || status === 201) {
      dispatch({ type: CHECK_USER, payload: true });
      dispatch({ type: SET_USER, payload: data });
    } else {
      dispatch({ type: CHECK_USER, payload: false });
    }
    dispatch({ type: CHECK_USER_LOADING, payload: false });
    return [];
  } catch (error) {
    const token = await AsyncStorage.getItem('token');
    if (token) logout(token)(dispatch);
    dispatch({ type: CHECK_USER, payload: false });
    dispatch({ type: CHECK_USER_LOADING, payload: false });
    return error;
  }
};

export const updateUserSettings = (id: number, body: IUser) => async (
  dispatch: IDispatch<ActionTypes>,
) => {
  dispatch({ type: LOADING, payload: true });
  try {
    const { status, data } = await API.updateSettings(id, body);
    if (status === 200 || status === 201) {
      dispatch({ type: SET_USER, payload: data });
    }
    dispatch({ type: LOADING, payload: false });
    return [];
  } catch (error) {
    const message = Message.exception(error);
    setToast({
      payload: {
        message,
        status: true,
        type: 'error',
      },
    })(dispatch);
    dispatch({ type: LOADING, payload: false });
    return error;
  }
};

export const updateProfile = (id: number, body: IUser) => async (
  dispatch: IDispatch<ActionTypes>,
) => {
  dispatch({ type: LOADING, payload: true });
  try {
    const { status, data } = await API.updateProfile(id, body);
    if (status === 200 || status === 201) {
      dispatch({ type: SET_USER, payload: data });
      setToast({
        payload: {
          status: true,
          type: 'success',
          message: customTraslate('perfilActualizado'),
        },
      })(dispatch);
    }
    dispatch({ type: LOADING, payload: false });
    return [];
  } catch (error) {
    const message = Message.exception(error);
    setToast({
      payload: {
        message,
        status: true,
        type: 'error',
      },
    })(dispatch);
    dispatch({ type: LOADING, payload: false });
    return error;
  }
};

export const updateCoord = (id: number, body: ICoord) => async (
  dispatch: IDispatch<ActionTypes>,
) => {
  dispatch({ type: LOADING, payload: true });
  try {
    const { status } = await API.updateCoord(id, body);
    if (status === 200 || status === 201) {
      dispatch({ type: LOADING, payload: false });
    }
    return [];
  } catch (error) {
    const message = Message.exception(error);
    setToast({
      payload: {
        message,
        status: true,
        type: 'error',
      },
    })(dispatch);
    dispatch({ type: LOADING, payload: false });
    return error;
  }
};

export const updateFcm = (id: number, body: IFcm) => async (dispatch: IDispatch<ActionTypes>) => {
  dispatch({ type: LOADING, payload: true });
  try {
    const { status } = await API.updateFcm(id, body);
    if (status === 200 || status === 201) {
      dispatch({ type: LOADING, payload: false });
    }
    return [];
  } catch (error) {
    const message = Message.exception(error);
    setToast({
      payload: {
        message,
        status: true,
        type: 'error',
      },
    })(dispatch);
    dispatch({ type: LOADING, payload: false });
    return error;
  }
};

export const sendHelp = (body: ISendHelp) => async (dispatch: IDispatch<ActionTypes>) => {
  dispatch({ type: LOADING, payload: true });
  try {
    const { status } = await API.sendHelp(body);
    let response = null;
    if (status === 200 || status === 201) {
      response = status;
    }
    dispatch({ type: LOADING, payload: false });
    return response;
  } catch (error) {
    const message = Message.exception(error);
    const customMessage =
      message === 'Ya posee una peticion activa' ? customTraslate('poseePeticionActiva') : message;
    setToast({
      payload: {
        message: customMessage,
        status: true,
        type: 'error',
      },
    })(dispatch);
    dispatch({ type: LOADING, payload: false });
    throw error;
  }
};

export const acceptHelp = (body: IAcceptHelp) => async (dispatch: IDispatch<ActionTypes>) => {
  try {
    const { status, data } = await API.acceptHelp(body);
    if (status === 200 || status === 201) {
      return data;
    }
    return [];
  } catch (error) {
    const message = Message.exception(error);
    setToast({
      payload: {
        message,
        status: true,
        type: 'error',
      },
    })(dispatch);
    return error;
  }
};

export const getUsersByLocation = (id: number, km: number) => async (
  dispatch: IDispatch<ActionTypes>,
) => {
  try {
    const { status, data } = await API.getUsersByLocation(id, km);
    if (status === 200 || status === 201) {
      dispatch({ type: SET_USERS_BY_LOCATION, payload: data });
    }
    return [];
  } catch (error) {
    return new Error(error);
  }
};

export const cleanUsersByLocation = () => async (dispatch: IDispatch<ActionTypes>) => {
  dispatch({ type: SET_USERS_BY_LOCATION, payload: [] });
};

export const getUserImage = (id: number) => async (dispatch: IDispatch<ActionTypes>) => {
  try {
    const { status, data } = await API.getUserImage(id);
    let res = [];
    if (status === 200 || (status === 201 && data)) {
      res = data;
      dispatch({ type: GET_USER_IMAGE, payload: data });
    }
    return res;
  } catch (error) {
    return new Error(error);
  }
};

export const getUserProfileImage = (id: number) => async (dispatch: IDispatch<ActionTypes>) => {
  try {
    const { status, data } = await API.getUserProfileImage(id);
    let res = [];
    if (status === 200 || (status === 201 && data)) {
      res = data;
      dispatch({ type: GET_USER_PROFILE_IMAGE, payload: data });
    }
    return res;
  } catch (error) {
    return new Error(error);
  }
};

export const cleanUserProfileImage = () => async (dispatch: IDispatch<ActionTypes>) => {
  dispatch({ type: GET_USER_PROFILE_IMAGE, payload: null });
};

export const updateStarterSettings = (settings: IStarterSettings) => async (
  dispatch: IDispatch<ActionTypes>,
) => {
  dispatch({ type: UPDATE_STARTER_SETTINGS, payload: settings });
};

export const updateStarterUser = (id: number, body: IUser) => async (
  dispatch: IDispatch<ActionTypes>,
) => {
  dispatch({ type: LOADING, payload: true });
  try {
    const { status, data } = await API.updateStarterUser(id, body);
    if (status === 200 || status === 201) {
      dispatch({ type: SET_USER, payload: data });
    }
    dispatch({ type: LOADING, payload: false });
    return [];
  } catch (error) {
    const message = Message.exception(error);
    setToast({
      payload: {
        message,
        status: true,
        type: 'error',
      },
    })(dispatch);
    dispatch({ type: LOADING, payload: false });
    return error;
  }
};

export const forgotPassword = (username: string) => async (dispatch: IDispatch<ActionTypes>) => {
  dispatch({ type: LOADING, payload: true });
  try {
    const { status, data } = await API.forgotPassword(username);
    if (status === 200 || (status === 201 && data)) {
      dispatch({ type: LOADING, payload: false });
      setToast({
        payload: {
          status: true,
          type: data.status ? 'success' : 'error',
          message: customTraslate(data.message),
        },
      })(dispatch);
    }
    return data;
  } catch (error) {
    const message = Message.exception(error);
    setToast({
      payload: {
        message,
        status: true,
        type: 'error',
      },
    })(dispatch);
    dispatch({ type: LOADING, payload: false });
  }
};

export const updateLangNationalities = (id: number, body: IStarterSettings) => async (
  dispatch: IDispatch<ActionTypes>,
) => {
  dispatch({ type: LOADING, payload: true });
  try {
    const { status, data } = await API.updateLangNationalities(id, body);
    if (status === 200 || (status === 201 && data)) {
      dispatch({ type: SET_USER, payload: data });
      dispatch({ type: LOADING, payload: false });
    }
    return data;
  } catch (error) {
    const message = Message.exception(error);
    setToast({
      payload: {
        message,
        status: true,
        type: 'error',
      },
    })(dispatch);
    dispatch({ type: LOADING, payload: false });
  }
};

export const getServerDate = () => async (dispatch: IDispatch<ActionTypes>) => {
  try {
    const { status, data } = await API.getServerDate();
    if (status === 200 || (status === 201 && data)) {
      dispatch({ type: GET_SERVER_DATE, payload: data });
    }
    return data;
  } catch (error) {
    const message = Message.exception(error);
    setToast({
      payload: {
        message,
        status: true,
        type: 'error',
      },
    })(dispatch);
    dispatch({ type: LOADING, payload: false });
  }
};

export const getUserPetition = (id: number) => async (dispatch: IDispatch<ActionTypes>) => {
  try {
    const { status, data } = await API.getUserPetition(id);
    if (status === 200 || (status === 201 && data)) {
      dispatch({ type: GET_USER_PETITION, payload: data ? data : {} });
    }
    return data;
  } catch (error) {
    dispatch({ type: GET_USER_PETITION, payload: {} });
    const message = Message.exception(error);
    setToast({
      payload: {
        message,
        status: true,
        type: 'error',
      },
    })(dispatch);
  }
};

export const setUser = (user: IUser) => async (dispatch: IDispatch<ActionTypes>) => {
  dispatch({ type: SET_USER, payload: user });
};

export const setLoadingRequest = (loading: boolean) => async (dispatch: IDispatch<ActionTypes>) => {
  dispatch({ type: ACCEPT_LOADING, payload: loading });
};

export const disablePetition = (id: number) => async (dispatch: IDispatch<ActionTypes>) => {
  try {
    const { data } = await API.disablePetition(id);
    return data;
  } catch (error) {
    const message = Message.exception(error);
    setToast({
      payload: {
        message,
        status: true,
        type: 'error',
      },
    })(dispatch);
  }
};

export const updateUserInterest = (body: IUserInterest) => async (
  dispatch: IDispatch<ActionTypes>,
) => {
  try {
    const { status, data } = await API.updateUserInterest(body);
    if (status === 200 || status === 201) {
      dispatch({ type: SET_USER, payload: data });
    }
    return [];
  } catch (error) {
    const message = Message.exception(error);
    setToast({
      payload: {
        message,
        status: true,
        type: 'error',
      },
    })(dispatch);
    dispatch({ type: LOADING, payload: false });
    return error;
  }
};

const loginReducer = (state = initialState, action: ActionTypes) => {
  switch (action.type) {
    case SET_USER:
      return {
        ...state,
        user: action.payload,
        status: true,
      };
    case GET_USER_IMAGE:
      return {
        ...state,
        userImage: action.payload,
      };
    case GET_USER_PROFILE_IMAGE:
      return {
        ...state,
        userProfileImage: action.payload,
      };
    case UPDATE_STARTER_SETTINGS:
      return {
        ...state,
        starterSettings: action.payload,
      };
    case LOGOUT:
      return {
        ...state,
        ...initialState,
      };
    case LOADING:
      return {
        ...state,
        loading: action.payload,
      };
    case ACCEPT_LOADING:
      return {
        ...state,
        acceptLoading: action.payload,
      };
    case CHECK_USER:
      return {
        ...state,
        checkUser: action.payload,
      };
    case CHECK_USER_LOADING:
      return {
        ...state,
        checkUserLoading: action.payload,
      };
    case SET_USERS_BY_LOCATION:
      return {
        ...state,
        usersByLocation: action.payload,
      };
    case GET_SERVER_DATE:
      return {
        ...state,
        serverDate: action.payload,
      };
    case GET_USER_PETITION:
      return {
        ...state,
        userPetition: action.payload,
      };
    default:
      return state;
  }
};

export default loginReducer;
