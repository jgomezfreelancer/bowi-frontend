import API from '../api/Language';
import { IDispatch } from '../interfaces/IDispatch';

const GET_ALL = 'language/get_all';
const LOADING = 'language/loading';

export interface ILanguage {
    id: number;
    description: string;
    image: string;
    slug: string;
}

interface GetAll {
  type: typeof GET_ALL;
  payload: ILanguage[]
}

interface Loading {
  type: typeof LOADING;
  payload: boolean;
}

type ActionTypes = GetAll | Loading;

export type IState = {
  list: ILanguage[];
  loading: boolean;
};

const initialState: IState = {
  list: [],
  loading: false,
};

export const getAll = () => async (dispatch: IDispatch<ActionTypes>) => {
  dispatch({
    type: LOADING,
    payload: true,
  });
  try {
    const { data } = await API.getAll();
    dispatch({
      type: GET_ALL,
      payload: data,
    });
    dispatch({
      type: LOADING,
      payload: false,
    });
    return data;
  } catch (error) {
    return error;
  }
};


const languageReducer = (state = initialState, action: ActionTypes) => {
  switch (action.type) {
    case GET_ALL:
      return {
        ...state,
        list: action.payload,
      };

    case LOADING:
      return {
        ...state,
        loading: action.payload,
      };
    default:
      return state;
  }
};

export default languageReducer;
