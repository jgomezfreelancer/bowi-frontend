import AXIOS from '../config/Axios';
import { Prefix } from '../config/ApiPrefix';
import headers from '../helpers/headers';
import { AxiosResponse } from 'axios';

const API = {
  async getUserRooms(id: number): Promise<AxiosResponse> {
    return AXIOS.get(`${Prefix.api}/room/get-by-user/${id}`, { headers: await headers() });
  },
  async setChat(
    responsibleId: number,
    participantId: number,
    petitionId: number | null,
  ): Promise<AxiosResponse> {
    return AXIOS.post(
      `${Prefix.api}/room`,
      { responsibleId, participantId, petitionId },
      { headers: await headers() },
    );
  },
  remove(id: number): Promise<AxiosResponse> {
    return AXIOS.delete(`${Prefix.api}/room/${id}`, { headers: headers() });
  },
  disable(id: number): Promise<AxiosResponse> {
    return AXIOS.put(`${Prefix.api}/room/disable/${id}`, {}, { headers: headers() });
  },
  async checkExpiration(id: number): Promise<AxiosResponse> {
    return AXIOS.get(`${Prefix.api}/room/check-expiration/${id}`, { headers: await headers() });
  },
  async blockRoomUser(body: { authUser: number; blockUser: number }): Promise<AxiosResponse> {
    return AXIOS.post(`${Prefix.api}/room/block-user`, body, { headers: await headers() });
  },
  async reportRoomUser(body: {
    authUser: number;
    reportUser: number;
    reason: string;
  }): Promise<AxiosResponse> {
    return AXIOS.post(`${Prefix.api}/email/report-user`, body, { headers: await headers() });
  },
};

export default API;
