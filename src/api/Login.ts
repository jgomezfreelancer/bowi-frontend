import AXIOS from '../config/Axios';
import { Prefix } from '../config/ApiPrefix';
import headers from '../helpers/headers';
import {
  ISocialLogin,
  IUser,
  IFcm,
  ISendHelp,
  ICoord,
  IAcceptHelp,
  IRegister,
  IStarterSettings,
  IUserInterest,
} from '../reducers/login.reducer';
import { AxiosResponse } from 'axios';

const API = {
  async login(data: object): Promise<AxiosResponse> {
    return AXIOS.post(`${Prefix.api}/auth/signin`, data, { headers: await headers() });
  },
  async signup(data: IRegister): Promise<AxiosResponse> {
    return AXIOS.post(`${Prefix.api}/auth/signup`, data, { headers: await headers() });
  },
  async socialLogin(data: ISocialLogin): Promise<AxiosResponse> {
    return AXIOS.post(`${Prefix.api}/auth/social-signup`, data, { headers: await headers() });
  },
  async checkLogin(): Promise<AxiosResponse> {
    return AXIOS.get(`${Prefix.api}/auth/check`, { headers: await headers() });
  },
  async updateSettings(id: number, data: IUser): Promise<AxiosResponse> {
    return AXIOS.put(`${Prefix.api}/auth/update-user/${id}`, data, { headers: await headers() });
  },
  async updateStarterUser(id: number, data: IStarterSettings): Promise<AxiosResponse> {
    return AXIOS.put(`${Prefix.api}/auth/update-starter/${id}`, data, { headers: await headers() });
  },
  async updateLangNationalities(id: number, data: IStarterSettings): Promise<AxiosResponse> {
    return AXIOS.put(`${Prefix.api}/auth/update-lang-nationality/${id}`, data, {
      headers: await headers(),
    });
  },
  async updateProfile(id: number, data: IUser): Promise<AxiosResponse> {
    return AXIOS.put(`${Prefix.api}/auth/update-profile/${id}`, data, { headers: await headers() });
  },
  async updateCoord(id: number, data: ICoord): Promise<AxiosResponse> {
    return AXIOS.put(`${Prefix.api}/auth/update-coord/${id}`, data, { headers: await headers() });
  },
  async updateFcm(id: number, data: IFcm): Promise<AxiosResponse> {
    return AXIOS.put(`${Prefix.api}/auth/update-fcm/${id}`, data, { headers: await headers() });
  },
  async sendHelp(data: ISendHelp): Promise<AxiosResponse> {
    return AXIOS.post(`${Prefix.api}/auth/send-help`, data, { headers: await headers() });
  },
  async acceptHelp(data: IAcceptHelp): Promise<AxiosResponse> {
    return AXIOS.post(`${Prefix.api}/auth/accept-help`, data, { headers: await headers() });
  },
  async getUsersByLocation(id: number, km: number): Promise<AxiosResponse> {
    return AXIOS.get(`${Prefix.api}/auth/users-by-location`, {
      params: {
        id,
        km,
      },
      headers: await headers(),
    });
  },
  async getUserImage(id: number): Promise<AxiosResponse> {
    return AXIOS.get(`${Prefix.api}/auth/user-image/${id}`, { headers: await headers() });
  },
  async getUserProfileImage(id: number): Promise<AxiosResponse> {
    return AXIOS.get(`${Prefix.api}/auth/profile-image/${id}`, { headers: await headers() });
  },
  async forgotPassword(username: string): Promise<AxiosResponse> {
    return AXIOS.get(`${Prefix.api}/email/forgot-password`, {
      params: { username },
      headers: await headers(),
    });
  },
  async getServerDate(): Promise<AxiosResponse> {
    return AXIOS.get(`${Prefix.api}/auth/server-date`, { headers: await headers() });
  },
  async getUserPetition(id: number): Promise<AxiosResponse> {
    return AXIOS.get(`${Prefix.api}/auth/user-petition/${id}`, { headers: await headers() });
  },
  async disablePetition(id: number): Promise<AxiosResponse> {
    return AXIOS.get(`${Prefix.api}/auth/user-disable-petition/${id}`, {
      headers: await headers(),
    });
  },
  async logout(token: string): Promise<AxiosResponse> {
    return AXIOS.get(`${Prefix.api}/auth/logout/${token}`, { headers: await headers() });
  },
  async updateUserInterest(data: IUserInterest): Promise<AxiosResponse> {
    return AXIOS.post(`${Prefix.api}/auth/update-interest`, data, { headers: await headers() });
  },
};

export default API;
