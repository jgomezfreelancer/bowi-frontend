import AXIOS from '../config/Axios';
import { Prefix } from '../config/ApiPrefix';
import headers from '../helpers/headers';
import { AxiosResponse } from 'axios';

const API = {
  async getAll(): Promise<AxiosResponse> {
    return AXIOS.get(`${Prefix.api}/interest`, { headers: await headers() });
  },
};

export default API;
