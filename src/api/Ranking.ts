import AXIOS from '../config/Axios';
import { Prefix } from '../config/ApiPrefix';
import headers from '../helpers/headers';
import { AxiosResponse } from 'axios';
import { IRanking } from '../reducers/ranking.reducer';

const API = {
  async getRankingByUser(id: number): Promise<AxiosResponse> {
    return AXIOS.get(`${Prefix.api}/ranking/user/${id}`, { headers: await headers() });
  },
  async create(data: IRanking): Promise<AxiosResponse> {
    return AXIOS.post(`${Prefix.api}/ranking`, data, { headers: await headers() });
  },
};

export default API;
