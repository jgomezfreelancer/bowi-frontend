import AXIOS from '../config/Axios';
import { Prefix } from '../config/ApiPrefix';
import headers from '../helpers/headers';
import { AxiosResponse } from 'axios';

const API = {
  async setRecievedNotification(id: number): Promise<AxiosResponse> {
    return AXIOS.put(`${Prefix.api}/notification/recieved`, { id }, { headers: await headers() });
  },
  async remove(requestId: number, acceptId: number): Promise<AxiosResponse> {
    return AXIOS.post(
      `${Prefix.api}/notification/remove`,
      { requestId, acceptId },
      { headers: await headers() },
    );
  },
  async getFailureNotification(id: number): Promise<AxiosResponse> {
    return AXIOS.get(`${Prefix.api}/notification/failure/${id}`, { headers: await headers() });
  },
  async checkPendingNotification(requestId: number, acceptId: number): Promise<AxiosResponse> {
    return AXIOS.get(`${Prefix.api}/notification/check-pending`, { params: {requestId, acceptId} , headers: await headers() });
  },
};

export default API;
