import AXIOS from '../config/Axios';
import { Prefix } from '../config/ApiPrefix';
import headers from '../helpers/headers';
import { AxiosResponse } from 'axios';

const API = {
  async getRoomMessages(id: number, sender: number): Promise<AxiosResponse> {
    return AXIOS.get(`${Prefix.api}/message/room`, { params: { room: id, sender },  headers: await headers() });
  },
};

export default API;
