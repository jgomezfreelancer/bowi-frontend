import React from 'react';
import { Text, TouchableHighlight, View, ImageBackground } from 'react-native';
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Container, Content, Header } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';

import Input from '../../components/common/form/Input2';
import { IRegister, signup } from '../../reducers/login.reducer';
import Button from '../../components/common/form/Button';
import { useNavigation } from '@react-navigation/native';
import setToast from '../../actions/toast.action';
import Styles from './style';
import { AppRootReducer } from '../../reducers';
import { customTraslate } from '../../utils/language';
const backgroundImage = require('../../assets/images/fondo-regístrate-03.png');
import BowiLogoIcon from '../../assets/images/bowi-logo.svg';
import ArrowIcon from '../../assets/icons/flecha.svg';

interface FormData extends IRegister {
  first_name: string;
  username: string;
  email: string;
  password: string;
  password2: string;
}

export type IFB = {
  email: string;
  first_name: string;
  last_name: string;
};

function Register(): JSX.Element {
  const { handleSubmit, control, errors } = useForm<FormData>();
  const dispatch = useDispatch();
  const {
    loginReducer: { loading },
  } = useSelector((state: AppRootReducer) => state);
  const navigation = useNavigation();

  const onSubmit = async (form: FormData) => {
    if (form.password !== form.password2) {
      dispatch(
        setToast({
          payload: {
            message: 'Contraseñas deben ser iguales',
            status: true,
            type: 'error',
          },
        }),
      );
    } else {
      signup(form)(dispatch)
        .then(() => {
          navigation.navigate('Starter');
        })
        .catch(() => {
          dispatch(
            setToast({
              payload: {
                message: 'Error durante el registro de usuario, intentar de nuevo',
                status: true,
                type: 'error',
              },
            }),
          );
        });
    }
  };

  const handleBack = () => {
    navigation.navigate('Main');
  };

  return (
    <Container>
      <Header style={Styles.headerStyle}>
        <View style={Styles.backButtonContainer}>
          <TouchableHighlight onPress={handleBack} underlayColor="transparent">
            <ArrowIcon width={25} height={25} />
          </TouchableHighlight>
        </View>
      </Header>
      <Content>
        <View style={Styles.formContainer}>
          <View style={Styles.logoContainer}>
            <BowiLogoIcon style={Styles.logo} />
          </View>
          <View>
            <Text style={Styles.registerText}>{customTraslate('registrate')}</Text>
          </View>
          <View style={Styles.fieldContainer}>
            <Input
              control={control}
              name="first_name"
              required
              error={errors.first_name ? errors.first_name.message : null}
              placeholder={'*' + customTraslate('nombre')}
            />
          </View>
          <View style={Styles.fieldContainer}>
            <Input
              control={control}
              name="username"
              required
              error={errors.username ? errors.username.message : null}
              placeholder={'*' + customTraslate('correo')}
              inputType="email"
            />
            {errors.username && (
              <Text style={Styles.requiredMessage}>{errors.username.message}</Text>
            )}
          </View>
          <View style={Styles.fieldContainer}>
            <Input
              control={control}
              name="password"
              required
              error={errors.password ? errors.password.message : null}
              placeholder={'*' + customTraslate('contrasena')}
              secureText
              iconName="lock"
            />
          </View>
          <View style={Styles.fieldContainer}>
            <Input
              control={control}
              name="password2"
              required
              error={errors.password2 ? errors.password2.message : null}
              placeholder={'*' + customTraslate('repetirContrasena')}
              secureText
            />
          </View>
          <View style={Styles.fieldContainer}>
            <Button
              loading={loading}
              handleSubmit={handleSubmit(onSubmit)}
              title={customTraslate('registrarse')}
            />
          </View>
        </View>
      </Content>
    </Container>
  );
}

export default Register;
