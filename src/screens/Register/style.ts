import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  headerStyle: {
    backgroundColor: 'white',
    elevation: 0,
    borderBottomColor: '#c8c8c8',
    paddingLeft: 10,
    paddingRight: 0,
  },
  backButtonContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  formContainer: {
    justifyContent: 'center',
    paddingLeft: 50,
    paddingRight: 50,
  },
  registerText: { 
    fontSize: 17,
    color: '#9C9C9C', 
    fontWeight: '700', 
    textAlign: 'center', 
    paddingTop: 43,
    paddingBottom: 30,
  },
  fieldContainer: { marginTop: 15 },
  forgotPasswordText: { color: '#a5a5a5', textAlign: 'center', textDecorationLine: 'underline' },
  backgroundImage: {
    width: '100%',
    flex: 1,
  },
  requiredMessage: { paddingLeft: 10, color: 'red', fontWeight: 'bold' },
  logoContainer: {
    width: '100%',
    justifyContent: 'center',
    flexDirection: 'row'
  },
  logo: {
    height: 100,
    width: 100,
  }
});
