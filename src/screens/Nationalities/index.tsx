import { useNavigation } from '@react-navigation/native';
import { Body, CheckBox, Container, ListItem, Text } from 'native-base';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import CustomHeader from '../../components/common/Header';
import FlagLogo from '../../components/FlagLogo';
import { AppRootReducer } from '../../reducers';
import { updateLangNationalities } from '../../reducers/login.reducer';
import { getAll } from '../../reducers/nationality.reducer';
import { BLUE } from '../../styles/colors';
import { customTraslate } from '../../utils/language';
import Styles from './style';

export default function Nastionalities(): JSX.Element {
  const [nationality, setNationality] = useState<number | null>(null);
  const {
    loginReducer: { user },
    nationalityReducer: { list },
  } = useSelector((state: AppRootReducer) => state);
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const [textValue, setTextValue] = useState<string>('');
  const [filteredNationality, setFilteredNationality] = useState<any[]>([]);
  const condition = (item: string, value: string) => {return item.toLowerCase().includes(value.toLowerCase())};
  const getFilteredNationality = () => {
    return textValue.length > 0 ? filteredNationality : list;
  };

  useEffect(() => {
    setNationality(user.nationality_id!);
  }, [user.nationality_id]);

  useEffect(() => {
    dispatch(getAll());
  }, [dispatch]);

  const handleBack = async () => {
    await dispatch(
      updateLangNationalities(user.id!, {
        languages: [],
        nationalities: [],
        nationality_id: nationality!,
      }),
    );
    navigation.navigate('Ajustes');
  };

  const onSelect = (id: number) => setNationality(id);

  const getList = (element: any, i: number) => {
    const current = nationality === element.id;
    const checked = !!current;
    return (
      <ListItem key={i} onPress={() => onSelect(element.id)} style={Styles.listItems}>
        <FlagLogo name={element.slug} />
        <Body>
          <Text style={Styles.listText}>{element.description}</Text>
        </Body>
        <CheckBox
          style={Styles.checkbox}
          color={BLUE}
          checked={checked}
          onPress={() => onSelect(element.id)}
        />
      </ListItem>
    );
  };

  const onFilter = (val: string) => {
    setTextValue(val);
    let filter = list.filter((item) => condition(item.description, val));
    setFilteredNationality(filter);
  };

  return (
    <Container>
      <CustomHeader
        leftIcon="arrow-left"
        leftColor="#989898"
        handleLeft={handleBack}
        title={customTraslate('nacionalidad')}
        isInput={true}
        onChangeText={onFilter}
      />
      {getFilteredNationality().length > 0 && getFilteredNationality().map(getList)}
    </Container>
  );
}
