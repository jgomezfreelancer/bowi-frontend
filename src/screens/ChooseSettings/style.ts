import { StyleSheet } from 'react-native';
import { FONT } from '../../styles/colors';

export default StyleSheet.create({
  root: {
    backgroundColor: '#d7fcfe',
  },
  headerStyles: {
    backgroundColor: '#d7fcfe',
    elevation: 0,
    borderBottomColor: '#c8c8c8',
    paddingLeft: 10,
    paddingRight: 0,
  },
  backButtonContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  rowContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 40,
    paddingRight: 40,
    marginVertical: 20
  },
  middleText: { 
    textAlign: 'center', 
    color: '#007E86', 
    fontSize: 22, 
    fontWeight: '700',
    fontFamily: FONT 
  },
  middleText2: {
    textAlign: 'center',
    color: '#007E86',
    fontSize: 16,
    fontStyle: 'italic',
  },
  startButtonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 100,
    paddingRight: 100,
  },
  startButton: {
    borderRadius: 30,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    margin: 5,
    backgroundColor: '#007E86',
    borderColor: '#007E86',
    padding: 10,
  },
  startButtonText: { 
    fontSize:15, 
    color: 'white', 
    fontWeight: '700' 
  },
  settingContainer: {
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'row',
    backgroundColor: '#ffffff',
    borderRadius: 5,
    paddingVertical: 15,
    paddingHorizontal: 30,
  },
  settingDistanceContainer: {
    flex: 1,
    justifyContent: 'space-around',
    flexDirection: 'row',
    backgroundColor: '#ffffff',
    borderRadius: 5,
  },
  switchTextTitle: {
    fontSize: 14,
    color: '#414141',
    fontWeight: '700',
    paddingHorizontal: 30,
  },
  switchTextTitleDistance: {
    fontSize: 14,
    color: '#414141',
    fontWeight: '700',
    paddingHorizontal: 30,
  },
  switchTextSubTitle: { 
    fontWeight: '400', 
    fontSize: 17, 
    color: '#8D8D8D' 
  },
  switchContainer: {
    padding: 10,
    textAlign: 'right',
    marginTop: 2,
  },
  slider: {
    width: 'auto',
    color: 'red',
    transform: [{ scaleX: 1.1 }, { scaleY: 1.1 }],
    borderColor: '#000',
    marginVertical: 15,
    marginHorizontal: 15,
  },
  iconStepsContainer: { justifyContent: 'space-around', flexDirection: 'row' },
  iconStepStyle: {
    marginLeft: 10,
    marginRight: 10,
  },
  sliderContainer: { 
    backgroundColor: '#ffffff',
    flex: 1,
    flexDirection: 'row',
  },
  sliderText: {
    width: 100,
    textAlign: 'left',
    color: '#007E86',
    fontWeight: 'bold',
    fontSize: 14,
    paddingLeft: 30
  },
  iconContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 81,
    height: 81,
    backgroundColor: '#ffffff',
    borderRadius: 100,
  },
  rowIconContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewContainer: {
    flex: 1,
    flexDirection: 'column',
  },
  viewText: {
    backgroundColor: '#ffffff',
    marginBottom: 15,
    borderRadius: 5,
    paddingVertical: 15
  },
});
