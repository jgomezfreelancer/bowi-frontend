import React, { useEffect, useState } from 'react';

import { Container, Grid, Row, Text, Header } from 'native-base';
import { TouchableHighlight, View, Dimensions, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { useNavigation } from '@react-navigation/native';
import Slider from '@react-native-community/slider';

import Styles from './style';
import ArrowIcon from '../../assets/icons/flecha.svg';
import LikeIcon from '../../assets/icons/icono-04.svg';
import { useDispatch, useSelector } from 'react-redux';
import { AppRootReducer } from '../../reducers';
import { getAll } from '../../reducers/nationality.reducer';
import Switch from '../../components/common/form/Switch';
import { useForm } from 'react-hook-form';
import { updateStarterSettings, updateStarterUser } from '../../reducers/login.reducer';
import { customTraslate } from '../../utils/language';

type FormData = {
  location: number;
  profile: number;
};

// const { width } = Dimensions.get('screen');



export default function ChooseSettings(): JSX.Element {
  const [sliderValue, setSliderValue] = useState(50);
  const {
    loginReducer: {
      starterSettings,
      user: { id },
    },
  } = useSelector((state: AppRootReducer) => state);
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const { handleSubmit, control } = useForm<FormData>();

  useEffect(() => {
    dispatch(getAll());
  }, [dispatch]);

  const handleBack = () => {
    navigation.navigate('ChooseInterest');
  };

  const onSliderChange = (e: number) => {
    setSliderValue(e);
  };

  // const getSliderTextValue = (): number => {
  //   return sliderValue >= 0 ? 10 : sliderValue >= 50 ? 80 : 0;
  // };

  const onSubmit = async (form: FormData) => {
    const data = {
      ...form,
      location: form.location ? 1 : 0,
      profile: form.profile ? 1 : 0,
      limit_distance: sliderValue,
    };

    dispatch(updateStarterSettings({ ...starterSettings, ...data }));
    dispatch(updateStarterUser(id!, { ...starterSettings, ...data }));
    navigation.navigate('TabNavigator');
  };

  // const value = getSliderTextValue();
  // const left = (sliderValue * (width - 100)) / 120 - value;

  return (
    <Container style={Styles.root}>
      <Header style={Styles.headerStyles}>
        <View style={Styles.backButtonContainer}>
          <TouchableHighlight onPress={handleBack} underlayColor="transparent">
            <ArrowIcon width={25} height={25} />
          </TouchableHighlight>
        </View>
      </Header>
      <Grid>
        <Row size={3} style={Styles.rowIconContainer}>
          <View style={Styles.iconContainer}>
            <LikeIcon width={57} height={60} />
          </View>
        </Row>
        <Row size={2} style={Styles.rowContainer}>
          <Text style={Styles.middleText}>{customTraslate('yaAcabamos')}</Text>
        </Row>
        <Row size={5} style={Styles.rowContainer}>
          <View style={Styles.settingContainer}>
            <Text style={Styles.switchTextTitle}>
              {customTraslate('ubicacion')} {'\n'}
              <Text style={Styles.switchTextSubTitle}>{customTraslate('ubicacionSubtitulo')}</Text>
            </Text>
            <Text style={Styles.switchContainer}>
              <Switch control={control} name="location"
              />
            </Text>
          </View>
        </Row>

        <Row size={5} style={Styles.rowContainer}>
          <View style={Styles.settingContainer}>
            <Text style={Styles.switchTextTitle}>
              {customTraslate('mostrarPerfil')} {'\n'}
              <Text style={Styles.switchTextSubTitle}>
                {customTraslate('mostrarPerfilSubtitulo')}
              </Text>
            </Text>
            <Text style={Styles.switchContainer}>
              <Switch control={control} name="profile" />
            </Text>
          </View>
        </Row>

        <Row size={11} style={Styles.rowContainer}>
          <View style={Styles.viewContainer}>
            <View style={Styles.viewText}>
              <Text style={Styles.switchTextTitleDistance}>
                {customTraslate('distancia')} {'\n'}
                <Text style={Styles.switchTextSubTitle}>{customTraslate('distanciaSubTitulo')}</Text>
              </Text>
              <Slider
                style={Styles.slider}
                step={1}
                thumbTintColor="#00B2B2"
                maximumValue={100}
                minimumTrackTintColor="#bdebeb"
                maximumTrackTintColor="#00b2b2"
                onValueChange={onSliderChange}
                value={sliderValue}
              />
              <Text style={{ ...Styles.sliderText }}>{Math.floor(sliderValue)} Km</Text>
            </View>
          </View>
        </Row>

        <Row size={5} style={Styles.startButtonContainer}>
          <TouchableHighlight
            underlayColor="#007E86"
            style={Styles.startButton}
            onPress={handleSubmit(onSubmit)}
          >
            <Text style={Styles.startButtonText}> {customTraslate('finalizar').toUpperCase()}</Text>
          </TouchableHighlight>
        </Row>
        <Row size={2} style={Styles.startButtonContainer}>
          <View style={Styles.iconStepsContainer}>
            <Icon size={14} color="#007E86" name="circle" solid />
            <Icon style={Styles.iconStepStyle} solid size={14} color="#007E86" name="circle" />
            <Icon size={14} color="#007E86" name="circle" solid />
            <Icon style={Styles.iconStepStyle} solid size={14} color="#007E86" name="circle" />
            <Icon size={14} color="#007E86" name="circle" solid />
          </View>
        </Row>
        <Row size={3}></Row>
      </Grid>
    </Container>
  );
}
