import { StyleSheet } from 'react-native';
import { FONT } from '../../styles/colors';

export default StyleSheet.create({
  root: { backgroundColor: '#d7fcfe' },
  headerStyles: {
    backgroundColor: '#d7fcfe',
    elevation: 0,
    borderBottomColor: '#c8c8c8',
    paddingLeft: 10,
    paddingRight: 0,
  },
  backButtonContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  middleTextContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 40,
    paddingRight: 40,
  },
  middleText: { 
    paddingTop: 13,
    textAlign: 'center', 
    color: '#007E86', 
    fontSize: 22, 
    fontFamily: FONT,
    fontWeight: '700',
  },
  middleText2: {
    textAlign: 'center',
    color: '#007E86',
    fontSize: 16,
    fontStyle: 'italic',
  },
  startButtonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 100,
    paddingRight: 100,
  },
  startButton: {
    borderRadius: 30,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    margin: 5,
    backgroundColor: '#007E86',
    borderColor: '#007E86',
    padding: 10,
  },
  startButtonText: { color: 'white', fontWeight: 'bold' },
  iconStepsContainer: { justifyContent: 'space-around', flexDirection: 'row' },
  iconStepStyle: {
    marginLeft: 10,
    marginRight: 10,
  },
  rowIconContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 81,
    height: 81,
    backgroundColor: '#ffffff',
    borderRadius: 81
  },
  languagesTitle: {
    marginTop: 15,
    padding: 10,
    paddingBottom: 0,
    paddingLeft: 0,
    marginLeft: '5%',
    fontWeight: '700',
    fontSize: 14,
    color: '#414141',
  },
  cardContainer: { 
    borderRadius: 10, 
    elevation: 0,
    width: '100%'
  },
  textArea: { 
    backgroundColor: '#f5f5f5',
    borderRadius: 30,
    marginTop: 15,
    marginBottom: 15,
    marginLeft: '5%',
    marginRight: '5%',
    textAlign: 'left',
    paddingLeft: 15,
    paddingRight: 15
  },
  introText: {
    color: '#007E86',
    textDecorationLine: 'underline'
  }
});
