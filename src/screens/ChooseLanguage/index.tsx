import React, { useEffect, useState, useRef } from 'react';

import { Container, Grid, Row, Text, Header, Card } from 'native-base';
import { TouchableHighlight, View, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { useNavigation } from '@react-navigation/native';

import Styles from './style';

import TraslateIcon from '../../assets/icons/icono-inicio-02.svg';
import ArrowIcon from '../../assets/icons/flecha.svg';
import MultipleSelect from '../../components/common/MultipleSelect';
import { useDispatch, useSelector } from 'react-redux';
import { AppRootReducer } from '../../reducers';
import { getAll } from '../../reducers/language.reducer';
import { updateStarterSettings, updateStarterUser } from '../../reducers/login.reducer';
import { customTraslate } from '../../utils/language';

export default function ChooseLanguage(): JSX.Element {
  const [languages, setLanguages] = useState<number[]>([]);
  const {
    languageReducer: { list },
    loginReducer: { starterSettings, 
      user: { id } 
    },
  } = useSelector((state: AppRootReducer) => state);
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const inputRef = useRef<TextInput | null>(null);
  const [filteredLanguage, setFilteredLanguage] = useState<any[]>([]);
  const [textValue, setTextValue] = useState<string>('');
  const condition = (item: string, value: string) => {return item.toLowerCase().includes(value.toLowerCase())};
  const getFilteredLanguage = () => {
    return textValue.length > 0 ? filteredLanguage : list;
  };
  useEffect(() => {
    dispatch(getAll());
  }, [dispatch]);

  const onFilterChange = (text: string) => {
    setTextValue(text);
    let filter = list.filter((item) => condition(item.description, text));
    setFilteredLanguage(filter);
  };

  const handleChooseLanguage = () => {
    dispatch(updateStarterSettings({ ...starterSettings, languages }));
    navigation.navigate('ChooseNationality');
  };

  const handleBack = () => {
    navigation.navigate('Starter');
  };

  const onChangeLanguages = (arrayList: number[]) => {
    setLanguages(arrayList);
  };

  const handleDashboard = () => {
    const data = {
      location: 0,
      profile: 0,
      limit_distance: 50,
      first_time: false,
      languages: [],
      nationalities: [],
      nationality_id: null,
      interests: [],
    };
    dispatch(updateStarterUser(id!, data));
    navigation.navigate('TabNavigator');
  };

  return (
    <Container style={Styles.root}>
      <Header style={Styles.headerStyles}>
        <View style={Styles.backButtonContainer}>
          <TouchableHighlight onPress={handleBack} underlayColor="transparent">
            <ArrowIcon width={25} height={25} />
          </TouchableHighlight>
        </View>
      </Header>
      <Grid>
        <Row size={10} style={Styles.rowIconContainer}>
          <View style={Styles.iconContainer}>
            <TraslateIcon width={47} height={47} />
          </View>
        </Row>
        <Row size={5} style={Styles.middleTextContainer}>
          <Text style={Styles.middleText}>{customTraslate('queIdiomaHablas').toUpperCase()}</Text>
        </Row>
        <Row size={2} style={Styles.middleTextContainer} />
        <Row size={30} style={Styles.middleTextContainer}>
          <Card style={Styles.cardContainer}>
            <Text style={Styles.languagesTitle}>{customTraslate('seleccionaTuIdioma')}</Text>
            <TextInput
              ref={inputRef}
              onChangeText={onFilterChange}
              style={Styles.textArea}
              placeholder={`${customTraslate('buscar')}`}
            />
            <MultipleSelect
              onChange={onChangeLanguages}
              list={getFilteredLanguage()}
              icon={false}
              selectedList={
                starterSettings.languages && starterSettings.languages.length > 0
                  ? starterSettings.languages
                  : []
              }
            />
          </Card>
        </Row>
        <Row size={8} style={Styles.startButtonContainer}>
          <TouchableHighlight
            underlayColor="#007E86"
            style={Styles.startButton}
            onPress={handleChooseLanguage}
          >
            <Text style={Styles.startButtonText}>{customTraslate('siguiente').toUpperCase()}</Text>
          </TouchableHighlight>
        </Row>
        <Row size={3} style={Styles.startButtonContainer}>
          <View style={Styles.iconStepsContainer}>
            <Icon size={14} color="#007E86" name="circle" solid />
            <Icon style={Styles.iconStepStyle} solid size={14} color="white" name="circle" />
            <Icon size={14} color="white" name="circle" solid />
            <Icon style={Styles.iconStepStyle} solid size={14} color="white" name="circle" />
            <Icon size={14} color="white" name="circle" solid />
          </View>
        </Row>
        <Row size={5} style={Styles.startButtonContainer}>
          <Text style={Styles.introText} onPress={handleDashboard}>
            {customTraslate('omitirIntroduccion')}
          </Text>
        </Row>
      </Grid>
    </Container>
  );
}
