import { useNavigation, } from '@react-navigation/native';
import { Container, Content, List, ListItem, Left, Right, Col, Row } from 'native-base';
import React, { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome';

import CustomHeader from '../../components/common/Header';
import { AppRootReducer } from '../../reducers';
import {
  disablePetition,
  getServerDate,
  getUserPetition,
  IUser,
} from '../../reducers/login.reducer';
import { setNotificationIcon } from '../../reducers/notification.reducer';
import { getUserRooms, IUserRooms, setRoomOptionsModal } from '../../reducers/room.reducer';
import ItemRoom from './ItemRoom';
import { Text, View, TouchableHighlight, Image } from 'react-native';
import moment, { Moment } from 'moment';
import BottomModal from './BottomModal';
import { MainOption } from './MainOption/MainOoption';
import { customTraslate } from '../../utils/language';
import SearchIcon from '../../assets/icons/icono-mapa-10.svg';
import ListOnlyHelpIcon from '../../assets/icons/icono-mapa-f-12.svg';
import Styles from './style';

function ListOnlyHelp(): JSX.Element {
  const [isOnlyHelp, setIsOnlyHelp] = useState<boolean>(false);
  const [onlyHelpStyles, setOnlyHelpStyles] = useState({ ...Styles.regularButtonRight });
  const [modalBottomCompoent, setModalBottomComponent] = useState<JSX.Element>(<MainOption />);
  const [counterTime, setCounterTime] = useState<Moment | null>(null);
  const navigation = useNavigation();
  const {
    loginReducer: {
      user: { id },
      serverDate,
      userPetition,
    },
    roomReducer: {
      userRooms,
      roomModalOptions: { statusModal },
    },
  } = useSelector((state: AppRootReducer) => state);
  const dispatch = useDispatch();
  const [counter, setCounter] = useState({
    hour: 0,
    minute: 0,
    second: 0,
  });
  const [everyOneStyles, setEveryOneStyles] = useState({
    ...Styles.regularButtonLeft,
    ...Styles.regularButtonLeftActive,
  });

  const [onlyTimeStyles, setOnlyTimeStyles] = useState({
    ...Styles.regularButtonLeft,
    ...Styles.regularButtonLeftActive,
  });

  const [onlyDistanceStyles, setOnlyDistanceStyles] = useState({ ...Styles.regularButtonRight });

  const getOnlyTime = useCallback(() => {
    setOnlyTimeStyles({ ...Styles.regularButtonLeft, ...Styles.regularButtonLeftActive });
    setOnlyDistanceStyles(Styles.regularButtonRight);
  }, [setOnlyTimeStyles, setOnlyDistanceStyles]);

  const getOnlyDistance = useCallback(() => {
    setOnlyTimeStyles(Styles.regularButtonLeft);
    setOnlyDistanceStyles({ ...Styles.regularButtonRight, ...Styles.regularButtonRightActive2 });
  }, [setOnlyTimeStyles, setOnlyDistanceStyles]);

  useEffect(() => {
    dispatch(getUserRooms(id!));
    dispatch(setNotificationIcon(false));
    dispatch(getServerDate());
    dispatch(getUserPetition(id!));
  }, [dispatch, id]);

  const handleChat = (room: IUserRooms, sender: IUser) => {
    navigation.navigate('Chat', { room, sender });
  };

  const handleNavigation = () => {
    navigation.navigate('Location');
  };

  useEffect(() => {
    if (Object.keys(userPetition).length > 0) {
      const expiration = moment(userPetition.created).add(1, 'hour');
      const expirationHour = expiration.format('HH:mm:ss');
      const serverHour = moment(serverDate).format('HH:mm:ss');
      const start = moment(expirationHour, 'HH:mm:ss');
      const end = moment(serverHour, 'HH:mm:ss');
      const startMinuteTime = moment.duration(start - end);
      const initialTime = `00:${startMinuteTime.minutes()}:${startMinuteTime.seconds()}`;
      setCounterTime(moment(`2020-01-01 ${initialTime}`));
    }
  }, [userPetition, serverDate]);

  useEffect(() => {
    if (Object.keys(userPetition).length > 0 && counterTime) {
      const interval = setInterval(() => {
        const valueCounter = counterTime;
        const petitionCounter = moment(valueCounter).subtract(1, 'second');
        const minute = petitionCounter.minute();
        setCounterTime(petitionCounter);
        setCounter({
          ...counter,
          hour: 0,
          minute: minute.toString().length === 1 ? `0${minute}` : minute,
          second: petitionCounter.second(),
        });
        if (petitionCounter.minute() === 0 && petitionCounter.second() === 0) {
          disablePetition(userPetition.id!)(dispatch)
            .then(() => {
              dispatch(getUserPetition(id!));
              setCounter({
                hour: 0,
                minute: 0,
                second: 0,
              });
              setCounterTime(null);
            })
            .catch(err => console.log(err));
          return () => clearInterval(interval);
        }
      }, 1000);
      return () => clearInterval(interval);
    }
  }, [counterTime, counter, userPetition]);

  const getEveryone = useCallback(() => {
    setIsOnlyHelp(false);
    setEveryOneStyles({ ...Styles.regularButtonLeft, ...Styles.regularButtonLeftActive });
    setOnlyHelpStyles(Styles.regularButtonRight);
  }, [setIsOnlyHelp, setEveryOneStyles, setOnlyHelpStyles]);

  const getOnlyHelp = useCallback(() => {
    setIsOnlyHelp(true);
    setEveryOneStyles(Styles.regularButtonLeft);
    setOnlyHelpStyles({ ...Styles.regularButtonRight, ...Styles.regularButtonRightActive });
  }, [setIsOnlyHelp, setEveryOneStyles, setOnlyHelpStyles]);


  const handlePetition = () => {
    navigation.navigate('PetitionDetail');
  };

  const handleOption = (optionRoom: IUserRooms) => {
    dispatch(
      setRoomOptionsModal({
        statusModal: true,
        selectedRoom: optionRoom,
      }),
    );
  };

  const handleStatusModal = (status: boolean) => {
    dispatch(
      setRoomOptionsModal({
        statusModal: status,
        selectedRoom: {},
      }),
    );
  };

  return (
    <Container>
      <Row style={Styles.rowButtonsContainer}>
        <Col>
          <Text style={everyOneStyles} onPress={getEveryone}>
            {customTraslate('todos').toUpperCase()}
          </Text>
        </Col>

        <Col>
          <Text style={onlyHelpStyles} onPress={getOnlyHelp}>
            {customTraslate('soloAyuda').toUpperCase()}
          </Text>
        </Col>
      </Row>
      <Row style={Styles.rowButtonsContainer2}>
        <Col>
          <Text style={onlyTimeStyles} onPress={getOnlyTime}>
            {customTraslate('tiempo').toUpperCase()}
          </Text>
        </Col>

        <Col>
          <Text style={onlyDistanceStyles} onPress={getOnlyDistance}>
            {customTraslate('distancia').toUpperCase()}
          </Text>
        </Col>
      </Row>
      <TouchableHighlight underlayColor="white" style={Styles.myLocation}>
        <SearchIcon width={20} height={20}/>
      </TouchableHighlight>
      <TouchableHighlight underlayColor="white" style={Styles.myLocation2} onPress={handleNavigation}>
        {isOnlyHelp != true ? 
          <Image
            source={require('../../assets/images/map-image01.png')}
            style={Styles.imageLocation}
          /> : 
        <ListOnlyHelpIcon width={25} height={25}/>
        }
      </TouchableHighlight>
      {/*<Content>
        <View style={Styles.view} />
        {Object.keys(userPetition).length > 0 && (
          <List style={{ marginLeft: -15 }}>
            <ListItem style={Styles.listItemContainer} onPress={handlePetition}>
              <Left style={Styles.expirationIconContainer}>
                <Icon size={30} color="#f24a23" name="exclamation" />
                <Text style={Styles.expirationContainer}>
                  {counter.hour.toString().length > 1 ? counter.hour : `0${counter.hour}`}:
                  {counter.minute},<Text style={Styles.secondText}>{counter.second}</Text> {'\n'}
                  <Text style={Styles.activeRequestText}>
                    {customTraslate('peticionActiva').toUpperCase()}
                  </Text>
                </Text>
              </Left>
              <Right>
                <Icon size={20} color="#989898" name="chevron-right" />
              </Right>
            </ListItem>
          </List>
        )}
        <List>
          {userRooms.map((element: IUserRooms, i: number) => (
            <ItemRoom key={i} room={element} handle={handleChat} handleOption={handleOption} />
          ))}
        </List>
        <BottomModal
          handleStatus={handleStatusModal}
          children={modalBottomCompoent}
          status={statusModal}
        />
          </Content>*/}
    </Container>
  );
}

export default ListOnlyHelp;
