import React, { useState } from 'react';
import { ListItem } from 'native-base';
import { View, Text, TouchableHighlight } from 'react-native';

import Styles from './style';
import { useDispatch, useSelector } from 'react-redux';
import { AppRootReducer } from '../../../reducers';
import { blockRoomUser, reportRoomUser, setRoomOptionsModal } from '../../../reducers/room.reducer';
import setToast from '../../../actions/toast.action';
import { getUsersByLocation } from '../../../reducers/login.reducer';
import { customTraslate } from '../../../utils/language';

const MainOption = (): JSX.Element => {
  const [loading, setLoading] = useState<boolean>(false);
  const [isComplaint, setIsComplaint] = useState<boolean>(false);
  const [isBlock, setIsBlock] = useState<boolean>(false);
  const [isConfirmBlock, setIsConfirmBlock] = useState<boolean>(false);
  const [isConfirmComplain, setIsConfirmComplain] = useState<boolean>(false);
  const {
    roomReducer: {
      roomModalOptions: { selectedRoom },
    },
    loginReducer: {
      user: { id, limit_distance },
    },
    locationReducer: { meters },
  } = useSelector((state: AppRootReducer) => state);
  const dispatch = useDispatch();

  const handleView = (option: string) => {
    if (option === 'complaint') {
      setIsComplaint(true);
      setIsBlock(false);
    }
    if (option === 'block') {
      setIsComplaint(false);
      setIsBlock(true);
    }
  };

  const handleConfirmYes = () => {
    console.log('selectedRoom ', selectedRoom);
    const body = {
      authUser: id!,
      blockUser: selectedRoom.user.id!,
    };
    blockRoomUser(body)(dispatch).then(() => {
      const blockUser = selectedRoom.user;
      setToast({
        payload: {
          status: true,
          type: 'success',
          message: `${customTraslate('usuario')} ${blockUser.first_name} ${blockUser.first_name} ${
            blockUser.last_name ? blockUser.last_name.charAt(0) : ''
          } ${customTraslate('haSidoBloqueado')}`,
        },
      })(dispatch);
      dispatch(
        setRoomOptionsModal({
          statusModal: false,
          selectedRoom: {},
        }),
      );
      dispatch(getUsersByLocation(id!, meters === 0 ? limit_distance! : meters));
    });
  };

  const handleConfirmNo = () => {
    dispatch(
      setRoomOptionsModal({
        statusModal: false,
        selectedRoom: {},
      }),
    );
  };

  const handleConfirmComplain = (reason: string) => {
    const reportUser = selectedRoom.user.id!;
    const body = {
      authUser: id!,
      reportUser,
      reason,
    };
    setLoading(true);
    setIsConfirmComplain(true);
    reportRoomUser(body)(dispatch).then(() => {
      setLoading(false);
      setTimeout(() => {
        dispatch(
          setRoomOptionsModal({
            statusModal: false,
            selectedRoom: {},
          }),
        );
      }, 3000);
    });
  };

  if (isComplaint) {
    return isConfirmComplain ? (
      <View style={Styles.complaintContainerResponse}>
        <Text style={Styles.title}>
          {loading ? `${customTraslate('enviando')} ...` : customTraslate('denunciaMensaje')}
        </Text>
      </View>
    ) : (
      <View style={Styles.complaintContainer}>
        <Text style={Styles.title}>{customTraslate('denunciar')}</Text>
        <View style={Styles.dividerContainer}>
          <View style={Styles.divider} />
        </View>
        <Text style={Styles.optionComplaintTitle}>
          ¿ {customTraslate('porqueDenunciarMensaje')} ?
        </Text>
        {[
          customTraslate('noMeGusta'),
          customTraslate('desnudosActividadSexual'),
          customTraslate('lenguajeOSimbolosOdio'),
          customTraslate('violencia'),
          customTraslate('bullying'),
          customTraslate('fraude'),
          customTraslate('informacionFalsa'),
        ].map((element: string, index: number) => (
          <ListItem key={index}>
            <TouchableHighlight
              underlayColor="transparent"
              onPress={() => handleConfirmComplain(element)}
            >
              <Text style={Styles.optionComplaintText}>{element}</Text>
            </TouchableHighlight>
          </ListItem>
        ))}
      </View>
    );
  }
  if (isBlock) {
    return (
      <View style={Styles.complaintContainer}>
        <Text style={Styles.title}>Bloquear</Text>
        <View style={Styles.dividerContainer}>
          <View style={Styles.divider} />
        </View>
        <Text style={Styles.optionBlockTitle}>¿ Estas seguro de que quieres bloquear ?</Text>
        <View style={Styles.blockOptionsContainer}>
          <TouchableHighlight underlayColor="transparent" onPress={handleConfirmYes}>
            <Text style={Styles.blockOptionText}>Si</Text>
          </TouchableHighlight>
          <TouchableHighlight underlayColor="transparent" onPress={handleConfirmNo}>
            <Text style={Styles.blockOptionText}>No</Text>
          </TouchableHighlight>
        </View>
      </View>
    );
  }
  return (
    <View>
      <Text style={Styles.optionTitle} onPress={() => handleView('complaint')}>
        Denunciar
      </Text>
      <Text style={Styles.optionTitle} onPress={() => handleView('block')}>
        Bloquear
      </Text>
    </View>
  );
};

export { MainOption };
