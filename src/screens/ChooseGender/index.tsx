import React, { useEffect, useState } from 'react';

import {
  Container,
  Grid,
  Row,
  Text,
  Header,
  Card,
  ListItem,
  Body,
  Content,
} from 'native-base';
import { TouchableHighlight, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { useNavigation } from '@react-navigation/native';

import Styles from './style';
import ArrowIcon from '../../assets/icons/flecha.svg';
import GenderIcon from '../../assets/icons/icono-inicio-03.svg';
import { useDispatch, useSelector } from 'react-redux';
import { AppRootReducer } from '../../reducers';
import { getAll } from '../../reducers/nationality.reducer';
import { updateStarterSettings, updateStarterUser } from '../../reducers/login.reducer';
import FlagLogo from '../../components/FlagLogo';
import { BLUE } from '../../styles/colors';
import { customTraslate } from '../../utils/language';

import GenderMan from '../../assets/icons/g-h.svg';
import GenderWoman from '../../assets/icons/g-m.svg';
import GenderNotBinary from '../../assets/icons/g-n.svg';

export default function ChooseGender(): JSX.Element {
  const {
    loginReducer: { 
      starterSettings,
      user: { id }  
    },
  } = useSelector((state: AppRootReducer) => state);
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const [genderList] = useState<any[]>([{id:0,slug:'g-h',name:'hombre'},{id:1,slug:'g-m',name:'mujer'},{id:2,slug:'g-n',name:'noBinario'}]);

  useEffect(() => {
    dispatch(getAll());
  }, [dispatch]);

  const handleChooseGender = () => {
    navigation.navigate('ChooseInterest');
  };

  const handleBack = () => {
    navigation.navigate('ChooseNationality');
  };

  const handleDashboard = () => {
    const data = {
      location: 0,
      profile: 0,
      limit_distance: 50,
      first_time: false,
      languages: [],
      nationalities: [],
      nationality_id: null,
      interests: [],
    };
    dispatch(updateStarterUser(id!, data));
    navigation.navigate('TabNavigator');
  };

  const handleGender = (gender: number) => {
    //dispatch(updateStarterGender({ ...starterSettings, gender_id: gender }));
    navigation.navigate('ChooseInterest');
  };

  const getFlagGender = (slug: string) => {
    switch (slug) {
      case 'g-h':
        return <GenderMan style={Styles.genderM} />
      case 'g-m':
        return <GenderWoman style={Styles.genderW} />
      case 'g-n':
        return <GenderNotBinary style={Styles.genderNB} />
    };
  };

  const getList = (element: any, i: number) => {
    return (
      <ListItem style={Styles.listItem} key={i} onPress={() => handleGender(element.id)}>
        <View style={Styles.iconGender}>
          {getFlagGender(element.slug)}
        </View>
        
        <Body>
          <Text style={Styles.listText}>{customTraslate(element.name)}</Text>
        </Body>
      </ListItem>
    );
  };

  return (
    <Container style={Styles.root}>
      <Header style={Styles.headerStyles}>
        <View style={Styles.backButtonContainer}>
          <TouchableHighlight onPress={handleBack} underlayColor="transparent">
            <ArrowIcon width={25} height={25} />
          </TouchableHighlight>
        </View>
      </Header>
      <Grid>
        <Row size={5} style={Styles.rowIconContainer}>
          <View style={Styles.iconContainer}>
            <GenderIcon width={50} height={50} />
          </View>
        </Row>
        <Row size={4} style={Styles.middleTextContainer}>
          <Text style={Styles.middleText}>{customTraslate('genero')}</Text>
        </Row>
        <Row size={20} style={Styles.middleTextContainer}>
          <Card style={Styles.cardContainer}>
            <Text style={Styles.selectNationalityTitle}>
              {customTraslate('seleccionaTuGenero')}
            </Text>
            <Content>
            {genderList.map(getList)}
            </Content>
          </Card>
        </Row>
        <Row size={5} style={Styles.startButtonContainer}>
          <TouchableHighlight
            underlayColor="#007E86"
            style={Styles.startButton}
            onPress={handleChooseGender}
          >
            <Text style={Styles.startButtonText}> {customTraslate('siguiente').toUpperCase()}</Text>
          </TouchableHighlight>
        </Row>
        <Row size={3} style={Styles.startButtonContainer}>
          <View style={Styles.iconStepsContainer}>
            <Icon size={14} color="#007E86" name="circle" solid />
            <Icon style={Styles.iconStepStyle} solid size={14} color="#007E86" name="circle" />
            <Icon size={14} color="#007E86" name="circle" solid />
            <Icon style={Styles.iconStepStyle} solid size={14} color="white" name="circle" />
            <Icon size={14} color="white" name="circle" solid />
          </View>
        </Row>
        <Row size={5} style={Styles.startButtonContainer}>
          <Text style={Styles.introText} onPress={handleDashboard}>
            {customTraslate('omitirIntroduccion')}
          </Text>
        </Row>
      </Grid>
    </Container>
  );
}
