import React, { useEffect } from 'react';
import { Modal, View, Text, Image, TouchableHighlight } from 'react-native';
import Stars from 'react-native-stars';
import { getDistance } from 'geolib';

import Icon from 'react-native-vector-icons/FontAwesome5';
import Styles from './style';
import { useDispatch, useSelector } from 'react-redux';
import { AppRootReducer } from '../../../reducers';
import { setModalRequest } from '../../../reducers/location.reducer';
import { cleanUserProfileImage, getUserProfileImage } from '../../../reducers/login.reducer';
import { BLUE } from '../../../styles/colors';
import { remove, setRecievedNotification } from '../../../reducers/notification.reducer';
import { customTraslate } from '../../../utils/language';

function RequestModal(): JSX.Element {
  const {
    locationReducer: {
      modalRequest: {
        status,
        type,
        handleRequest,
        user: { id, first_name, last_name, ranking, latitude, longitude, notificationId },
        isPetition,
      },
      location,
    },
    loginReducer: { userProfileImage, user, acceptLoading },
    roomReducer: { setChatLoading },
  } = useSelector((state: AppRootReducer) => state);
  const dispatch = useDispatch();
  const requestMessage = `¡${customTraslate('empiezaUnaConversacion')}!`;
  const acceptMessage = `${first_name} ${
    isPetition ? customTraslate('quiereAyudarte') : customTraslate('quiereHablarContigo')
  }`;

  const getMeters = () => {
    const requestUserLocation = { latitude: latitude!, longitude: longitude! };
    const meters = getDistance(location, requestUserLocation);
    return meters <= 1000 ? `${meters} Mtrs` : `${Math.round(meters! / 1000)} Km`;
  };

  useEffect(() => {
    if (status) {
      dispatch(getUserProfileImage(id!));
      if (notificationId) dispatch(setRecievedNotification(notificationId!));
    } else {
      dispatch(cleanUserProfileImage());
      if (notificationId) {
        remove(
          user.id!,
          id!,
        )(dispatch).then(() => {
          dispatch(
            setModalRequest({
              status: false,
              handleRequest: () => null,
              user: {},
            }),
          );
        });
      } else {
        dispatch(
          setModalRequest({
            status: false,
            handleRequest: () => null,
            user: {},
          }),
        );
      }
    }
  }, [status, dispatch]);

  const close = () => {
    if (notificationId) {
      remove(
        user.id!,
        id!,
      )(dispatch).then(() => {
        dispatch(
          setModalRequest({
            status: false,
            handleRequest: () => null,
            user: {},
          }),
        );
      });
    } else {
      dispatch(
        setModalRequest({
          status: false,
          handleRequest: () => null,
          user: {},
        }),
      );
    }
  };

  return (
    <View style={Styles.centeredView}>
      <Modal animationType="slide" transparent={true} visible={status}>
        <View style={Styles.centeredView}>
          <View style={Styles.modalView}>
            <View style={Styles.header}>
              <Icon style={Styles.closeIcon} name="times" onPress={close} />
            </View>
            <View style={Styles.content}>
              <View style={Styles.root}>
                <View style={Styles.userIconContainer}>
                  {userProfileImage ? (
                    <Image source={{ uri: userProfileImage }} style={Styles.imageProfile} />
                  ) : (
                    <Icon
                      size={75}
                      color={isPetition ? '#F34900' : BLUE}
                      name="user-circle"
                      solid
                    />
                  )}
                </View>
                <View style={Styles.userDetailContainer}>
                  <Text style={Styles.userDetail}>
                    {first_name} {last_name?.charAt(0)}
                  </Text>
                </View>
                <View style={Styles.rankingContainer}>
                  <Stars
                    disabled
                    default={ranking ? parseInt(String(ranking), 36) : 0}
                    count={5}
                    half={true}
                    starSize={50}
                    fullStar={<Icon size={12} color={BLUE} name="star" solid />}
                    emptyStar={<Icon size={12} color={BLUE} name="star" light />}
                    halfStar={<Icon size={12} color={BLUE} name="star-half-alt" solid />}
                  />
                </View>
                <Text style={Styles.kmTitle}>
                  {latitude && longitude! ? `a ${getMeters()}` : ''}
                </Text>
                <View style={Styles.commentContainer}>
                  <Text style={Styles.comment}>
                    {type === 'request' ? requestMessage : acceptMessage}
                  </Text>
                  {type === 'request' && (
                    <TouchableHighlight
                      disabled={acceptLoading}
                      underlayColor={BLUE}
                      style={Styles.singleButton}
                      onPress={handleRequest}
                    >
                      <Text
                        style={{
                          color: 'white',
                          textAlign: 'center',
                          fontSize: 17,
                        }}
                      >
                        {acceptLoading
                          ? `${customTraslate('enviando')}...`
                          : customTraslate('chat')}
                      </Text>
                    </TouchableHighlight>
                  )}
                  {type === 'accept' && (
                    <View style={Styles.buttonsContainer}>
                      <TouchableHighlight
                        underlayColor={BLUE}
                        disabled={acceptLoading}
                        style={Styles.buttonLeft}
                        onPress={close}
                      >
                        <Text style={Styles.textButton}>{customTraslate('rechazar')}</Text>
                      </TouchableHighlight>
                      <TouchableHighlight
                        underlayColor={BLUE}
                        disabled={acceptLoading}
                        style={Styles.buttonRight}
                        onPress={handleRequest}
                      >
                        <Text style={Styles.textButton}>
                          {setChatLoading
                            ? `${customTraslate('espere')}...`
                            : customTraslate('aceptar')}
                        </Text>
                      </TouchableHighlight>
                    </View>
                  )}
                </View>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
}

export default RequestModal;
