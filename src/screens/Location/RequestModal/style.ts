import { StyleSheet, Dimensions } from 'react-native';
import { BLUE } from '../../../styles/colors';

const { width } = Dimensions.get('window');

export default StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    alignSelf: 'center',
    top: '15%',
  },
  modalView: {
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 10,
    paddingBottom: 0,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    width: width - 50,
  },
  closeIcon: {
    textAlign: 'right',
    color: '#818181',
    paddingRight: 7,
    fontSize: 16,
  },
  content: {
    backgroundColor: 'white',
    padding: 10,
    paddingTop: 0,
    width: width - 50,
    borderBottomEndRadius: 20,
    borderBottomStartRadius: 20,
    flexWrap: 'nowrap',
    flex: 1,
  },
  header: {
    display: 'flex',
    justifyContent: 'center',
    width: '100%',
  },
  root: {
    justifyContent: 'center',
  },
  userIconContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  userDetailContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
  },
  userDetail: { color: '#2c3e50', fontWeight: 'bold', fontSize: 16 },
  buttonLeft: {
    marginTop: 15,
    borderRadius: 30,
    padding: 10,
    backgroundColor: '#b4b4b4',
    width: '40%',
    marginRight: 5,
  },
  buttonRight: {
    marginTop: 15,
    borderRadius: 30,
    padding: 10,
    backgroundColor: BLUE,
    width: '40%',
    marginLeft: 5
  },
  singleButton: {
    marginTop: 15,
    borderRadius: 50,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: BLUE,
    color: 'white',
    width: '35%',
  },
  rankingContainer: { flexDirection: 'row', justifyContent: 'center', marginTop: 5, marginBottom: 5 },
  rankingCounter: {
    color: '#949494',
    fontSize: 10,
    marginBottom: -2,
  },
  divider: {
    borderBottomColor: '#d3d3d3',
    borderBottomWidth: 1,
    marginTop: 20,
    marginBottom: 10,
  },
  commentContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 15,
    marginBottom: 15,
  },
  comment: {
    color: '#2e6f6b',
    fontSize: 16,
    width: '70%',
    textAlign: 'center',
  },
  imageProfile: {
    width: 100,
    height: 100,
    borderRadius: 50,
  },
  buttonsContainer: { flexDirection: 'row' },
  kmTitle: { textAlign: 'center' },
  textButton: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 17,
    color: 'white',
  }
});
