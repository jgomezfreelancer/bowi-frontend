import React from 'react';

import { Marker } from 'react-native-maps';
import Icon from 'react-native-vector-icons/FontAwesome5';

import { ILocation } from './Location';
import { IUser } from '../../reducers/login.reducer';
import { BLUE } from '../../styles/colors';

interface IProps {
  user: IUser;
  location: ILocation;
  handleChat: (user: IUser) => void;
  handlePetition: (user: IUser) => void;
}

export const NearMarker = (props: IProps) => {
  const { user, location, handleChat, handlePetition } = props;
  return (
    <Marker
      coordinate={location}
      onPress={() => (user.petition ? handlePetition(user) : handleChat(user))}
    >
      <Icon color={user.petition ? '#F34900' : BLUE} size={40} name="map-marker-alt" />
    </Marker>
  );
};
