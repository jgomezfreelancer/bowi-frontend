import React, { FunctionComponent, useCallback, useEffect, useRef, useState } from 'react';
import { Modal, View, Text, TouchableHighlight, TextInput, Dimensions } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import MultiSelect from 'react-native-multiple-select';
import IconV5 from 'react-native-vector-icons/FontAwesome5';

import Styles from './style';
import { useDispatch, useSelector } from 'react-redux';
import { getAll } from '../../../reducers/interest.reducer';
import { getServerDate, getUserPetition, sendHelp } from '../../../reducers/login.reducer';
import { AppRootReducer } from '../../../reducers';
import { customTraslate } from '../../../utils/language';
import { Button, CheckBox, Content, ListItem } from 'native-base';
import { BLUE, GREY } from '../../../styles/colors';

const { height } = Dimensions.get('window');

type Props = {
  status: boolean;
  handleModal: (value: boolean) => void;
  handleConfirmModal: (value: boolean) => void;
};

const HelpModal: FunctionComponent<Props> = ({ status, handleModal, handleConfirmModal }) => {
  const [showInterests, setShowInterests] = useState<boolean>(false);
  const [selection, setSelection] = useState<number[]>([]);
  const [message, setMessage] = useState<string>('');
  const [disableButton, setDisableButton] = useState<boolean>(false);
  const [imageSize, setImageSize] = useState({
    width: 300,
    height: 300,
  });
  const inputRef = useRef<TextInput | null>(null);
  const dispatch = useDispatch();
  const {
    loginReducer: {
      user: { id },
    },
    locationReducer: { meters },
    interestReducer: { list },
  } = useSelector((state: AppRootReducer) => state);

  useEffect(() => {
    dispatch(getAll());
  }, [dispatch]);

  const onSubmit = async () => {
    if (message !== '') {
      setDisableButton(true);
      sendHelp({ id, message, km: meters, interests: selection })(dispatch)
        .then(() => {
          handleModal(false);
          handleConfirmModal(true);
          setDisableButton(false);
          dispatch(getUserPetition(id!));
          dispatch(getServerDate());
        })
        .catch(() => {
          handleConfirmModal(false);
          setDisableButton(false);
          inputRef.current!.blur();
        });
    }
  };

  useEffect(() => {
    if (status) {
      inputRef.current!.focus();
    } else {
      setMessage('');
      setSelection([]);
    }

    if (showInterests) {
      inputRef.current!.blur();
    }
  }, [status, showInterests, list]);

  useEffect(() => {
    if (height < 700) {
      setImageSize({ width: 230, height: 230 });
    }
  }, [height]);

  const close = useCallback(() => {
    handleModal(false);
  }, [handleModal]);

  const onMessageChange = (text: string) => setMessage(text);

  const onSelect = (id: number) => {
    const exist = selection.find(element => element === -1);
    if (exist && id === -1) {
      setSelection([]);
      console.log('paso');
    } else if (id === -1) {
      const currentList = [...list];
      const newList = currentList.map(element => element.id);
      setSelection([-1, ...newList]);
    } else {
      const selected = selection.find(element => element === id);
      if (selected) {
        let newArray = selection.filter(element => element !== id);
        if (selection.length > list.length) {
          newArray = newArray.filter(element => element !== -1);
        }

        setSelection(newArray);
      } else {
        const arrayToAdd = [...selection];
        let newList = arrayToAdd.filter(element => element !== -1);
        if (selection.length > list.length) {
          newList = newList.filter(element => element !== -1);
        }
        newList.push(id);
        setSelection(newList);
      }
    }
  };

  const onSelectedItemsChange = (selectedItems: any) => {
    console.log('selectedItems ', selectedItems);
    setSelection(selectedItems);
  };

  const setInterestOptions = (condition: boolean) => {
    setShowInterests(condition);
  };

  const getList = (element: any, i: number) => {
    const current = selection.find(row => row === element.id);
    const checked = !!current;
    return (
      <View style={Styles.interestItemContainer} key={i}>
        <ListItem button onPress={() => onSelect(element.id)}>
          <View style={{ width: '95%' }}>
            <Text>{customTraslate(element.language)}</Text>
          </View>
          <View style={Styles.interestItemText}>
            <CheckBox style={Styles.checkbox} color={BLUE} checked={checked} />
          </View>
        </ListItem>
      </View>
    );
  };

  return (
    <View style={Styles.centeredView}>
      <Modal animationType="slide" transparent={true} visible={status}>
        <View style={Styles.centeredView}>
          <View style={Styles.modalView}>
            <View style={Styles.header}>
              <Icon style={Styles.closeIcon} name="times" onPress={close} />
            </View>
            <View style={Styles.helpTitleContainer}>
              <Text style={Styles.helpText}>{customTraslate('solicitarAyuda').toUpperCase()}</Text>
              <Text style={Styles.helpText2}>{customTraslate('solicitarAyudaDescripcion')}</Text>
            </View>
            <View style={{ width: '80%' }}>
              <Text style={Styles.interestTitle}>{customTraslate('intereses')}</Text>
            </View>
            {showInterests ? (
              <View style={Styles.interestsOptionsContainer}>
                <Content style={{ height: 150, margin: 0 }}>
                  {list.length > 0 &&
                    [
                      {
                        id: -1,
                        description: 'todos',
                        image: '',
                        language: 'todos',
                        slug: 'todos',
                      },
                      ...list,
                    ].map(getList)}
                </Content>

                <View style={{ flex: 2, marginTop: -10 }}>
                  <View style={Styles.selectButtonContainer}>
                    <TouchableHighlight
                      underlayColor="#ffffff"
                      style={Styles.selectButton}
                      onPress={() => setInterestOptions(false)}
                      onPressIn={() => setInterestOptions(false)}
                      onPressOut={() => setInterestOptions(false)}
                    >
                      <Text style={Styles.closeText}>{customTraslate('cerrar')}</Text>
                    </TouchableHighlight>
                  </View>
                </View>
              </View>
            ) : (
              <View style={{ width: '80%', marginTop: 10 }}>
                <View style={Styles.selectContainer}>
                  <Button
                    style={{ width: '90%' }}
                    transparent
                    onPress={() => setInterestOptions(true)}
                  >
                    <Text style={Styles.selectTitle}>
                      {selection.length > list.length
                        ? customTraslate('todos')
                        : selection.length < list.length
                        ? `${selection.length} ${customTraslate('items')}`
                        : customTraslate('seleccione')}
                    </Text>
                  </Button>
                  <Button
                    style={{ width: '10%' }}
                    transparent
                    onPress={() => setInterestOptions(true)}
                  >
                    <Text style={Styles.selectIcon}>
                      <IconV5 size={30} color={GREY} name="sort-down" />
                    </Text>
                  </Button>
                </View>
              </View>
            )}

            {/* <MultiSelect
                hideTags
                items={list}
                uniqueKey="id"
                onSelectedItemsChange={onSelectedItemsChange}
                selectedItems={selection}
                selectText="Items"
                searchInputPlaceholderText="Buscar..."
                onChangeInput={text => console.log(text)}
                altFontFamily="ProximaNova-Light"
                selectedItemTextColor={BLUE}
                selectedItemIconColor={BLUE}
                itemTextColor={GREY}
                displayKey="description"
                searchInputStyle={{ color: '#CCC' }}
                submitButtonColor={BLUE}
                submitButtonText="Seleccionar"
                styleListContainer={{ overflow: 'scroll', height: 100 }}
              /> */}

            <View style={Styles.content}>
              <TextInput
                ref={inputRef}
                onChangeText={onMessageChange}
                placeholder={`${customTraslate('escribeTuMensaje')} ...`}
                placeholderTextColor="#c8c8c8"
                multiline={true}
                numberOfLines={5}
                value={message}
                style={Styles.textArea}
              />
              <View style={Styles.buttonContainer}>
                <TouchableHighlight
                  underlayColor="#ffffff"
                  onPress={onSubmit}
                  style={Styles.sendButton}
                  disabled={disableButton}
                >
                  <Text style={Styles.sendTextButton}>
                    {disableButton
                      ? `${customTraslate('enviando')} ...`
                      : customTraslate('compartir').toUpperCase()}
                  </Text>
                </TouchableHighlight>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
};

export default HelpModal;
