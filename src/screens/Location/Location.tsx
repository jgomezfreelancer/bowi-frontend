import React, { useRef, useCallback, useEffect, useMemo, useState } from 'react';

import {
  Dimensions,
  View,
  StyleSheet,
  Platform,
  PermissionsAndroid,
  TouchableHighlight,
} from 'react-native';
import MapView, { Marker, PROVIDER_GOOGLE, Region } from 'react-native-maps';
import Geolocation, { GeoPosition } from 'react-native-geolocation-service';
import { Col, Row, Text } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import IconV5 from 'react-native-vector-icons/FontAwesome5';
import randomLocation from 'random-location';
import { getDistance } from 'geolib';

import Styles from './style';
import HelpModal from './HelpModal';
import { NearMarker } from './NearMarker';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import { AppRootReducer } from '../../reducers';
import {
  acceptHelp,
  getImage,
  getUsersByLocation,
  IUser,
  setLoadingRequest,
  updateCoord,
} from '../../reducers/login.reducer';
import RequestModal from './RequestModal';
import ConfirmHelpModal from './ConfirmHelpModal';
import {
  setModalPetition,
  setModalRequest,
  setLocation as customSetLocation,
  setCamera,
} from '../../reducers/location.reducer';
import PetitionModal from './PetitionModal';
import { checkPendingNotification } from '../../reducers/notification.reducer';
import setToast from '../../actions/toast.action';
import { isEmpty } from 'lodash';
import { customTraslate } from '../../utils/language';

import ListIcon from '../../assets/icons/icono-mapa-09.svg';
import ListOnlyHelpIcon from '../../assets/icons/icono-mapa-08.svg';
import SearchIcon from '../../assets/icons/icono-mapa-10.svg';
import UbicationIcon from '../../assets//icons/icono-mapa-02.svg';

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;

const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const Location = (): JSX.Element => {
  const [isOnlyHelp, setIsOnlyHelp] = useState<boolean>(false);
  const [everyOneStyles, setEveryOneStyles] = useState({
    ...Styles.regularButtonLeft,
    ...Styles.regularButtonLeftActive,
  });
  const [onlyHelpStyles, setOnlyHelpStyles] = useState({ ...Styles.regularButtonRight });
  const [location, setLocation] = useState<Region>({
    latitude: 0,
    longitude: 0,
    latitudeDelta: 0,
    longitudeDelta: 0,
  });
  const [statusModal, setStatusModal] = useState(false);
  const [statusConfirmModal, setStatusConfirmModal] = useState(false);
  const [hasLocationPermission, setHasLocationPermission] = useState<boolean>(true);
  const {
    locationReducer: { location: newLocation, cameraZoom, meters },
    loginReducer: {
      user: { id, limit_distance },
      usersByLocation,
    },
  } = useSelector((state: AppRootReducer) => state);
  const mapViewRef = useRef<any>();
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const getZoom = useCallback(() => {
    const currentKm = meters > 0 ? meters : limit_distance! * 1000;
    const radius = currentKm + currentKm / 2;
    const scale = radius / 500;
    const newZoom = 16 - Math.log(scale) / Math.log(2);
    dispatch(setCamera(newZoom));
  }, [dispatch, limit_distance, meters]);

  const restoreZoom = () => {
    mapViewRef.current.animateCamera({
      center: {
        latitude: isEmpty(location) ? 0 : location.latitude,
        longitude: isEmpty(location) ? 0 : location.longitude,
      },
      heading: 0,
      pitch: 10,
      zoom: cameraZoom,
      altitude: 0,
    });
  };

  const initGeolocation = useCallback(() => {
    if (newLocation.latitude > 0) {
      const { latitude, longitude } = newLocation;

      setLocation({
        latitude,
        longitude,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      });
    } else {
      Geolocation.getCurrentPosition(
        (res: GeoPosition) => {
          const {
            coords: { latitude, longitude },
          } = res;

          setHasLocationPermission(true);

          setLocation({
            latitude,
            longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
          });
          dispatch(updateCoord(id!, { latitude, longitude }));
          dispatch(getUsersByLocation(id!, meters === 0 ? limit_distance! : meters));
          dispatch(
            customSetLocation(
              {
                latitude,
                longitude,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA,
              },
              limit_distance!,
            ),
          );
          getZoom();
        },
        err => {
          setHasLocationPermission(false);

          return new Error(err.message);
        },
        { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 },
      );
    }
  }, [dispatch, getZoom, id, limit_distance, meters, newLocation]);

  useEffect(() => {
    initGeolocation();
  }, [newLocation, initGeolocation]);

  const handleModal = (status: boolean) => setStatusModal(status);

  const handleSearch = () => {
    navigation.navigate('MyLocation');
  };

  const handleList = () => {
    navigation.navigate('ListOnlyHelp');
  };

  const activateGps = async () => {
    if (Platform.OS === 'android') {
      await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
        .then(() => {
          setHasLocationPermission(true);
          initGeolocation();
        })
        .catch(() => setHasLocationPermission(false));
    }
    if (Platform.OS === 'ios') {
      setHasLocationPermission(true);
      initGeolocation();
    }
  };

  const getMarkerLocation = useMemo(
    () => (markerLocation: any) => {
      const newMeter = 50;
      const randomPoint = randomLocation.randomCirclePoint(location, newMeter);
      const userMeters = getDistance(location, markerLocation);
      if (userMeters <= 10) {
        return {
          latitude: randomPoint.latitude,
          longitude: randomPoint.longitude,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA,
        };
      }
      return {
        latitude: parseFloat(markerLocation.latitude),
        longitude: parseFloat(markerLocation.longitude),
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      };
    },
    [],
  );
  const handleRequest = useCallback(
    (requestId: number, acceptId: number, isPetition: boolean) => () => {
      dispatch(setLoadingRequest(true));

      checkPendingNotification(
        requestId,
        acceptId,
      )(dispatch)
        .then((res: boolean) => {
          if (res) {
            dispatch(
              setToast({
                payload: {
                  message: customTraslate('esperandoRespuestDeUsuario'),
                  status: true,
                  type: 'error',
                },
              }),
            );

            dispatch(setLoadingRequest(false));

            dispatch(
              setModalRequest({
                status: false,
                handleRequest: null,
                user: {},
              }),
            );

            dispatch(
              setModalPetition({
                status: false,
                user: {},
                handleRequest: () => null,
              }),
            );
          } else {
            acceptHelp({ requestId, acceptId, message: 'example message 1', isPetition })(dispatch)

              .then(() => {
                dispatch(setLoadingRequest(false));

                dispatch(
                  setModalRequest({
                    status: false,
                    handleRequest: null,
                    user: {},
                  }),
                );

                dispatch(
                  setModalPetition({
                    status: false,
                    handleRequest: () => null,
                    user: {},
                  }),
                );
              })
              .catch(err => new Error(err));
          }
        })
        .catch(err => new Error(err));
    },
    [dispatch],
  );

  const handleRequestChat = useCallback(
    (userRequest: IUser) => {
      dispatch(
        setModalRequest({
          status: true,
          type: 'request',
          handleRequest: handleRequest(userRequest!.id!, id!, false),
          user: userRequest!,
          isPetition: false,
        }),
      );
    },
    [dispatch, handleRequest, id],
  );

  const handleModalPetition = useCallback(
    (user: IUser) => {
      dispatch(
        setModalPetition({
          status: true,
          handleRequest: handleRequest(user!.id!, id!, true),
          user,
        }),
      );
    },
    [dispatch, handleRequest, id],
  );

  const getEveryone = useCallback(() => {
    setIsOnlyHelp(false);
    setEveryOneStyles({ ...Styles.regularButtonLeft, ...Styles.regularButtonLeftActive });
    setOnlyHelpStyles(Styles.regularButtonRight);
  }, [setIsOnlyHelp, setEveryOneStyles, setOnlyHelpStyles]);

  const getOnlyHelp = useCallback(() => {
    setIsOnlyHelp(true);
    setEveryOneStyles(Styles.regularButtonLeft);
    setOnlyHelpStyles({ ...Styles.regularButtonRight, ...Styles.regularButtonRightActive });
  }, [setIsOnlyHelp, setEveryOneStyles, setOnlyHelpStyles]);

  return hasLocationPermission ? (
    <View style={Styles.container}>
      <MapView
        ref={mapViewRef}
        style={{ ...StyleSheet.absoluteFillObject }}
        provider={Platform.OS === 'android' ? PROVIDER_GOOGLE : null}
        initialRegion={isEmpty(location) ? undefined : location}
        camera={{
          center: {
            latitude: isEmpty(location) ? 0 : location.latitude,
            longitude: isEmpty(location) ? 0 : location.longitude,
          },
          heading: 0,
          pitch: 10,
          zoom: cameraZoom,
          altitude: 0,
        }}
      >
        {Object.keys(location).length > 0 && (
          <Marker coordinate={location}>
            <Text style={Styles.textUserLocation} />
          </Marker>
        )}

        {usersByLocation.length > 0 &&
          usersByLocation.map((user: any, i: number) => {
            const nearLocation = getMarkerLocation(user);
            if (isOnlyHelp) {
              if (user.petition) {
                return (
                  <NearMarker
                    key={i}
                    user={user}
                    location={nearLocation}
                    handleChat={handleRequestChat}
                    handlePetition={handleModalPetition}
                  />
                );
              }
            } else {
              return (
                <NearMarker
                  key={i}
                  user={user}
                  location={nearLocation}
                  handleChat={handleRequestChat}
                  handlePetition={handleModalPetition}
                />
              );
            }
          })}
      </MapView>

      <TouchableHighlight underlayColor="white" style={Styles.myLocation} onPress={handleSearch}>
        <SearchIcon width={20} height={20}/>
      </TouchableHighlight>
      <TouchableHighlight underlayColor="white" style={Styles.myLocation3} onPress={restoreZoom}>
          <UbicationIcon width={50} height={50}/>
      </TouchableHighlight>
      {
        isOnlyHelp != true ? 
          <TouchableHighlight underlayColor="white" style={Styles.myLocation2} onPress={handleList}>
            <ListIcon width={20} height={20}/>
          </TouchableHighlight> : 
          <TouchableHighlight underlayColor="white" style={Styles.myLocation2} onPress={handleList}>
            <ListOnlyHelpIcon width={20} height={20}/>
          </TouchableHighlight>
      }
      <Row style={Styles.rowButtonsContainer}>
        <Col>
          <Text style={everyOneStyles} onPress={getEveryone}>
            {customTraslate('todos').toUpperCase()}
          </Text>
        </Col>

        <Col>
          <Text style={onlyHelpStyles} onPress={getOnlyHelp}>
            {customTraslate('soloAyuda').toUpperCase()}
          </Text>
        </Col>
      </Row>

      <View style={Styles.helpContainer}>
        <View style={Styles.helpButtonContainer}>
          <View style={Styles.row}></View>
          <Text style={Styles.helpButton} onPress={() => handleModal(true)}>
            {customTraslate('solicitarAyuda')}
          </Text>
          <View style={Styles.row}></View>
        </View>

        <HelpModal
          status={statusModal}
          handleModal={handleModal}
          handleConfirmModal={setStatusConfirmModal}
        />

        <ConfirmHelpModal status={statusConfirmModal} handleModal={setStatusConfirmModal} />

        <PetitionModal />

        <RequestModal />
      </View>
    </View>
  ) : (
    <View style={Styles.gpsActiveContainer}>
      <IconV5 color="#818181" size={30} name="map-marked-alt" />

      <Text style={Styles.gpsButton} onPress={activateGps}>
        {customTraslate('activarGps').toUpperCase()}
      </Text>
    </View>
  );
};

export default Location;
