import React, { FunctionComponent, useEffect } from 'react';
import { Modal, View, Text } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { customTraslate } from '../../../utils/language';

import Styles from './style';

type Props = {
  status: boolean;
  handleModal: (value: boolean) => void;
};

const ConfirmHelpModal: FunctionComponent<Props> = ({ status, handleModal }) => {
  useEffect(() => {
    if (status) {
      const timer = setTimeout(() => {
        handleModal(false);
      }, 3000);
      return () => clearTimeout(timer);
    }
  }, [status]);

  return (
    <View style={Styles.centeredView}>
      <Modal animationType="slide" transparent={true} visible={status}>
        <View style={Styles.centeredView}>
          <View style={Styles.modalView}>
            <View style={Styles.contentContainer}>
              <Text style={Styles.text1t}>
                {customTraslate('tuMensajeHaSidoCompartido').toUpperCase()}
              </Text>
              <Text style={Styles.iconContainer}>
                <Icon color="white" size={70} name="exclamation" />
              </Text>
              <Text style={Styles.text2}>
                {customTraslate('tuPetticionExpiraraEn')}{' '}
                <Text style={Styles.text3}>1 {customTraslate('hora')}</Text>
              </Text>
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
};

export default ConfirmHelpModal;
