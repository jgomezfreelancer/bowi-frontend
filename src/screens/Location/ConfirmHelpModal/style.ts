import { StyleSheet, Dimensions, TextStyle, PointPropType } from 'react-native';

import { BLUE, GREY } from '../../../styles/colors';

const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    alignSelf: 'center',
    top: '10%',
  },
  modalView: {
    backgroundColor: BLUE,
    borderRadius: 20,
    padding: 10,
    paddingBottom: 0,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    width: width - 50,
  },
  header: {
    display: 'flex',
    justifyContent: 'center',
    width: '100%',
  },
  closeIcon: {
    textAlign: 'right',
    color: GREY,
    paddingRight: 7,
    fontSize: 16,
  },
  contentContainer: { marginTop: 20, marginBottom: 20 },
  text1t: {
    color: 'white',
    fontSize: 20,
    textAlign: 'center',
  },
  iconContainer: { marginTop: 20, textAlign: 'center' },
  text2: { marginTop: 20, textAlign: 'center', fontStyle: 'italic', color: 'white', fontSize: 16 },
  text3: {
    marginTop: 20,
    textAlign: 'center',
    fontStyle: 'italic',
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
  },
  helpImage: { zIndex: 10 },
  textArea: { backgroundColor: '#f5f5f5', paddingTop: 0, paddingBottom: 20, borderRadius: 20 },
});
