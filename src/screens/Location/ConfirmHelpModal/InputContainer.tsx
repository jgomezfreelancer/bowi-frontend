import React from 'react';
import { View, TextInput, TextInputProps } from 'react-native';

interface IProps extends TextInputProps {
  onBlur: () => void;
  onChange: () => void;
  value: string;
}

export const InputContainer = (props: IProps) => {
  const { onBlur, onChange, value } = props;
  return (
    <View>
      <TextInput
        onBlur={onBlur}
        onChangeText={onChange}
        value={value}
        placeholder="Introduzca su mensaje aqui ..."
        placeholderTextColor="#c8c8c8"
        multiline={true}
        numberOfLines={3}
      />
    </View>
  );
};
