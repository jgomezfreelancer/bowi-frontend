import { StyleSheet, Dimensions, TextStyle, PointPropType } from 'react-native';
import { isIOS } from '../../../helpers/isIOS';
import { BLUE, GREY } from '../../../styles/colors';

const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    alignSelf: 'center',
    top: '10%',
  },
  modalView: {
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 10,
    paddingBottom: 0,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    width: width - 50,
  },
  closeIcon: {
    textAlign: 'right',
    color: GREY,
    paddingRight: 7,
    fontSize: 16,
  },
  content: {
    backgroundColor: 'white',
    padding: 10,
    paddingTop: 5,
    width: width - 50,
    borderBottomEndRadius: 20,
    borderBottomStartRadius: 20,
    flexWrap: 'nowrap',
    flex: 1,
    paddingLeft: 40,
    paddingRight: 40
  },
  header: {
    display: 'flex',
    justifyContent: 'center',
    width: '100%',
  },
  textContent: {
    color: '#9f9f9f',
    textAlign: 'left',
  },
  helpImageContainer: {},
  buttonContainer: {
    backgroundColor: 'transparent',
    alignItems: 'center',
    marginTop: isIOS ? 30 : 15,
    marginBottom: 10,
    position: 'relative',
  },
  sendButton: {
    backgroundColor: '#F34900',
    alignItems: 'center',
    width: '60%',
    padding: 10,
    borderRadius: 30,
    borderColor: '#F34900',
  },
  sendTextButton: {
    color: 'white',
    fontWeight: 'bold'
  },
  helpText: {
    color: BLUE,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  helpText2: {
    color: GREY,
    fontSize: 13,
    fontStyle: 'italic',
    textAlign: 'center',
    paddingLeft: 15,
    paddingRight: 15,
    marginTop: 10
  },
  helpImage: { zIndex: 10 },
  messageArea: { backgroundColor : '#f5f5f5', padding: 20, borderRadius: 20, marginTop: 5 },
  helpTitleContainer: { marginTop: 20, marginBottom: 20 },
  userIconContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  userDetailContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 5,
    marginBottom: 5,
  },
  userDetail: { color: '#2c3e50', fontWeight: 'bold', fontSize: 16 },
  rankingContainer: { flexDirection: 'row', justifyContent: 'center' },
  imageProfile: {
    width: 100,
    height: 100,
    borderRadius: 50,
  },
});
