import React, { useEffect } from 'react';

import { Modal, View, Text, Image, TouchableHighlight } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Stars from 'react-native-stars';
import { getDistance } from 'geolib';

import Styles from './style';
import { useDispatch, useSelector } from 'react-redux';
import { cleanUserProfileImage, getUserProfileImage } from '../../../reducers/login.reducer';
import { AppRootReducer } from '../../../reducers';
import { setModalPetition } from '../../../reducers/location.reducer';
import { BLUE } from '../../../styles/colors';
import { customTraslate } from '../../../utils/language';

function PetitionModal(): JSX.Element {
  const {
    locationReducer: {
      modalPetition: { status, handleRequest, user },
      location,
    },
    loginReducer: { userProfileImage, acceptLoading },
  } = useSelector((state: AppRootReducer) => state);
  const dispatch = useDispatch();
  const { id, first_name, last_name, ranking, petition, latitude, longitude } = user;

  const getMeters = () => {
    const requestUserLocation = { latitude: latitude!, longitude: longitude! };
    const meters = getDistance(location, requestUserLocation);
    return meters <= 1000 ? `${meters} Mtrs` : `${Math.round(meters! / 1000)} Km`;
  };

  useEffect(() => {
    if (status) {
      dispatch(getUserProfileImage(id!));
    } else {
      dispatch(cleanUserProfileImage());
      dispatch(
        setModalPetition({
          status: false,
          handleRequest: () => null,
          user: {},
        }),
      );
    }
  }, [dispatch, id, status]);

  const close = () => {
    dispatch(
      setModalPetition({
        status: false,
        handleRequest: () => null,
        user: {},
      }),
    );
  };
  return (
    <View style={Styles.centeredView}>
      <Modal animationType="slide" transparent={true} visible={status}>
        <View style={Styles.centeredView}>
          <View style={Styles.modalView}>
            <View style={Styles.header}>
              <Icon style={Styles.closeIcon} name="times" onPress={close} />
            </View>
            <View style={Styles.userIconContainer}>
              {userProfileImage ? (
                <Image source={{ uri: userProfileImage }} style={Styles.imageProfile} />
              ) : (
                <Icon size={70} color="#F34900" name="user-circle" solid />
              )}
            </View>
            <View style={Styles.userDetailContainer}>
              <Text style={Styles.userDetail}>
                {first_name} {last_name?.charAt(0)}
              </Text>
            </View>
            <View style={Styles.rankingContainer}>
              <Stars
                disabled
                default={ranking ? parseInt(String(ranking), 36) : 0}
                count={5}
                half={true}
                starSize={50}
                fullStar={<Icon size={12} color={BLUE} name="star" solid />}
                emptyStar={<Icon size={12} color={BLUE} name="star" light />}
                halfStar={<Icon size={12} color={BLUE} name="star-half-alt" solid />}
              />
            </View>
            <Text>{latitude && longitude! ? `a ${getMeters()}` : ''}</Text>
            <View style={Styles.content}>
              <Text style={Styles.messageArea}>{petition && petition.description}</Text>

              <View style={Styles.buttonContainer}>
                <TouchableHighlight
                  underlayColor="#F34900"
                  onPress={handleRequest ? handleRequest : () => null}
                  style={Styles.sendButton}
                  disabled={acceptLoading}
                >
                  <Text style={Styles.sendTextButton}>
                    {acceptLoading
                      ? `${customTraslate('enviando')}...`
                      : `${customTraslate('ayudar').toUpperCase()}`}
                  </Text>
                </TouchableHighlight>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
}

export default PetitionModal;
