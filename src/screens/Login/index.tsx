import React from 'react';
import { Text, TouchableHighlight, View, ImageBackground } from 'react-native';
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Container, Header } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';

import Input from '../../components/common/form/Input2';
import { login } from '../../reducers/login.reducer';
import Button from '../../components/common/form/Button';
import { useNavigation } from '@react-navigation/native';
import Styles from './style';
import { AppRootReducer } from '../../reducers';
import { customTraslate } from '../../utils/language';
import ArrowIcon from '../../assets/icons/flecha.svg';
// const backgroundImage = require('../../assets/images/fondo-regístrate-03.png');
import BowiLogoIcon from '../../assets/images/bowi-logo.svg';

type FormData = {
  username: string;
  password: string;
};

export type IFB = {
  email: string;
  first_name: string;
  last_name: string;
};

function Login(): JSX.Element {
  const { handleSubmit, control, errors } = useForm<FormData>();
  const dispatch = useDispatch();
  const {
    loginReducer: {
      loading,
      user: { first_time },
    },
  } = useSelector((state: AppRootReducer) => state);
  const navigation = useNavigation();

  const onSubmit = async (form: FormData) => {
    login(form)(dispatch)
      .then(() => {
        if (first_time) {
          navigation.navigate('Starter');
        } else {
          navigation.navigate('TabNavigator');
        }
      })
      .catch(err => new Error(err));
  };

  const handleBack = () => {
    navigation.navigate('Main');
  };

  const handleForgotPassword = () => {
    navigation.navigate('ForgotPassword');
  };

  return (
    <Container>
      <Header style={Styles.headerStyles}>
        <View style={Styles.backButtonContainer}>
          <TouchableHighlight onPress={handleBack} underlayColor="transparent">
            <ArrowIcon width={25} height={25}/>
          </TouchableHighlight>
        </View>
      </Header>
      {/* <ImageBackground resizeMode="stretch" source={backgroundImage} style={Styles.backgroundImage}> */}
            {/* </ImageBackground> */}
        <View style={Styles.loginContainer}>
          <View style={Styles.logoContainer}>
            <BowiLogoIcon style={Styles.logo}/>
          </View>
          <View>
            <Text style={Styles.loginText}>{customTraslate('iniciarSesion')}</Text>
          </View>
          <View style={Styles.fieldContainer}>
            <Input
              control={control}
              name="username"
              required
              error={errors.username ? errors.username.message : null}
              placeholder="Usuario"
              iconName="envelope"
            />
          </View>
          <View style={Styles.fieldContainer}>
            <Input
              control={control}
              name="password"
              required
              error={errors.password ? errors.password.message : null}
              placeholder="Contraseña"
              secureText
              iconName="lock"
            />
          </View>
          <View style={Styles.fieldContainer}>
            <Button
              loading={loading}
              handleSubmit={handleSubmit(onSubmit)}
              title={customTraslate('entrar')}
            />
          </View>
          <View style={Styles.fieldContainer}>
            <Text style={Styles.forgotPasswordText} onPress={handleForgotPassword}>
              {customTraslate('olvidoClave')}
            </Text>
          </View>
        </View>
    </Container>
  );
}

export default Login;
