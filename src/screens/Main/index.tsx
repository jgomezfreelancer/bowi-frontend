import React from 'react';
import { ImageBackground, Text, TouchableHighlight } from 'react-native';
import { isIOS } from '../../helpers/isIOS';
import { useDispatch, useSelector } from 'react-redux';
import { Container, Grid, Row } from 'native-base';
import { User } from '@react-native-google-signin/google-signin';

import { IUser, sociaLogin } from '../../reducers/login.reducer';
import Styles from './style';
import GoogleLogin from '../../components/GoogleLogin';
import FacebookLogin from '../../components/FacebookLogin';
import AppleLogin from '../../components/AppleLogin';
import { useNavigation } from '@react-navigation/native';
import { AppRootReducer } from '../../reducers';
import BowiLogoIcon from '../../assets/images/bowi-logo.svg';
import { customTraslate } from '../../utils/language';
const backgroundImage = require('../../assets/images/background-login.png');

export type IFB = {
  email: string;
  first_name: string;
  last_name: string;
};

function Main(): JSX.Element {
  const dispatch = useDispatch();
  const {
    loginReducer: { loading },
  } = useSelector((state: AppRootReducer) => state);
  const navigation = useNavigation();

  const handleGoogleLogin = async (form: User) => {
    const {
      user: { email, givenName, familyName },
    } = form;
    const body = {
      email,
      first_name: givenName,
      last_name: familyName,
    };
    sociaLogin(body)(dispatch)
      .then((userRes: IUser) => {
        if (userRes.first_time) {
          navigation.navigate('Starter');
        } else {
          navigation.navigate('TabNavigator');
        }
      })
      .catch(err => console.error(err));
  };

  const handleFacebookLogin = async (form: IFB) => {
    sociaLogin(form)(dispatch)
      .then((userRes: IUser) => {
        if (userRes.first_time) {
          navigation.navigate('Starter');
        } else {
          navigation.navigate('TabNavigator');
        }
      })
      .catch(err => console.error(err));
  };

  const handleAppleLogin = async (form: IFB) => { };

  const handleLogin = () => {
    navigation.navigate('Login');
  };

  const handleRegister = () => {
    navigation.navigate('Register');
  };

  return (
    <Container style={Styles.root}>
      <ImageBackground resizeMode="stretch" source={backgroundImage} style={Styles.imageBackground}>
      </ImageBackground>
      <Grid style={Styles.gridLogoContainer}>
        <Row size={50} style={Styles.fieldContainer}>
          <BowiLogoIcon style={Styles.logoImage} />
        </Row>
        <Row size={50}>
          <Grid style={Styles.fieldContainer}>
            <Row size={25}>
              {isIOS === true ? <AppleLogin loading={loading} handle={handleAppleLogin} /> : null}
            </Row>
            <Row size={25}>
              <FacebookLogin loading={loading} handle={handleFacebookLogin} />
            </Row>
            <Row size={25}>
              <GoogleLogin loading={loading} handleLogin={handleGoogleLogin} />
            </Row>
            <Row size={25}>
              <TouchableHighlight
                underlayColor="#e3e3e3"
                style={Styles.registerButton}
                onPress={handleRegister}
              >
                <Text style={Styles.buttonText}>{customTraslate('registrate')}</Text>
              </TouchableHighlight>
            </Row>
            <Row size={20} style={Styles.orTitleContainer}>
              <Text onPress={handleLogin} style={Styles.haveAccount}>{customTraslate('tienesCuenta')}</Text>
              <Text onPress={handleLogin} style={Styles.login}>{customTraslate('iniciarSesion')}</Text>
            </Row>
          </Grid>
        </Row>
      </Grid>
    </Container>
  );
}

export default Main;
