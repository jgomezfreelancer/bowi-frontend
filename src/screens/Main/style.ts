import { relativeTimeRounding } from 'moment';
import { StyleSheet } from 'react-native';
import { BLUE, FONT } from '../../styles/colors';

export default StyleSheet.create({
  root: {
    width: '100%',
    paddingRight: -10
  },
  gridLogoContainer: { 
    paddingBottom: 25,
    paddingLeft: 50,
    paddingRight: 50,
},
  fieldContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  dividerLine: {
    backgroundColor: '#e2e2e2',
    height: 1,
    flex: 1,
    alignSelf: 'center',
    marginTop: 5,
  },
  dividerLineText: {
    alignSelf: 'center',
    paddingHorizontal: 5,
    fontSize: 18,
    color: '#e2e2e2',
    marginLeft: 5,
    marginRight: 5,
  },
  textLogoTitle: {
    color: '#53e6f3',
    fontSize: 25,
  },
  loginButton: {
    borderRadius: 30,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    margin: 5,
    backgroundColor: BLUE,
    borderColor: BLUE,
  },
  registerButton: {
    borderRadius: 30,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    margin: 5,
    backgroundColor: '#fff',
    borderColor: '#fff',
  },
  buttonText: { color: 'black', fontWeight: '800', fontFamily: FONT },
  logoContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
    marginBottom: 30,
    backgroundColor: 'blue',
  },
  logo: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 150,
    height: 150,
    borderRadius: 80,
    borderColor: '#fff',
    borderWidth: 5,
    textAlign: 'center',
  },
  logoText: {
    color: '#fff',
    fontSize: 25,
    fontWeight: 'bold',
  },
  logoImage: {
    width: 95,
    height: '100%',
  },
  orTitleContainer: {
    paddingTop: 15,
    justifyContent: 'center',
  },
  orTitle: {
    color: '#a5a5a5',
    fontWeight: 'bold',
  },
  imageBackground: {
    width: '100%',
    height: '100%',
    flex: 1,
    marginTop: '60%',
    position: 'absolute',
  },
  login: {
    paddingLeft: 7,
    color: '#ffffff',
    fontWeight: 'bold',
    textDecorationLine: 'underline'
  },
  haveAccount: {
    color: '#ffffff',
    fontWeight: '400',
  }
});
