import React, { FunctionComponent, useEffect, useState } from 'react';
import { View, Dimensions, ScrollView, Switch } from 'react-native';
// import Slider from '@react-native-community/slider';
import { GoogleSignin } from '@react-native-google-signin/google-signin';
import { Container, Content, Text, Left, List, ListItem, Grid, Right } from 'native-base';

import Styles from './style';
import Icon from 'react-native-vector-icons/FontAwesome';
// import IconV5 from 'react-native-vector-icons/FontAwesome5';
import isEmpty from 'lodash/isEmpty';

import CustomHeader from '../../components/common/Header';
import { useDispatch, useSelector } from 'react-redux';
import setModal from '../../actions/modal.action';
import Confirm from '../../components/common/Confirm';
import { getUsersByLocation, logout, updateUserSettings } from '../../reducers/login.reducer';
import { AppRootReducer } from '../../reducers';
// import TraslateIconActive from '../../assets/icons/icono-07.svg';
import { setCamera, setLocation as customSetLocation } from '../../reducers/location.reducer';
// import { BLUE } from '../../styles/colors';
import AsyncStorage from '@react-native-community/async-storage';
import { customTraslate } from '../../utils/language';

import MyProfileIcon from '../../assets/icons/iconos-perfil-09.svg';
import LanguageIcon from '../../assets/icons/iconos-perfil-08.svg';
import SourceIcon from '../../assets/icons/iconos-perfil-07.svg';
import HowCanYouHelpIcon from '../../assets/icons/iconos-perfil-06.svg';
import SettingsIcon from '../../assets/icons/iconos-perfil-05.svg';
import SuggestionsIcon from '../../assets/icons/iconos-perfil-04.svg';
import DonationsIcon from '../../assets/icons/iconos-perfil-03.svg';
import ShareIcon from '../../assets/icons/iconos-perfil-02.svg';
import Share from '../../assets/icons/compartir-02.svg';

const { width } = Dimensions.get('screen');

type Props = {
  navigation: any;
};

type FormData = {
  location: boolean;
  profile: boolean;
};

const Settings: FunctionComponent<Props> = ({ navigation }) => {
  const [location, setLocation] = useState<boolean>(false);
  const [profile, setProfile] = useState<boolean>(false);
  const [sliderValue, setSliderValue] = useState(50);
  const dispatch = useDispatch();

  const {
    loginReducer: { user },
    locationReducer: { location: customLocation },
  } = useSelector((state: AppRootReducer) => state);

  useEffect(() => {
    if (!isEmpty(user)) {
      const { location, profile, limit_distance } = user;
      setProfile(profile === 1);
      setLocation(location === 1);
      if (limit_distance! > 0) {
        setSliderValue(limit_distance!);
      }
    }
  }, [user]);

  const handleLanguages = () => navigation.navigate('Languages');

  const handleNationalities = () => navigation.navigate('Nationalities');

  const handleSettings = () => navigation.navigate('ChangeSettings');

  const handleEditProfile = () => navigation.navigate('EditProfile');

  const handleInterest = () => navigation.navigate('Interest');

  const googleSignOut = async () => {
    try {
      await GoogleSignin.signOut();
    } catch (error) {
      console.error(error);
    }
  };

  const handleConfirmLogout = async () => {
    setModal({
      payload: {
        status: false,
        children: null,
      },
    })(dispatch);
    const token = await AsyncStorage.getItem('token');
    logout(token!)(dispatch)
      .then(() => {
        googleSignOut();
        navigation.navigate('Main');
      })
      .catch(err => new Error(err));
  };

  const handleLogout = () => {
    setModal({
      payload: {
        status: true,
        children: (
          <Confirm
            message={`${customTraslate('cerrarSesionMensajeModal')}`}
            leftText={customTraslate('cancelar')}
            rightText={customTraslate('cerrarSesion')}
            handleConfirm={() => handleConfirmLogout()}
          />
        ),
      },
    })(dispatch);
  };

  const onSliderChange = (e: number) => {
    setSliderValue(e);
  };

  const getSliderTextValue = (): number => {
    return sliderValue >= 0 ? 130 : sliderValue >= 50 ? 80 : 0;
  };

  const value = getSliderTextValue();
  const left = (sliderValue * (width - 100)) / 100 - value;

  const onLocationChange = (valueLocation: boolean) => {
    setLocation(valueLocation);
    dispatch(updateUserSettings(user.id!, { location: valueLocation ? 1 : 0 }));
  };

  const onProfileChange = (valueProfile: boolean) => {
    setProfile(valueProfile);
    dispatch(updateUserSettings(user.id!, { profile: valueProfile ? 1 : 0 }));
  };

  const getZoom = () => {
    const currentKm = sliderValue * 1000;
    let zoom = 18;

    if (currentKm >= 1000 && currentKm <= 1999) zoom = 14;
    if (currentKm >= 2000 && currentKm <= 6999) zoom = 12;
    if (currentKm >= 7000 && currentKm <= 29999) zoom = 10;
    if (currentKm >= 30000 && currentKm <= 69999) zoom = 8;
    if (currentKm >= 70000) zoom = 8;
    dispatch(setCamera(zoom));
  };

  const onSliderComplete = (sliderDistante: number) => {
    updateUserSettings(user.id!, { limit_distance: sliderDistante! })(dispatch)
      .then(() => {
        dispatch(customSetLocation(customLocation, sliderDistante));
        dispatch(getUsersByLocation(user.id!, sliderDistante!));
      })
      .catch(err => err);
  };

  useEffect(() => {
    getZoom();
  }, [sliderValue]);

  return (
    <Container>
      <CustomHeader title={customTraslate('ajustes')} />
      <Content>
        <ScrollView>
          <List>
            <ListItem style={Styles.listItemsContainer} onPress={handleEditProfile}>
              <Grid style={Styles.gridContainer}>
                <Left>
                  <MyProfileIcon width={25} height={25} style={Styles.settingIcon} />
                  <Text style={Styles.textField}>{customTraslate('miPerfil')}</Text>
                </Left>
                <Right>
                  <Icon
                    style={Styles.settingIcon}
                    size={20}
                    color={'#989898'}
                    name="chevron-right"
                  />
                </Right>
              </Grid>
            </ListItem>
            <ListItem style={Styles.listItemsContainer} onPress={handleLanguages}>
              <Grid style={Styles.gridContainer}>
                <Left>
                  <LanguageIcon width={25} height={25} style={Styles.settingIcon} />
                  <Text style={Styles.textField}>{customTraslate('idioma')}</Text>
                </Left>
                <Right>
                  <Icon
                    style={Styles.settingIcon}
                    size={20}
                    color={'#989898'}
                    name="chevron-right"
                  />
                </Right>
              </Grid>
            </ListItem>
            <ListItem style={Styles.listItemsContainer} onPress={handleNationalities}>
              <Grid style={Styles.gridContainer}>
                <Left>
                  <SourceIcon width={25} height={25} style={Styles.settingIcon} />
                  <Text style={Styles.textField}>{customTraslate('nacionalidad')}</Text>
                </Left>
                <Right>
                  <Icon style={Styles.settingIcon} size={20} color="#989898" name="chevron-right" />
                </Right>
              </Grid>
            </ListItem>
            <ListItem style={Styles.listItemsContainer} onPress={handleInterest}>
              <Grid style={Styles.gridContainer}>
                <Left>
                  <HowCanYouHelpIcon width={25} height={25} style={Styles.settingIcon} />
                  <Text style={Styles.textField}>{customTraslate('tusIntereses')}</Text>
                </Left>
                <Right>
                  <Icon
                    style={Styles.settingIcon}
                    size={20}
                    color={'#989898'}
                    name="chevron-right"
                  />
                </Right>
              </Grid>
            </ListItem>
            <ListItem style={Styles.listItemsContainer} onPress={handleSettings}>
              <Grid style={Styles.gridContainer}>
                <Left>
                  <SettingsIcon width={25} height={25} style={Styles.settingIcon} />
                  <Text style={Styles.textField}>{customTraslate('configuraciones')}</Text>
                </Left>
                <Right>
                  <Icon
                    style={Styles.settingIcon}
                    size={20}
                    color={'#989898'}
                    name="chevron-right"
                  />
                </Right>
              </Grid>
            </ListItem>
            <ListItem style={Styles.listItemsContainer}>
              <Grid style={Styles.gridContainer}>
                <Left>
                  <SuggestionsIcon width={25} height={25} style={Styles.settingIcon} />
                  <Text style={Styles.textField}>{customTraslate('sugerencias')}</Text>
                </Left>
                <Right>
                  <Icon
                    style={Styles.settingIcon}
                    size={20}
                    color={'#989898'}
                    name="chevron-right"
                  />
                </Right>
              </Grid>
            </ListItem>
            <ListItem style={Styles.listItemsContainer}>
              <Grid style={Styles.gridContainer}>
                <Left>
                  <DonationsIcon width={25} height={25} style={Styles.settingIcon} />
                  <Text style={Styles.textField}>{customTraslate('donaciones')}</Text>
                </Left>
                <Right>
                  <Icon
                    style={Styles.settingIcon}
                    size={20}
                    color={'#989898'}
                    name="chevron-right"
                  />
                </Right>
              </Grid>
            </ListItem>
            <ListItem style={Styles.listItemsContainer}>
              <Grid style={Styles.gridContainer}>
                <Left>
                  <ShareIcon width={28} height={28} style={Styles.settingIcon} />
                  <Text style={Styles.textField}>{customTraslate('compartirHelpo')}</Text>
                </Left>
                <Right>
                  <Share width={28} height={28} style={Styles.settingIcon} />
                </Right>
              </Grid>
            </ListItem>
          </List>
        </ScrollView>
      </Content>
      <Text style={Styles.logoutText} onPress={handleLogout}>
        {customTraslate('cerrarSesion')}
      </Text>
    </Container>
  );
};

export default Settings;
