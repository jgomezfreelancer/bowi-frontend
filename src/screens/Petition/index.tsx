import React, { useCallback } from 'react';
import { Container, Header, Left, Button, Body, Title, Right, Grid, Row, Col } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';

import Styles from './style';
import { RouteProp, useNavigation, useRoute } from '@react-navigation/native';
import { Text, View } from 'react-native';
import { getUsersByLocation, IUser } from '../../reducers/login.reducer';
import { getUserRooms, IUserRooms, setChat } from '../../reducers/room.reducer';
import { useDispatch, useSelector } from 'react-redux';
import { AppRootReducer } from '../../reducers';
import Message from '../../helpers/message';
import setToast from '../../actions/toast.action';

type RouteParamList = {
  petition: {
    petitionUser: IUser;
  };
};

export default function Petition(): JSX.Element {
  const {
    loginReducer: { user },
  } = useSelector((state: AppRootReducer) => state);
  const dispatch = useDispatch();
  const {
    params: { petitionUser },
  } = useRoute<RouteProp<RouteParamList, 'petition'>>();
  const navigation = useNavigation();
  const left = useCallback(() => {
    navigation.navigate('Location');
  }, []);

  const handleChat = () => {
    setChat(
      petitionUser.id!,
      user.id!,
      petitionUser.petition.id,
    )(dispatch)
      .then((room: IUserRooms) => {
        dispatch(getUserRooms(user.id!));
        navigation.navigate('Chat', { room, sender: room.user_participant });
        dispatch(getUsersByLocation(user.id!, user.limit_distance!));
      })
      .catch(error => {
        const errorMessage = Message.exception(error);
        setToast({
          payload: {
            message: errorMessage,
            status: true,
            type: 'error',
          },
        })(dispatch);
      });
  };

  return (
    <Container>
      <Header style={Styles.header}>
        <Left>
          <Button transparent onPress={left}>
            <Icon name="arrow-left" size={25} color="#989898" />
          </Button>
        </Left>
        <Body>
          <Title style={Styles.headerTitle}>{petitionUser.first_name}</Title>
        </Body>
        <Right />
      </Header>
      <Grid>
        <Row size={17}>
          <View style={Styles.requestDescription}>
            <Text style={Styles.requestDescriptionTitle}>{petitionUser.first_name}</Text>
            <Text style={Styles.requestDescriptionSubTitle}>
              {petitionUser.petition && petitionUser.petition.description}
            </Text>
          </View>
        </Row>
        <Row size={10}>
          <View style={Styles.temporalMessageContainer}>
            <Text style={Styles.temporalMessage}>
              Este chat se borrará a las 24h desde su inicio
            </Text>
          </View>
        </Row>
        <Row size={50} style={Styles.rowButtonsContainer}>
          <Grid style={Styles.gridButtonsContainer}>
            <Col>
              <Text style={Styles.decline} onPress={left}>
                RECHAZAR
              </Text>
            </Col>
            <Col>
              <Text style={Styles.accept} onPress={handleChat}>
                ACEPTAR
              </Text>
            </Col>
          </Grid>
        </Row>
      </Grid>
    </Container>
  );
}
