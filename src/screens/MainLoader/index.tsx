import React from 'react';
import { View, ActivityIndicator, Text } from 'react-native';
import { Container, Grid, Row } from 'native-base';
import TraslateIcon from '../../assets/images/bowi-logo.svg';
import style from './style';

function MainLoader(): JSX.Element {
  return (
    <Container style={style.root}>
      <Grid style={style.gridContainer}>
        <Row style={style.rowContainer}>
          <View>
            <TraslateIcon width={100} height={100}/>
            <View style={style.loader}>
              <ActivityIndicator size="large" color="#42b3b1"/>
            </View>
          </View>
        </Row>
      </Grid>
    </Container>
  );
}

export default MainLoader;
