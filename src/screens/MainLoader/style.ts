import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  root: {
    width: '100%',
    padding: 20,
    height: '100%',
    backgroundColor: '#f5f2ef',
    display: 'flex',
    justifyContent: 'center',
  },
  gridContainer: { alignItems: 'center' },
  rowContainer: { alignItems: 'center' },
  text: { color: 'white', fontSize: 16, marginTop: 20 },
  loader:{
    paddingTop: 50
  }
});
