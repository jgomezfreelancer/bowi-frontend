import React, { useCallback, useEffect, useState } from 'react';
import { Container, Header, Left, Button, Body, Title, Grid, Row, Right } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import moment, { Moment } from 'moment';

import Styles from './style';
import { useNavigation } from '@react-navigation/native';
import { Text, View } from 'react-native';
import { disablePetition, getServerDate, getUserPetition } from '../../reducers/login.reducer';
import { useDispatch, useSelector } from 'react-redux';
import { AppRootReducer } from '../../reducers';
import setModal from '../../actions/modal.action';
import Confirm from '../../components/common/Confirm';
import { customTraslate } from '../../utils/language';

export default function PetitionDetail(): JSX.Element {
  const [counter, setCounter] = useState({
    hour: 0,
    minute: 0,
    second: 0,
  });
  const [counterTime, setCounterTime] = useState<Moment | null>(null);
  const {
    loginReducer: { user, userPetition, serverDate },
  } = useSelector((state: AppRootReducer) => state);
  const navigation = useNavigation();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getServerDate());
    dispatch(getUserPetition(user.id!));
  }, [dispatch]);

  useEffect(() => {
    if (Object.keys(userPetition).length > 0) {
      const expiration = moment(userPetition.created).add(1, 'hour');
      const expirationHour = expiration.format('HH:mm:ss');
      const serverHour = moment(serverDate).format('HH:mm:ss');
      const start = moment(expirationHour, 'HH:mm:ss');
      const end = moment(serverHour, 'HH:mm:ss');
      const startMinuteTime = moment.duration(start - end);
      const initialTime = `00:${startMinuteTime.minutes()}:${startMinuteTime.seconds()}`;
      setCounterTime(moment(`2020-01-01 ${initialTime}`));
    }
  }, [userPetition, serverDate]);

  useEffect(() => {
    if (Object.keys(userPetition).length > 0 && counterTime) {
      const interval = setInterval(() => {
        const valueCounter = counterTime;
        const petitionCounter = moment(valueCounter).subtract(1, 'second');
        const minute = petitionCounter.minute();
        setCounterTime(petitionCounter);
        setCounter({
          ...counter,
          hour: 0,
          minute: minute.toString().length === 1 ? `0${minute}` : minute,
          second: petitionCounter.second(),
        });
        if (petitionCounter.minute() === 0 && petitionCounter.second() === 0) {
          disablePetition(userPetition.id!)(dispatch)
            .then(() => {
              dispatch(getUserPetition(user.id!));
              setCounter({
                hour: 0,
                minute: 0,
                second: 0,
              });
              setCounterTime(null);
              navigation.navigate('Rooms');
            })
            .catch(err => console.log(err));
          return () => clearInterval(interval);
        }
      }, 1000);
      return () => clearInterval(interval);
    }
  }, [counterTime, counter, userPetition]);

  const left = useCallback(() => {
    navigation.navigate('Rooms');
  }, []);

  const handleConfirmDeletePetition = () => {
    disablePetition(userPetition.id!)(dispatch)
      .then(() => {
        dispatch(getUserPetition(user.id!));
        setCounter({
          hour: 0,
          minute: 0,
          second: 0,
        });
        setCounterTime(null);
        setModal({
          payload: {
            status: false,
            children: <></>,
          },
        })(dispatch);
        navigation.navigate('Rooms');
      })
      .catch(err => console.log(err));
  };

  const removePetition = useCallback(() => {
    setModal({
      payload: {
        status: true,
        children: (
          <Confirm
            message={`¿${customTraslate('borrarPetitionMensajeModal')}?`}
            leftText={customTraslate('cancelar')}
            rightText={customTraslate('borrar')}
            handleConfirm={handleConfirmDeletePetition}
          />
        ),
      },
    })(dispatch);
  }, []);
  return (
    <Container>
      <Header style={Styles.header}>
        <Left>
          <Button transparent onPress={left}>
            <Icon name="arrow-left" size={25} color="#989898" />
          </Button>
        </Left>
        <Body>
          <Title style={Styles.headerTitle}>{customTraslate('peticionActiva')}</Title>
        </Body>
        <Right style={Styles.crashContainer}>
          <Button transparent onPress={removePetition}>
            <Icon name="trash" size={25} color="#989898" />
          </Button>
        </Right>
      </Header>
      <Grid>
        <Row size={20}>
          <View style={Styles.requestDescription}>
            <Text style={Styles.requestDescriptionTitle}>{customTraslate('tuPeticion')}</Text>
            <Text style={Styles.requestDescriptionSubTitle}>{userPetition.description}</Text>
          </View>
        </Row>
        <Row size={1}></Row>
        <Row size={9} style={{ justifyContent: 'center' }}>
          <Text style={Styles.expirationText}>
            {counter.hour.toString().length > 1 ? counter.hour : `0${counter.hour}`}:
            {counter.minute}:{counter.second}
          </Text>
        </Row>
        <Row size={60}></Row>
      </Grid>
    </Container>
  );
}
