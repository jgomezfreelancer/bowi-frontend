import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  expirationText: { textAlign: 'center', color: '#b05e38', fontStyle: 'italic', fontSize: 17 },
  crashContainer: { paddingRight: 5 },
  header: {
    backgroundColor: 'white',
    elevation: 0,
    borderBottomColor: '#c8c8c8',
    borderBottomWidth: 1,
    paddingLeft: 0,
    paddingRight: 0,
  },
  headerTitle: { color: '#e35321' },
  requestDescription: { backgroundColor: '#fde9e0', padding: 20, width: '100%' },
  requestDescriptionTitle: {
    textAlign: 'center',
    color: '#220c01',
    fontWeight: 'bold',
    fontSize: 19,
  },
  requestDescriptionSubTitle: {
    textAlign: 'center',
    color: '#463b37',
    fontStyle: 'italic',
    marginTop: 5,
  },
  temporalMessageContainer: { width: '100%', paddingTop: 10 },
  temporalMessage: { textAlign: 'center', color: '#c7d1d0', fontStyle: 'italic' },
  rowButtonsContainer: {
    alignItems: 'flex-end',
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 15,
  },
  gridButtonsContainer: { borderColor: '#00E5F3', borderWidth: 2, borderRadius: 30 },
  decline: {
    textAlign: 'center',
    color: '#00E5F3',
    padding: 10,
    fontSize: 16,
    fontWeight: 'bold',
  },
  accept: {
    backgroundColor: '#00E5F3',
    textAlign: 'center',
    padding: 10,
    borderBottomRightRadius: 30,
    borderTopRightRadius: 30,
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
  },
});
