import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  gridContainer: {
    width: '100%',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  listItemsContainer: {
    flexDirection: 'column',
  },
  description: {
    color: '#949494',
    textAlign: 'right',
  },
  rankingContainer: { flexDirection: 'row', justifyContent: 'center' },
  userContainer: {},
  userIconContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 8,
  },
  rankingCounter: {
    color: '#949494',
    fontSize: 10,
    marginBottom: -2,
  },
  userListItemContainer: { justifyContent: 'center' },
  descriptionField: { justifyContent: 'flex-end' },
  imageProfile: {
    width: 60,
    height: 60,
    borderRadius: 50,
  }
});
