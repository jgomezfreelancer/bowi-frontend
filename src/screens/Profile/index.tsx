import { useNavigation } from '@react-navigation/native';
import { Container, Content, List, ListItem, Text, View } from 'native-base';
import React, { useEffect, useState } from 'react';
import { Image, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Stars from 'react-native-stars';
import CustomHeader from '../../components/common/Header';
import Item from './Item';

import Style from './style';
import { useDispatch, useSelector } from 'react-redux';
import { AppRootReducer } from '../../reducers';
import { getRankingByUser } from '../../reducers/ranking.reducer';
import { getAll as getAllCountries } from '../../reducers/country.reducer';
import { getAll as getAllLanguages } from '../../reducers/language.reducer';
import { toString } from 'lodash';
import { getUserImage } from '../../reducers/login.reducer';

export default function Profile(): JSX.Element {
  const [countryDetail, setCountryDetail] = useState<string>('');
  const [languageDetail, setLanguageDetail] = useState<string>('');
  const navigation = useNavigation();
  const {
    rankingReducer: { ranking },
    languageReducer: { list: languageList },
    countryReducer: { list: countryList },
    loginReducer: { user, userImage },
  } = useSelector((state: AppRootReducer) => state);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getRankingByUser(user.id!));
    dispatch(getAllCountries());
    dispatch(getAllLanguages());
    dispatch(getUserImage(user.id!));
  }, []);

  useEffect(() => {
    if (languageList.length > 0 && user.language_id! > 0) {
      const language = languageList.find(
        (element: { id: number }) => element.id === user.language_id,
      );
      if (language) {
        setLanguageDetail(language.description);
      }
    }
    if (countryList.length > 0 && user.country_id! > 0) {
      const country = countryList.find((element: { id: number }) => element.id === user.country_id);
      if (country) {
        setCountryDetail(country.description);
      }
    }
  }, [user, languageList, countryList]);

  const handleBack = () => {
    navigation.navigate('Ajustes');
  };
  const handleEdit = () => {
    navigation.navigate('EditProfile');
  };
  return (
    <Container>
      <CustomHeader
        leftColor="#949494"
        leftIcon="arrow-left"
        handleLeft={handleBack}
        handleRigth={handleEdit}
        rightIcon="edit"
        rightColor="#42c2cf"
        title="Mi Perfil"
      />
      <Content>
        <ScrollView>
          <List>
            <ListItem style={Style.userListItemContainer}>
              <View style={Style.userContainer}>
                <View style={Style.userIconContainer}>
                  {userImage !== '' ? (
                    <Image
                      source={{ uri: userImage }}
                      style={Style.imageProfile}
                    />
                  ) : (
                    <Icon size={50} color="#818181" name="user-circle" solid />
                  )}
                </View>

                <View style={Style.rankingContainer}>
                  <Stars
                    default={ranking}
                    count={5}
                    half={true}
                    starSize={50}
                    fullStar={<Icon size={12} color="#42c2cf" name="star" solid />}
                    emptyStar={<Icon size={12} color="#42c2cf" name="star" light />}
                    halfStar={<Icon size={12} color="#42c2cf" name="star-half-alt" solid />}
                  />
                  <Text style={Style.rankingCounter}> ({ranking})</Text>
                </View>
              </View>
            </ListItem>
            <Item name="Nombre" description={user.first_name!} />
            <Item name="Apellidos" description={user.last_name!} />
            <Item name="Edad" description={toString(user.age!)} />
            <Item name="Direccion" description={user.address!} />
            <Item name="C.P" description={user.cp!} />
            <Item name="Localidad" description={user.country_location!} />
            <Item name="Pais" description={countryDetail} />
            <Item name="Email" description={user.username!} />
            <Item name="Contraseña" description="*******" />
          </List>
        </ScrollView>
      </Content>
    </Container>
  );
}
