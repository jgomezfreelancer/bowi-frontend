import React from 'react';
import { Col, Grid, ListItem, Row, Text } from 'native-base';

import Style from './style';

interface IProps {
  name: string;
  description: string;
}

export default function Item(props: IProps): JSX.Element {
  const { name, description } = props;
  return (
    <ListItem style={Style.listItemsContainer} >
      <Grid style={Style.gridContainer} >
        <Col size={4} >
          <Text>{name}</Text>
        </Col>
        <Col size={8} >
        <Row style={Style.descriptionField}>
          <Text style={Style.description}>{description}</Text>
        </Row>
        </Col>
      </Grid>
    </ListItem>
  );
}
