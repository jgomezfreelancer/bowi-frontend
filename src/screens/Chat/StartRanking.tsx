import React, { useState } from 'react';

import Icon from 'react-native-vector-icons/FontAwesome5';
import { BLUE } from '../../styles/colors';


interface Iprops {
    onChange: (ranking: number) => void
}

export default function StartRanking(props: Iprops): JSX.Element {
  const { onChange } = props;
  const [rating, setRating] = useState<number | null>(null);
  const changeRating = (value: number) => {
    setRating(value);
    onChange(value)
  }
  return (
    <>
      {[...Array(5)].map((start: number, i: number) => {
        const ratingValue = i + 1;
        return (
          <Icon
            key={i}
            size={30}
            color={`${ratingValue <= rating! ? BLUE  : '#bdc3c7'}`}
            name="star"
            solid={ratingValue <= rating! || false}
            light={ratingValue > rating! || false}
            onPress={() => changeRating(ratingValue)}
          />
        );
      })}
    </>
  );
}
