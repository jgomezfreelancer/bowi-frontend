import { StyleSheet, Dimensions } from 'react-native';
import { moderateScale } from 'react-native-size-matters';
import { BLUE, GREY } from '../../styles/colors';

const { width } = Dimensions.get('window');

export default StyleSheet.create({
  commentsContainer: { flex: 1, paddingTop: 10, position: 'relative' },
  clipIconContainer: {
    borderRadius: 50,
    paddingTop: 8,
    paddingBottom: 5,
    paddingLeft: 8,
    paddingRight: 15,
    textAlign: 'center',
  },
  imageBackgroundContainer: {
    flex: 1,
    justifyContent: 'center',
    position: 'relative',
  },
  scrollViewContainer: { paddingTop: 1, position: 'relative' },
  senderContainer: { position: 'relative', paddingLeft: 5, margin: 10 },
  senderContent: {
    position: 'relative',
    fontWeight: 'bold',
    color: '#454545',
    borderRadius: 30,
    borderBottomLeftRadius: 0,
    backgroundColor: '#f3f3f3',
    width: '65%',
    padding: 15,
  },
  recieverContainer: {
    paddingRight: 5,
    margin: 10,
    justifyContent: 'flex-end',
    flexDirection: 'row',
    position: 'relative',
  },
  recieverContent: {
    position: 'relative',
    fontWeight: 'bold',
    color: 'white',
    borderRadius: 30,
    borderBottomRightRadius: 0,
    backgroundColor: BLUE,
    width: '75%',
    padding: 15,
    zIndex: 5,
  },
  footerContainer: {
    width: '100%',
    padding: 0,
    backgroundColor: 'white',
    marginBottom: 10,
    elevation: 0,
  },
  footerTabContainer: { backgroundColor: 'white', paddingLeft: 10, paddingRight: 10 },
  textCommentContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 15,
    paddingRight: 8,
    borderWidth: 1,
    borderColor: '#dddddd',
    borderRadius: 30,
  },
  textComment: {
    borderWidth: 2,
    backgroundColor: 'white',
    color: 'black',
    borderColor: 'white',
    width: width - width / 2.8,
  },
  sendIconContainer: { alignSelf: 'center', flexDirection: 'row' },
  iconContainer: {
    backgroundColor: BLUE,
    borderRadius: 50,
    paddingTop: 8,
    paddingBottom: 5,
    paddingLeft: 8,
    paddingRight: 9,
    textAlign: 'center',
  },
  conversationEndContainer: {
    width: '100%',
    backgroundColor: '#287e86',
    color: 'white',
    textAlign: 'center',
    paddingTop: 15,
    paddingBottom: 15,
    marginBottom: 60,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    position: 'absolute',
    bottom: 20,
    fontSize: 16,
  },
  rankingMessageContainer: {
    width: '100%',
    backgroundColor: 'white',
    textAlign: 'center',
    paddingTop: 15,
    paddingBottom: 15,
    marginBottom: 60,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    position: 'absolute',
    bottom: -40,
    fontWeight: 'bold',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  rankingMessage: {
    color: '#42c2cf',
    marginLeft: 10,
    fontWeight: 'bold',
    fontSize: 16,
  },
  centeredRankingView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    alignSelf: 'center',
    bottom: 10,
    paddingLeft: 0,
  },
  modalView: {
    backgroundColor: 'white',
    padding: 10,
    paddingBottom: 0,
    alignItems: 'center',
    borderWidth: 2,
    borderTopColor: '#f1f1f1',
    borderBottomColor: 'white',
  },
  closeIcon: {
    textAlign: 'right',
    color: '#818181',
    paddingRight: 7,
    fontSize: 16,
  },
  content: {
    backgroundColor: 'white',
    paddingTop: 20,
    flexWrap: 'nowrap',
    width,
  },
  rankingContainer: { flexDirection: 'row', justifyContent: 'center', marginTop: 15 },
  rowButtonsContainer: {},
  gridButtonsContainer: { borderColor: BLUE, borderWidth: 3, borderRadius: 30 },
  regularButtonLeft: {
    textAlign: 'center',
    color: BLUE,
    backgroundColor: 'white',
    borderColor: BLUE,
    padding: 10,
    fontSize: 16,
    borderBottomLeftRadius: 30,
    borderTopLeftRadius: 30,
    borderWidth: 2,
  },
  regularButtonRight: {
    color: 'white',
    backgroundColor: BLUE,
    borderColor: BLUE,
    textAlign: 'center',
    padding: 10,
    fontSize: 16,
    borderBottomRightRadius: 30,
    borderTopRightRadius: 30,
    borderWidth: 2,
  },
  buttonContainer: {
    position: 'relative',
    alignSelf: 'center',
    marginTop: 20,
  },
  cancelButton: {
    borderRadius: 15,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: 'white',
    color: '#b4b4b4',
    fontWeight: 'bold',
    overflow: 'hidden',
    textAlign: 'center',
    paddingLeft: 25,
    paddingRight: 25,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  rankingButton: {
    borderRadius: 15,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: '#37c1cf',
    color: 'white',
    fontWeight: 'bold',
    overflow: 'hidden',
    textAlign: 'center',
    paddingLeft: 25,
    paddingRight: 25,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  messageContainer: { justifyContent: 'center' },
  textMessage: { textAlign: 'center', color: '#2c3e50' },
  textMessage1: { textAlign: 'center', color: '#2c3e50', fontWeight: 'bold' },
  buttonsContainer: { justifyContent: 'space-around', flexDirection: 'row' },
  tailLeftBubble: {
    left: moderateScale(1, 0.5),
    position: 'absolute',
    zIndex: 2,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 2,
    shadowRadius: 0,
    bottom: -1,
  },
  tailRightBubble: {
    right: moderateScale(1, 0.5),
    position: 'absolute',
    zIndex: 2,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 2,
    shadowRadius: 0,
    bottom: -1,
    transform: [{ rotateX: '180deg' }, { rotateZ: '180deg' }],
  },
  requestDescription: { backgroundColor: '#e2f2f4', padding: 20 },
  requestDescriptionTitle: {
    textAlign: 'center',
    color: '#287d84',
    fontWeight: 'bold',
    fontSize: 19,
  },
  requestDescriptionSubTitle: {
    textAlign: 'center',
    color: '#287d84',
    fontStyle: 'italic',
    marginTop: 5,
  },
  temporalMessageContainer: { width: '100%', paddingTop: 10, paddingBottom: 10 },
  temporalMessage: { textAlign: 'center', color: '#c7d1d0', fontStyle: 'italic' },
  viewButtonsContainer: { padding: 20 },
  disableChatContainer: { justifyContent: 'center', flexDirection: 'row', flex: 1 },
  disableChatText: { textAlign: 'center', color: '#95a5a6', fontSize: 16, fontStyle: 'italic' },
  senderMessageImage: {
    width: 200,
    height: 200,
    borderColor: '#f3f3f3',
    borderWidth: 10,
    borderRadius: 30,
    borderBottomLeftRadius: 0,
  },
  recieverMessageImage: {
    width: 200,
    height: 200,
    borderColor: BLUE,
    borderWidth: 10,
    borderRadius: 30,
    borderBottomRightRadius: 0,
  },
  buttonImage: { width: 200, height: 200 },
});
