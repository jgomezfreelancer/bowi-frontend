import { Container, Footer, FooterTab, Button } from 'native-base';
import React, { useState, useEffect, useRef } from 'react';
import { View, Text, TextInput, ScrollView, Platform, PermissionsAndroid } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import socketIOClient from 'socket.io-client';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {
  ImageLibraryOptions,
  ImagePickerResponse,
  launchImageLibrary,
} from 'react-native-image-picker';

import { AppRootReducer } from '../../reducers';
import Styles from './style';
import CustomHeader from '../../components/common/Header';
import { RouteProp, useNavigation, useRoute } from '@react-navigation/native';
import { getRoomMessages, setRoomMessages, IMessage } from '../../reducers/message.reducer';
import { ENV } from '../../../env';
import { getUsersByLocation, IUser } from '../../reducers/login.reducer';
import Comment from './Comment';
import {
  getUserRooms,
  IUserRooms,
  disableChat as setDisableRoom,
  checkExpiration,
} from '../../reducers/room.reducer';
import { create as storeRanking } from '../../reducers/ranking.reducer';
import RankingModal from './RankingModal';
import setToast from '../../actions/toast.action';
import { setNotificationIcon } from '../../reducers/notification.reducer';
import Message from '../../helpers/message';
import { BLUE, GREY } from '../../styles/colors';
import { customTraslate } from '../../utils/language';

const ENDPOINT = ENV.websocket.chat.room;

let chatRoomSocket: any;

type RouteParamList = {
  chat: {
    room: IUserRooms;
    sender: IUser;
  };
};

const Chat = (): JSX.Element => {
  const [rankinMessage, setRankingMessage] = useState<boolean>(false);
  const [disableChat, setDisableChat] = useState<boolean>(false);
  const [message, setMessage] = useState<string>('');
  const [statusRankingModal, setStatusRankingModal] = useState<boolean>(false);
  const {
    loginReducer: {
      user: { username, id: userId, limit_distance },
    },
    messageReducer: { messages: chat },
    roomReducer: { roomExpiration },
    locationReducer: { meters },
  } = useSelector((state: AppRootReducer) => state);
  const {
    params: { room, sender },
  } = useRoute<RouteProp<RouteParamList, 'chat'>>();
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const scrollViewRef = useRef(null);

  useEffect(() => {
    setDisableChat(!!room.disable);
  }, [room.disable]);

  const init = () => {
    chatRoomSocket = socketIOClient(ENDPOINT);
    chatRoomSocket.on('connect', () => {
      chatRoomSocket.emit('joinRoom', { room: room.description, username });
    });
    chatRoomSocket.on('chatToClient', (data: IMessage[]) => {
      dispatch(setRoomMessages(data));
    });

    chatRoomSocket.on('clientDisableRoom', (data: { status: boolean }) => {
      if (room.petition) {
        if (room.petition.user_id !== userId) {
          getRoomMessages(
            room.id,
            sender.id!,
          )(dispatch)
            .then(() => {
              setToast({
                payload: {
                  status: true,
                  type: 'success',
                  message: `${customTraslate('chatDeshabilitadoPor')}  ${sender.first_name} ${sender.last_name ? sender.last_name.charAt(0) : ''
                    }`,
                },
              })(dispatch);
              setDisableChat(true);
              dispatch(getUsersByLocation(userId!, meters === 0 ? limit_distance! : meters));
            })
            .catch(error => {
              const errorMessage = Message.exception(error);
              setToast({
                payload: {
                  message: errorMessage,
                  status: true,
                  type: 'error',
                },
              })(dispatch);
            });
        }
      } else {
        if (data.status && room.participant_id === userId) {
          getRoomMessages(
            room.id,
            sender.id!,
          )(dispatch)
            .then(() => {
              dispatch(getUsersByLocation(userId!, meters === 0 ? limit_distance! : meters));
              setToast({
                payload: {
                  status: true,
                  type: 'success',
                  message: `${customTraslate('chatDeshabilitadoPor')} ${sender.first_name} ${sender.last_name ? sender.last_name.charAt(0) : ''
                    }`,
                },
              })(dispatch);
              setDisableChat(true);
            })
            .catch(error => {
              const errorMessage = Message.exception(error);
              setToast({
                payload: {
                  message: errorMessage,
                  status: true,
                  type: 'error',
                },
              })(dispatch);
            });
        }
      }
    });

    chatRoomSocket.on('clientForceRoom', (data: { status: boolean }) => {
      getUserRooms(userId!)(dispatch)
        .then(() => {
          setToast({
            payload: {
              status: true,
              message: `${customTraslate('chatExpirado')} `,
            },
          })(dispatch);
          navigation.navigate('Rooms');
        })
        .catch(error => {
          const errorMessage = Message.exception(error);
          setToast({
            payload: {
              message: errorMessage,
              status: true,
              type: 'error',
            },
          })(dispatch);
        });
    });
  };

  useEffect(() => {
    dispatch(getRoomMessages(room.id, sender.id!));
    dispatch(getUserRooms(userId!));
  }, [dispatch]);

  useEffect(() => {
    init();
    dispatch(checkExpiration(room.id));
    dispatch(setNotificationIcon(false));
    return () => {
      dispatch(getUserRooms(userId!));
      dispatch(setRoomMessages([]));
      dispatch(setNotificationIcon(false));
      chatRoomSocket.emit('leaveRoom', { room: room.description, username });
      chatRoomSocket.disconnect();
    };
  }, []);

  const send = async () => {
    if (message !== '' && !disableChat) {
      const payload = {
        message,
        userId,
        roomDescription: room.description,
        roomId: room.id,
        username,
        isMultimedia: false,
      };
      await chatRoomSocket.emit('chatToServer', payload);
      setMessage('');
    }
  };

  const handleBack = async () => {
    dispatch(getUserRooms(userId!));
    navigation.navigate('Rooms');
  };

  const setComment = (text: string) => setMessage(text);

  const handleRankingModal = () => {
    setStatusRankingModal(true);
  };

  const removeChat = (ranking: number | null, isRanking: boolean) => {
    setRankingMessage(true);
    setDisableRoom(room.id)(dispatch)
      .then(() => {
        setStatusRankingModal(false);
        setDisableChat(true);
        dispatch(getUsersByLocation(userId!, meters === 0 ? limit_distance! : meters));
        setTimeout(() => {
          setRankingMessage(false);
        }, 3000);
        if (isRanking && ranking! > 0) {
          dispatch(storeRanking({ user_id: sender.id!, type: ranking! }));
        }
        dispatch(getUserRooms(userId!));
      })
      .catch(error => {
        const errorMessage = Message.exception(error);
        setToast({
          payload: {
            message: errorMessage,
            status: true,
            type: 'error',
          },
        })(dispatch);
      });
  };

  const handleRankingStatus = (status: boolean) => {
    setStatusRankingModal(status);
  };

  const handleImage = () => {
    const options = {
      mediaType: 'photo',
      includeBase64: true,
      maxHeight: 512,
      maxWidth: 512,
    };
    launchImageLibrary(options, async (response: { didCancel: any; errorCode: any; type: any; base64: any; }) => {
      if (response.didCancel) {
      } else if (response.errorCode) {
        if (Platform.OS === 'android') {
          await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE)
            .then(() => {
              handleImage();
            })
            .catch(error => console.log(error));
        }
      } else {
        const payload = {
          message: `data:${response.type};base64,${response.base64}`,
          userId,
          roomDescription: room.description,
          roomId: room.id,
          username,
          isMultimedia: true,
        };
        await chatRoomSocket.emit('chatToServer', payload);
        setMessage('');
      }
    });
  };

  useEffect(() => {
    const interval = setInterval(() => {
      dispatch(checkExpiration(room.id));
    }, 10000);
    return () => clearInterval(interval);
  }, []);

  const checkEndButton = room.petition
    ? room.petition.user_id === userId && !disableChat
    : room.responsible_id === userId && !disableChat;
  const showMessage = room.petition
    ? room.petition.user_id === userId
    : room.responsible_id === userId;
  return (
    <Container>
      <CustomHeader
        leftColor="#818181"
        leftIcon="arrow-left"
        handleLeft={handleBack}
        title={room.petition ? '' : sender.first_name!}
        rightTitle={checkEndButton ? customTraslate('finalizar') : ''}
        rightColor="#287d84"
        handleRigth={checkEndButton ? handleRankingModal : null}
      />
      {room.petition && (
        <View style={Styles.requestDescription}>
          <Text style={Styles.requestDescriptionTitle}>{room.userPetition.first_name}</Text>
          <Text style={Styles.requestDescriptionSubTitle}>
            {room.petition && room.petition.description}
          </Text>
        </View>
      )}
      <View style={Styles.temporalMessageContainer}>
        <Text style={Styles.temporalMessage}>
          {customTraslate('mensajeSeBorrara24Horas')} {'\n'}
          {customTraslate('tiempoRestante')}: {roomExpiration > 0 ? roomExpiration : '24'}h
        </Text>
      </View>
      <ScrollView
        style={Styles.scrollViewContainer}
        ref={scrollViewRef}
        onContentSizeChange={() => {
          scrollViewRef.current !== null
            ? scrollViewRef.current.scrollToEnd({ animated: true })
            : null;
        }}
      >
        <View style={Styles.commentsContainer}>
          {chat.map((element: IMessage, i: number) => (
            <Comment
              type={element.user.username === username ? 'reciever' : 'sender'}
              key={i}
              name={element.user!.first_name!}
              message={element.description}
              isMultimedia={element.isMultimedia}
            />
          ))}
        </View>
      </ScrollView>
      {showMessage && rankinMessage && (
        <View style={Styles.rankingMessageContainer}>
          <Icon color="#42c2cf" size={20} name="grin-alt" solid />
          <Text style={Styles.rankingMessage}> {customTraslate('graciasPorSuValoracion')}</Text>
        </View>
      )}
      <RankingModal
        status={statusRankingModal}
        handleRankingStatus={handleRankingStatus}
        sender={room.petition ? room.responsible : sender}
        handle={removeChat}
      />
      <Footer style={Styles.footerContainer}>
        <FooterTab style={Styles.footerTabContainer}>
          {disableChat ? (
            <View style={Styles.disableChatContainer}>
              <Text style={Styles.disableChatText}>
                {customTraslate('elChatEstaDeshabilitado')}
              </Text>
            </View>
          ) : (
            <View style={Styles.textCommentContainer}>
              <TextInput
                style={Styles.textComment}
                value={message}
                placeholder={customTraslate('escribeTuMensaje')}
                onChangeText={setComment}
              />
              <View style={Styles.sendIconContainer}>
                <Text style={Styles.clipIconContainer} onPress={handleImage}>
                  <Icon color={GREY} size={21} name="paperclip" solid />
                </Text>
                <Text style={Styles.iconContainer} onPress={send}>
                  <Icon color="white" size={21} name="paper-plane" solid />
                </Text>
              </View>
            </View>
          )}
        </FooterTab>
      </Footer>
    </Container>
  );
};

export default Chat;
