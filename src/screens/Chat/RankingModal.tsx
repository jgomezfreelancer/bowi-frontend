import { Col, Row } from 'native-base';
import React, { useState } from 'react';
import { Modal, View, Text } from 'react-native';

import { IUser } from '../../reducers/login.reducer';
import { customTraslate } from '../../utils/language';
import StartRanking from './StartRanking';
import Styles from './style';

interface Iprops {
  status: boolean;
  handle: (ranking: number | null, isRanking: boolean) => void;
  sender: IUser;
  handleRankingStatus: (valueStatus: boolean) => void;
}

function RankingModal(props: Iprops): JSX.Element {
  const [currentRanking, setCurrentRanking] = useState<number | null>(null);
  const { status, handle, sender, handleRankingStatus } = props;

  const onChangeRanking = (ranking: number) => {
    setCurrentRanking(ranking);
  };

  const handleCancelRanking = () => {
    handleRankingStatus(false);
  };

  const applyRanking = () => {
    return handle(currentRanking, true);
  };

  return (
    <View style={Styles.centeredRankingView}>
      <Modal animationType="slide" transparent={true} visible={status}>
        <View style={Styles.centeredRankingView}>
          <View style={Styles.modalView}>
            <View style={Styles.content}>
              <View style={Styles.messageContainer}>
                <Text style={Styles.textMessage}>
                  {customTraslate('laConversacionCon')} {sender.first_name}{' '}
                  {customTraslate('haFinalizado')}.
                </Text>
                <Text style={Styles.textMessage1}>
                  ¿ {customTraslate('satisfactoriaAyudaDe')} {sender.first_name} ?
                </Text>
              </View>
              <View style={Styles.rankingContainer}>
                <StartRanking onChange={onChangeRanking} />
              </View>
              <View style={Styles.viewButtonsContainer}>
                <Row style={Styles.rowButtonsContainer}>
                  <Col>
                    <Text style={Styles.regularButtonLeft} onPress={handleCancelRanking}>
                      {customTraslate('cancelar').toUpperCase()}
                    </Text>
                  </Col>
                  <Col>
                    <Text style={Styles.regularButtonRight} onPress={applyRanking}>
                      {customTraslate('valorar').toUpperCase()}
                    </Text>
                  </Col>
                </Row>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
}

export default RankingModal;
