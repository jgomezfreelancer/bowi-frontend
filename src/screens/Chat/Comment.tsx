import React, { useState } from 'react';
import { Button, Text, View } from 'native-base';
import { moderateScale } from 'react-native-size-matters';
import { Svg, Path } from 'react-native-svg';
import ImagePreview from 'react-native-image-preview';

import Style from './style';
import { BLUE } from '../../styles/colors';
import { Image } from 'react-native';

interface Iprops {
  name: string;
  message: string;
  type: 'reciever' | 'sender';
  isMultimedia: boolean;
}

function Comment(props: Iprops): JSX.Element {
  const [visible, setVisible] = useState<boolean>(false);
  const { message, type, isMultimedia } = props;
  const openImage = () => setVisible(true);

  return (
    <View style={type === 'reciever' ? Style.recieverContainer : Style.senderContainer}>
      <Svg
        style={type === 'reciever' ? Style.tailRightBubble : Style.tailLeftBubble}
        width={moderateScale(12.5, 2)}
        height={moderateScale(13.5, 2)}
        viewBox="32.484 17.5 15.515 17.5"
        enable-background="new 32.485 17.5 15.515 17.5"
      >
        <Path
          d="M38.484,17.5c0,8.75,1,13.5-6,17.5C51.484,35,52.484,17.5,38.484,17.5z"
          fill={type === 'reciever' ? BLUE : '#f3f3f3'}
          x="0"
          y="0"
        />
      </Svg>
      {isMultimedia ? (
        <React.Fragment>
          <ImagePreview
            visible={visible}
            source={{ uri: message }}
            close={() => setVisible(false)}
            style={type === 'reciever' ? Style.recieverMessageImage : Style.senderMessageImage}
          />
          <Button transparent style={Style.buttonImage} onPress={openImage}>
            <Image
              source={{ uri: message }}
              style={type === 'reciever' ? Style.recieverMessageImage : Style.senderMessageImage}
            />
          </Button>
        </React.Fragment>
      ) : (
        <Text style={type === 'reciever' ? Style.recieverContent : Style.senderContent}>
          {message}
        </Text>
      )}
    </View>
  );
}

export default Comment;
