import { useNavigation } from '@react-navigation/native';
import { Container } from 'native-base';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import CustomHeader from '../../components/common/Header';
import MultipleSelect from '../../components/common/MultipleSelect';
import MultipleInterestSelect from '../../components/common/MultipleInterestSelect';
import { AppRootReducer } from '../../reducers';
import { updateUserInterest } from '../../reducers/login.reducer';
import { getAll } from '../../reducers/interest.reducer';
import { customTraslate } from '../../utils/language';

export default function Interest(): JSX.Element {
  /**
   * mock
   */
   const [listMock] = useState<any[]>([
    {id:1,description:'Ocio',language:'ocio'},
    {id:2,description:'Conversación',language:'conversacion'},
    {id:3,description:'Laboral y educativo',language:'laboralYEducativo'},
    {id:4,description:'Animales',language:'animales'},
    {id:5,description:'Comunitaria',language:'comunitaria'},
    {id:6,description:'Covid-19',language:'covid19'},
    {id:7,description:'Diversidad funcional',language:'diversidadFuncional'},
    {id:8,description:'Desplazamiento',language:'desplazamiento'},
    {id:9,description:'Apoyo emocional',language:'apoyoEmocional'},
    {id:10,description:'Legal y administrativo',language:'legalYadministrativo'},
    {id:11,description:'Alojamiento',language:'alojamiento'},
    {id:12,description:'Familiar',language:'familiar'},
    {id:13,description:'Sanitario',language:'sanitario'},
    {id:14,description:'Pérdidas',language:'perdidas'},
    {id:15,description:'Otros',language:'otros'}]);
  const [selection, setSelection] = useState<number[]>([]);
  const {
    loginReducer: {
      user: { userInterestKeys: selectedList, id },
    },
    interestReducer: { list },
  } = useSelector((state: AppRootReducer) => state);
  const dispatch = useDispatch();
  const navigation = useNavigation();

  useEffect(() => {
    dispatch(getAll());
  }, [dispatch]);

  const handleBack = async () => {
    if (selection.length > 0) {
      await dispatch(
        updateUserInterest({
          interests: selection,
          userId: id!,
        }),
      );
    }
    navigation.navigate('Ajustes');
  };

  const onChange = (interestList: number[]) => {
    setSelection(interestList);
  };

  const getLabel = (object: { language: string }) => {
    return customTraslate(object.language);
  };

  return (
    <Container>
      <CustomHeader
        leftIcon="arrow-left"
        leftColor="#989898"
        handleLeft={handleBack}
        title={customTraslate('intereses')}
        borderBottom={false}
      />
      <MultipleInterestSelect
        onChange={onChange}
        list={listMock}
        selectedList={selectedList ? selectedList : []}
        icon
        getLabel={getLabel}
      />
    </Container>
  );
}
