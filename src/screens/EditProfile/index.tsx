import { useNavigation } from '@react-navigation/native';
import { Col, Container, Content, Grid, List, ListItem, Row, Text, View } from 'native-base';
// import { Picker as SelectPicker } from '@react-native-community/picker';
import { Picker as SelectPicker } from '@react-native-picker/picker';
import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import {
  ScrollView,
  Image,
  PermissionsAndroid,
  Platform,
  NativeSyntheticEvent,
  TextInputFocusEventData,
  TouchableHighlight,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { useDispatch, useSelector } from 'react-redux';
import isEmpty from 'lodash/isEmpty';
import Stars from 'react-native-stars';
// import { ImageLibraryOptions, ImagePickerResponse, launchImageLibrary } from 'react-native-image-picker';
import { launchImageLibrary } from 'react-native-image-picker';
import Input from '../../components/common/form/Input';
import Select from '../../components/common/form/Select';
import CustomHeader from '../../components/common/Header';
import { AppRootReducer } from '../../reducers';
import { getAll as getAllCountries, ICountry } from '../../reducers/country.reducer';
import { getUserImage, updateProfile } from '../../reducers/login.reducer';
import EditIcon from '../../assets/icons/icono-gris-06.svg';

import Style from './style';
import { BLUE } from '../../styles/colors';
import { customTraslate } from '../../utils/language';

interface AgeOptions {
  value: number | null;
  description: string;
}

const ageOptions: AgeOptions[] = [
  { value: 18, description: '18' },
  { value: 19, description: '19' },
  { value: 20, description: '20' },
  { value: 21, description: '21' },
  { value: 22, description: '22' },
  { value: 23, description: '23' },
  { value: 24, description: '24' },
  { value: 25, description: '25' },
  { value: 26, description: '26' },
  { value: 27, description: '27' },
  { value: 28, description: '28' },
  { value: 29, description: '29' },
  { value: 30, description: '30' },
  { value: 31, description: '31' },
  { value: 32, description: '32' },
  { value: 33, description: '33' },
  { value: 34, description: '34' },
  { value: 35, description: '35' },
  { value: 36, description: '36' },
  { value: 37, description: '37' },
  { value: 38, description: '38' },
  { value: 39, description: '39' },
  { value: 40, description: '40' },
  { value: 41, description: '41' },
  { value: 42, description: '42' },
  { value: 43, description: '43' },
  { value: 44, description: '44' },
  { value: 45, description: '45' },
  { value: 46, description: '46' },
  { value: 47, description: '47' },
  { value: 48, description: '48' },
  { value: 49, description: '49' },
  { value: 50, description: '50' },
  { value: 51, description: '51' },
  { value: 52, description: '52' },
  { value: 53, description: '53' },
  { value: 54, description: '54' },
  { value: 55, description: '55' },
  { value: 56, description: '56' },
  { value: 57, description: '57' },
  { value: 58, description: '58' },
  { value: 59, description: '59' },
  { value: 60, description: '60' },
  { value: 61, description: '61' },
  { value: 62, description: '62' },
  { value: 63, description: '63' },
  { value: 64, description: '64' },
  { value: 65, description: '65' },
  { value: 66, description: '66' },
  { value: 67, description: '67' },
  { value: 68, description: '68' },
  { value: 69, description: '69' },
  { value: 70, description: '70' },
  { value: 71, description: '71' },
  { value: 72, description: '72' },
  { value: 73, description: '73' },
  { value: 74, description: '74' },
  { value: 75, description: '75' },
  { value: 76, description: '76' },
  { value: 77, description: '77' },
  { value: 78, description: '78' },
  { value: 79, description: '79' },
  { value: 80, description: '80' },
  { value: 81, description: '81' },
  { value: 82, description: '82' },
  { value: 83, description: '83' },
  { value: 84, description: '84' },
  { value: 85, description: '85' },
  { value: 86, description: '86' },
  { value: 87, description: '87' },
  { value: 88, description: '88' },
  { value: 89, description: '89' },
  { value: 90, description: '90' },
];

type FormData = {
  first_name: string;
  last_name: string;
  age: number;
  address: string;
  cp: string;
  country_id: number;
  username: string;
  password: string;
  country_location: string;
};

interface IFile {
  data: string;
  type: string;
  name: string;
}

export default function EditProfile(): JSX.Element {
  const [currentField, setCurrentField] = useState<string>('');
  const [file, setFile] = useState<IFile | null>(null);
  const [photo, setPhoto] = useState({ uri: '' });
  const { control, errors, setValue, getValues } = useForm<FormData>();
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const {
    loginReducer: { user, loading, userImage },
    countryReducer: { list: countryList },
  } = useSelector((state: AppRootReducer) => state);

  useEffect(() => {
    dispatch(getAllCountries());
    if (!isEmpty(user)) {
      const {
        username,
        first_name,
        last_name,
        age,
        address,
        cp,
        country_id,
        country_location,
      } = user;
      setValue('username', username);
      setValue('first_name', first_name);
      setValue('last_name', last_name);
      setValue('age', age);
      setValue('address', address);
      setValue('cp', cp);
      setValue('country_id', country_id);
      setValue('country_location', country_location);
      dispatch(getUserImage(user.id!));
    }
  }, [dispatch, setValue, user]);

  const onSubmit = () => {
    const form = getValues();
    const body = {
      ...form,
      file,
    };
    dispatch(updateProfile(user.id!, body));
  };

  useEffect(() => {
    if (userImage !== '') {
      const source = { uri: userImage };
      setPhoto(source);
    }
  }, [userImage]);

  const handleBack = () => {
    dispatch(getUserImage(user.id!));
    navigation.navigate('Ajustes');
  };

  const upload = () => {
    const options = {
      mediaType: 'photo',
      includeBase64: true,
      maxHeight: 200,
      maxWidth: 200,
    };
    launchImageLibrary(options, async (response: { didCancel: any; errorCode: any; type: any; base64: any; fileName: any; }) => {
      if (response.didCancel) {
        if (userImage === '') {
          setPhoto({ uri: '' });
        }
      } else if (response.errorCode) {
        if (Platform.OS === 'android') {
          await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE)
            .then(() => {
              upload();
            })
            .catch(error => console.log(error));
        }
      } else {
        const source = { uri: `data:${response.type};base64,${response.base64}` };
        setPhoto(source);
        const data = {
          data: response.base64,
          type: response.type,
          name: response.fileName,
        };
        setFile(data);
      }
    });
  };

  const handleInputBlur = (event: NativeSyntheticEvent<TextInputFocusEventData> | undefined) => {
    setCurrentField('');
  };

  const handleCurrentField = (field: string) => {
    setCurrentField(field);
  };

  return (
    <Container>
      <CustomHeader
        leftColor="#949494"
        leftIcon="arrow-left"
        handleLeft={handleBack}
        handleRigth={onSubmit}
        rightColor={BLUE}
        rightTitle={loading ? `${customTraslate('espere')}...` : customTraslate('guardar')}
        borderBottom={true}
        isInput={false}
      />
      <Content>
        <ScrollView>
          <List>
            <ListItem style={Style.listItemPictureContainer} onPress={upload}>
              <View style={Style.rankingContainer}>
                {photo.uri !== '' ? (
                  <Image source={photo} style={Style.imageProfile} />
                ) : (
                  <Icon style={Style.iconProfile} size={40} color={BLUE} name="user-plus" solid />
                )}
                <Stars
                  disabled
                  default={user.ranking ? parseFloat(user.ranking) : 0}
                  count={5}
                  half={true}
                  starSize={50}
                  fullStar={<Icon size={12} color={BLUE} name="star" solid />}
                  emptyStar={<Icon size={12} color={BLUE} name="star" light />}
                  halfStar={<Icon size={12} color={BLUE} name="star-half-alt" solid />}
                />
              </View>
            </ListItem>

            <ListItem style={Style.listItemsContainer}>
              <Grid style={Style.gridContainer}>
                <Col size={8}>
                  <View>
                    <Text style={Style.fieldTitle}>{customTraslate('nombre')}</Text>
                  </View>
                  <Input
                    control={control}
                    name="first_name"
                    align="left"
                    required
                    error={errors.first_name ? errors.first_name.message : null}
                    placeholder={`${customTraslate('nombre')} ...`}
                    onInputBlur={handleInputBlur}
                    editable={currentField === 'first_name'}
                  />
                </Col>
                <Col size={2}>
                  <Row style={Style.IconField}>
                    {currentField !== 'first_name' && (
                      <TouchableHighlight onPress={() => handleCurrentField('first_name')}>
                        <EditIcon width={40} height={40} />
                      </TouchableHighlight>
                    )}
                  </Row>
                </Col>
              </Grid>
            </ListItem>

            <ListItem style={Style.listItemsContainer}>
              <Grid style={Style.gridContainer}>
                <Col size={8}>
                  <View>
                    <Text style={Style.fieldTitle}>{customTraslate('apellidos')}</Text>
                  </View>
                  <Input
                    control={control}
                    name="last_name"
                    align="left"
                    required
                    error={errors.last_name ? errors.last_name.message : null}
                    placeholder={`${customTraslate('apellidos')} ...`}
                    onInputBlur={handleInputBlur}
                    editable={currentField === 'last_name'}
                  />
                </Col>
                <Col size={2}>
                  <Row style={Style.IconField}>
                    {currentField !== 'last_name' && (
                      <TouchableHighlight onPress={() => handleCurrentField('last_name')}>
                        <EditIcon width={40} height={40} />
                      </TouchableHighlight>
                    )}
                  </Row>
                </Col>
              </Grid>
            </ListItem>

            <ListItem style={Style.listItemsContainer}>
              <Grid style={Style.gridContainer}>
                <Col size={8}>
                  <View>
                    <Text style={Style.fieldTitle}>{customTraslate('edad')}</Text>
                  </View>
                  <Select
                    control={control}
                    name="age"
                    required
                    error={errors.age ? errors.age.message : null}
                    enabled={currentField === 'age'}
                  >
                    {ageOptions.map((age: AgeOptions) => (
                      <SelectPicker.Item key={age.value} value={age.value!} label={age.description} />
                    ))}
                  </Select>
                </Col>
                <Col size={2}>
                  <Row style={Style.IconField}>
                    {currentField === 'age' ? (
                      <TouchableHighlight onPress={() => handleCurrentField('')}>
                        <Icon size={30} color="#989898" name="times-circle" />
                      </TouchableHighlight>
                    ) : (
                      <TouchableHighlight onPress={() => handleCurrentField('age')}>
                        <EditIcon width={40} height={40} />
                      </TouchableHighlight>
                    )}
                  </Row>
                </Col>
              </Grid>
            </ListItem>

            <ListItem style={Style.listItemsContainer}>
              <Grid style={Style.gridContainer}>
                <Col size={8}>
                  <View>
                    <Text style={Style.fieldTitle}>{customTraslate('direccion')}</Text>
                  </View>
                  <Input
                    control={control}
                    name="address"
                    align="left"
                    required
                    error={errors.address ? errors.address.message : null}
                    placeholder={`${customTraslate('direccion')} ...`}
                    onInputBlur={handleInputBlur}
                    editable={currentField === 'address'}
                  />
                </Col>
                <Col size={2}>
                  <Row style={Style.IconField}>
                    {currentField !== 'address' && (
                      <TouchableHighlight onPress={() => handleCurrentField('address')}>
                        <EditIcon width={40} height={40} />
                      </TouchableHighlight>
                    )}
                  </Row>
                </Col>
              </Grid>
            </ListItem>

            <ListItem style={Style.listItemsContainer}>
              <Grid style={Style.gridContainer}>
                <Col size={8}>
                  <View>
                    <Text style={Style.fieldTitle}>{customTraslate('cp')}</Text>
                  </View>
                  <Input
                    control={control}
                    name="cp"
                    align="left"
                    required
                    error={errors.cp ? errors.cp.message : null}
                    placeholder={`${customTraslate('cp')} ...`}
                    onInputBlur={handleInputBlur}
                    editable={currentField === 'cp'}
                  />
                </Col>
                <Col size={2}>
                  <Row style={Style.IconField}>
                    {currentField !== 'cp' && (
                      <TouchableHighlight onPress={() => handleCurrentField('cp')}>
                        <EditIcon width={40} height={40} />
                      </TouchableHighlight>
                    )}
                  </Row>
                </Col>
              </Grid>
            </ListItem>

            <ListItem style={Style.listItemsContainer}>
              <Grid style={Style.gridContainer}>
                <Col size={8}>
                  <View>
                    <Text style={Style.fieldTitle}>{customTraslate('localidad')}</Text>
                  </View>
                  <Input
                    control={control}
                    name="country_location"
                    align="left"
                    required
                    error={errors.country_location ? errors.country_location.message : null}
                    placeholder={`${customTraslate('localidad')} ...`}
                    onInputBlur={handleInputBlur}
                    editable={currentField === 'country_location'}
                  />
                </Col>
                <Col size={2}>
                  <Row style={Style.IconField}>
                    {currentField !== 'country_location' && (
                      <TouchableHighlight onPress={() => handleCurrentField('country_location')}>
                        <EditIcon width={40} height={40} />
                      </TouchableHighlight>
                    )}
                  </Row>
                </Col>
              </Grid>
            </ListItem>

            <ListItem style={Style.listItemsContainer}>
              <Grid style={Style.gridContainer}>
                <Col size={8}>
                  <View>
                    <Text style={Style.fieldTitle}>{customTraslate('pais')}</Text>
                  </View>
                  <Select
                    control={control}
                    name="country_id"
                    required
                    error={errors.country_id ? errors.country_id.message : null}
                  >
                    {countryList.map((country: ICountry) => (
                      <SelectPicker.Item
                        key={country.id}
                        value={country.id}
                        label={country.description}
                      />
                    ))}
                  </Select>
                </Col>
                <Col size={2}>
                  <Row style={Style.IconField}>
                    {currentField === 'country_id' ? (
                      <TouchableHighlight onPress={() => handleCurrentField('')}>
                        <Icon size={30} color="#989898" name="times-circle" />
                      </TouchableHighlight>
                    ) : (
                      <TouchableHighlight onPress={() => handleCurrentField('country_id')}>
                        <EditIcon width={40} height={40} />
                      </TouchableHighlight>
                    )}
                  </Row>
                </Col>
              </Grid>
            </ListItem>

            <ListItem style={Style.listItemsContainer}>
              <Grid style={Style.gridContainer}>
                <Col size={8}>
                  <View>
                    <Text style={Style.fieldTitle}>{customTraslate('correoElectronico')}</Text>
                  </View>
                  <Input
                    control={control}
                    name="username"
                    align="left"
                    required
                    error={errors.username ? errors.username.message : null}
                    placeholder={`${customTraslate('correoElectronico')} ...`}
                    onInputBlur={handleInputBlur}
                    editable={false}
                  />
                </Col>
                <Col size={2}>
                  <Row style={Style.IconField} />
                </Col>
              </Grid>
            </ListItem>

            <ListItem style={Style.listItemsContainer}>
              <Grid style={Style.gridContainer}>
                <Col size={8}>
                  <View>
                    <Text style={Style.fieldTitle}>{customTraslate('contrasena')}</Text>
                  </View>
                  <Input
                    control={control}
                    name="password"
                    align="left"
                    required
                    error={errors.password ? errors.password.message : null}
                    placeholder="********"
                    onInputBlur={handleInputBlur}
                    editable={currentField === 'password'}
                    secureText
                  />
                </Col>
                <Col size={2}>
                  <Row style={Style.IconField}>
                    {currentField !== 'password' && (
                      <TouchableHighlight onPress={() => handleCurrentField('password')}>
                        <EditIcon width={40} height={40} />
                      </TouchableHighlight>
                    )}
                  </Row>
                </Col>
              </Grid>
            </ListItem>
          </List>
        </ScrollView>
      </Content>
    </Container>
  );
}
