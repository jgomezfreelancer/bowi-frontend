import { StyleSheet, Dimensions } from 'react-native';
import { GREY } from '../../styles/colors';

const { width } = Dimensions.get('window');

export default StyleSheet.create({
  gridContainer: {
    width: '100%',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  listItemsContainer: {
    flexDirection: 'column',
  },
  description: {
    color: '#949494',
    textAlign: 'right',
  },
  rankingContainer: { flexDirection: 'column', justifyContent: 'center', marginTop: 10 },
  userContainer: {},
  userIconContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 8,
  },
  rankingCounter: {
    color: '#949494',
    fontSize: 10,
    marginBottom: -2,
  },
  buttonContainer: {
    position: 'relative',
    alignSelf: 'center',
    width: width - 70,
    marginTop: 20,
    marginBottom: 20
  },
  button: {
    borderRadius: 15,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: '#37c1cf',
    color: 'white',
    paddingLeft: '45%',
    overflow: 'hidden',
  },
  icon: {
    position: 'absolute',
    color: '#fff',
    bottom: '23%',
    left: '30%',
  },
  textField: {
    color: '#000000'
  },
  userListItemContainer: { justifyContent: 'center' },
  inputContainer: { justifyContent: 'flex-end' },
  imageProfile: {
    width: 60,
    height: 60,
    borderRadius: 50,
    marginBottom: 5
  },
  iconProfile: {
    marginBottom: 5,
    marginLeft: 16
  },
  listItemPictureContainer: { justifyContent: 'center', flexDirection: 'column', borderBottomWidth: 0 },
  descriptionField: { justifyContent: 'flex-end' },
  fieldTitle: { textAlign: 'left', color: GREY },
  IconField: { justifyContent: 'flex-end', alignItems: 'center' },
});
