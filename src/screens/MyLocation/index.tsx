import React, { useEffect, useRef, useState } from 'react';
import { Dimensions, View, StyleSheet, TouchableHighlight } from 'react-native';
import { Header } from 'native-base';
import MapView, { Circle } from 'react-native-maps';
import Geolocation, { GeoPosition } from 'react-native-geolocation-service';
import { Text } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Slider from '@react-native-community/slider';
import { useNavigation } from '@react-navigation/native';

import Styles from './style';

import GoogleSearchAutoComplete from '../../components/GoogleSearchAutoComplete';
import ListIcon from '../../assets//icons/icono-mapa-02.svg';

import ArrowIcon from '../../assets/icons/flecha.svg';

import {
  ILocation,
  setCamera,
  setLocation as customSetLocation,
  setMarkerLocation,
} from '../../reducers/location.reducer';
import { useDispatch, useSelector } from 'react-redux';
import { GooglePlaceData, GooglePlaceDetail } from 'react-native-google-places-autocomplete';
import { AppRootReducer } from '../../reducers';
import {
  getUsersByLocation,
  setUser,
  updateCoord,
  cleanUsersByLocation,
} from '../../reducers/login.reducer';
import { BLUE } from '../../styles/colors';
import { customTraslate } from '../../utils/language';

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;

const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const MyLocation = (): JSX.Element => {
  const [isRegion, setIsRegion] = useState<boolean>(false);
  const [isSlider, setIsSlider] = useState<boolean>(false);
  const [zoom, setZoom] = useState(0);
  const [isCustomSearch, setIsCustomSearch] = useState<boolean>(false);
  const [initialLocation, setInitialLocation] = useState({
    latitude: 0,
    longitude: 0,
    latitudeDelta: 0,
    longitudeDelta: 0,
  });
  const [km, setKm] = useState(1);
  const {
    locationReducer: { markerLocation },
    loginReducer: { user },
  } = useSelector((state: AppRootReducer) => state);
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const mapViewRef = useRef<any>();
  const markeRef = useRef<any>();

  const getZoom = () => {
    const dividerScaleValue = isCustomSearch ? 400 : 500;
    const currentKm = km * 1000;
    const radius = currentKm + currentKm / 2;
    const scale = radius / dividerScaleValue;
    const newZoom = 16 - Math.log(scale) / Math.log(2);
    setZoom(newZoom);
    mapViewRef.current.animateCamera({
      center: {
        latitude: markerLocation.latitude,
        longitude: markerLocation.longitude,
      },
      zoom: newZoom,
    });
  };

  useEffect(() => {
    Geolocation.getCurrentPosition(
      (res: GeoPosition) => {
        const {
          coords: { latitude, longitude },
        } = res;
        dispatch(
          setMarkerLocation({
            latitude,
            longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
          }),
        );
        setInitialLocation({
          latitude,
          longitude,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA,
        });
        const currentKm = 1000;
        const radius = currentKm + currentKm / 2;
        const scale = radius / 400;
        const newZoom = 16 - Math.log(scale) / Math.log(2);
        setZoom(newZoom);
        mapViewRef.current.animateCamera({
          center: {
            latitude,
            longitude,
          },
          zoom: newZoom,
        });
      },
      () => navigation.navigate('Location'),
      { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 },
    );
  }, [dispatch]);

  const onGoogleSearchChange = (_: GooglePlaceData, details: GooglePlaceDetail | null) => {
    if (details) {
      setIsCustomSearch(true);
      setKm(1);
      const newLocation = details.geometry.location;
      const currentKm = 1000;
      const radius = currentKm + currentKm / 2;
      const scale = radius / 400;
      const newZoom = 16 - Math.log(scale) / Math.log(2);
      dispatch(
        setMarkerLocation({
          latitude: newLocation.lat,
          longitude: newLocation.lng,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA,
        }),
      );
      setZoom(newZoom);
      mapViewRef.current.animateCamera({
        center: {
          latitude: newLocation.lat,
          longitude: newLocation.lng,
        },
        heading: 0,
        pitch: 10,
        zoom: newZoom,
      });
    }
  };

  const handleBack = async () => {
    navigation.navigate('Location');
    dispatch(
      setMarkerLocation({
        latitude: 0,
        longitude: 0,
        latitudeDelta: 0,
        longitudeDelta: 0,
      }),
    );
  };

  const handleRestore  = async () => {
    dispatch(
      Geolocation.getCurrentPosition(
        (res: GeoPosition) => {
          const {
            coords: { latitude, longitude },
          } = res;
          dispatch(
            setMarkerLocation({
              latitude,
              longitude,
              latitudeDelta: LATITUDE_DELTA,
              longitudeDelta: LONGITUDE_DELTA,
            }),
          );
          setInitialLocation({
            latitude,
            longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
          });
          const currentKm = 1000;
          const radius = currentKm + currentKm / 2;
          const scale = radius / 400;
          const newZoom = 16 - Math.log(scale) / Math.log(2);
          setZoom(newZoom);
          mapViewRef.current.animateCamera({
            center: {
              latitude,
              longitude,
            },
            zoom: newZoom,
          });
        },
        () => navigation.navigate('Location'),
        { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 },
      )
    );
  };

  const handleSearch = async () => {
    updateCoord(user.id!, {
      latitude: markerLocation.latitude,
      longitude: markerLocation.longitude,
    })(dispatch).then(() => {
      dispatch(customSetLocation(markerLocation, km));
      dispatch(setCamera(zoom));
      dispatch(getUsersByLocation(user.id!, km));
      dispatch(
        setMarkerLocation({
          latitude: 0,
          longitude: 0,
          latitudeDelta: 0,
          longitudeDelta: 0,
        }),
      );
      navigation.navigate('Location');
    });
  };

  const onSliderChange = (e: number) => {
    setKm(e);
  };

  useEffect(() => {
    getZoom();
  }, [km]);

  const getSliderTextValue = (): number => {
    return km >= 0 ? 10 : km >= 50 ? 80 : 0;
  };

  const value = getSliderTextValue();
  const left = (km * (width - 250)) / 120 - value;

  const onRegionChange = (e: ILocation) => {
    const { longitude, latitude } = e;
    if (isRegion) {
      setIsSlider(false);
      dispatch(
        setMarkerLocation({
          latitude,
          longitude,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA,
        }),
      );
    }
  };
  const onSLiderEnd = () => {
    setIsRegion(false);
  };

  const onSlidingStart = () => {
    setIsRegion(false);
    setIsSlider(true);
  };

  return (
    <View style={Styles.container}>
      <MapView
        ref={mapViewRef}
        style={{ ...StyleSheet.absoluteFillObject }}
        initialRegion={initialLocation}
        initialCamera={{
          center: {
            latitude: initialLocation.latitude,
            longitude: initialLocation.longitude,
          },
          heading: 0,
          pitch: 10,
          zoom: 18,
          altitude: 0,
        }}
        onRegionChangeComplete={onRegionChange}
        onTouchStart={() => setIsRegion(true)}
        onTouchEnd={() => setIsRegion(true)}
      >
        <Circle
          center={markerLocation}
          radius={km ? km * 1000 : 0}
          fillColor="rgba(116, 185, 255, 0.5)"
          strokeColor="#e0e0e0"
        />
      </MapView>
      <TouchableHighlight underlayColor="white" style={Styles.myLocation2} onPress={handleRestore}>
          <ListIcon width={45} height={45}/>
      </TouchableHighlight>
      <View style={Styles.fakeMarkerContainer}>
        <Text style={Styles.textUserLocation}>{user.first_name}</Text>
        <Icon style={Styles.fakeMarker} color="#f3452c" size={40} name="map-marker-alt" />
      </View>
      <View style={Styles.headerContainer}>
        <Header style={Styles.inputHeaderContainer}>
          <View style={Styles.itemTabContainer}>
            <View style={Styles.textCommentContainer}>
              <TouchableHighlight style={Styles.sendIconContainer}  underlayColor="transparent" onPress={handleBack}>
                <Text style={Styles.clipIconContainer}>
                  <ArrowIcon width={21} height={21}/>
                </Text>
              </TouchableHighlight>
              <View style={Styles.googleMapSearch}>
                <GoogleSearchAutoComplete onChange={onGoogleSearchChange} />
              </View>
              <TouchableHighlight style={Styles.sendIconContainer} underlayColor="transparent">
                <Text style={Styles.clipIconContainer}>
                  <Icon color="#b3b3b3" size={21} name="search" />
                </Text>
              </TouchableHighlight>
            </View>
          </View>
        </Header>
      </View>
      <View style={Styles.locationScope}>
        <View style={Styles.sliderContainer}>
          <Text style={Styles.sliderTitleText}>{customTraslate('buscarPorKm')} {customTraslate('km')}</Text>
          <View style={Styles.sliderContainer}>
            <Slider
              style={Styles.slider}
              step={1}
              thumbTintColor="#42c2cf"
              maximumValue={200}
              minimumValue={1}
              minimumTrackTintColor={BLUE}
              maximumTrackTintColor={BLUE}
              onValueChange={onSliderChange}
              onSlidingStart={onSlidingStart}
              onSlidingComplete={onSLiderEnd}
              value={km}
            />
            <Text style={{ ...Styles.sliderText, left }}>{Math.floor(km)} Km</Text>
          </View>
        </View>
        <TouchableHighlight onPress={handleSearch}>
          <Text style={Styles.searchButton}>{customTraslate('buscar')}</Text>
        </TouchableHighlight>
      </View>
    </View>
  );
};

export default MyLocation;
