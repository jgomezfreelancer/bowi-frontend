import { StyleSheet, Dimensions } from 'react-native';
import { isIOS } from '../../helpers/isIOS';
import { BLUE } from '../../styles/colors';
const { width } = Dimensions.get('window');

export default StyleSheet.create({
  container: { flex: 1 },
  textUserLocation: {
    color: '#ecf0f1',
    fontWeight: 'bold',
    borderColor: '#f3452c',
    borderWidth: 2,
    backgroundColor: '#f3452c',
    borderRadius: 10,
    textAlign: 'center',
    fontSize: 12,
    paddingLeft: 5,
    paddingRight: 5,
  },
  textUser: {
    color: '#ecf0f1',
    fontWeight: 'bold',
    borderColor: '#27ae60',
    borderWidth: 2,
    backgroundColor: '#27ae60',
    borderRadius: 10,
    textAlign: 'center',
    fontSize: 12,
    paddingLeft: 5,
    paddingRight: 5,
  },
  locationScope: {
    position: 'absolute',
    bottom: 0,
    alignSelf: 'center',
    width: '100%',
    backgroundColor: 'white',
    padding: 40,
    paddingTop: 20,
    paddingBottom: 20,
  },
  searchButton: {
    borderRadius: 30,
    paddingTop: 10,
    paddingBottom: 10,
    textAlign: 'center',
    backgroundColor: BLUE,
    color: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  icon: {
    position: 'absolute',
    color: '#fff',
    zIndex: 20,
    top: isIOS ? 110 : 112,
    left: 120,
    elevation: 5,
  },
  googleMapSearch: {
    borderWidth: 0,
    backgroundColor: 'white',
    color: 'black',
    borderColor: 'white',
    width: width - width / 2.8,
    bottom: 0,
  },
  myLocation: {
    position: 'absolute',
    backgroundColor: 'white',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    top: 20,
    left: 20,
    width: 60,
    height: 60,
    borderWidth: 2,
    borderRadius: 80,
    borderColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 12,
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,

    elevation: 24,
  },
  headerContainer: {
    position: 'absolute',
    width: '100%',
  },
  sliderContainer: {
    width: '100%',
    marginBottom: 5
  },
  sliderTitleText: {
    width: '35%',
    paddingTop: 2,
    paddingBottom: 2,
    borderRadius: 50,
    textAlign: 'center',
    color: '#7C7C7C',
    fontSize: 12,
    fontWeight: 'bold',
  },
  sliderText: {
    color: '#b4b4b4',
    fontSize: 14
  },
  slider: {
    width: '100%',
    height: 40,
    color: 'red',
    transform: [{ scaleX: 1.1 }, { scaleY: 1.1 }],
    marginBottom: -8
  },
  fakeMarkerContainer: {
    left: '50%',
    marginLeft: -23,
    position: 'absolute',
    top: '50%',
    marginTop: -77
  },
  fakeMarker: {
    height: 48,
    width: 48,
    borderRadius: 50,
  },
  leftIcon: {
    paddingLeft: 50,
    flex: 1,
  },
  vh1:{
    width: '100%',
    height: 50,
    backgroundColor: 'white',
    elevation: 0,
    borderBottomColor: '#c8c8c8',
    borderBottomWidth: 1,
    paddingLeft: 0,
    paddingRight: 0,
  },
  vh2:{
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  vh3:{
    flexDirection: 'row',
    flex: 1 
  },
  myLocation2: {
    position: 'relative',
    top: '60%',
    backgroundColor: 'white',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    left: '35%',
    width: 60,
    height: 60,
    borderWidth: 2,
    borderRadius: 80,
    borderColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 12,
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,

    elevation: 24,
  },
  inputHeaderContainer: {
    width: '100%',
    padding: 0,
    backgroundColor: 'white',
    marginBottom: 20,
    marginTop: 10,
    elevation: 0,
  },
  itemTabContainer: { backgroundColor: 'white', paddingLeft: 10, paddingRight: 10 },
  textCommentContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 15,
    paddingRight: 8,
    borderWidth: 1,
    borderColor: '#dddddd',
    borderRadius: 30,
    height: 50
  },
  sendIconContainer: { alignSelf: 'center', flexDirection: 'row' },
  clipIconContainer: {
    borderRadius: 50,
    paddingTop: 8,
    paddingBottom: 5,
    paddingLeft: 8,
    paddingRight: 15,
    textAlign: 'center',
  },
});
