import React, { useEffect, useState } from 'react';

import { Container, Grid, Row, Text, Header, Card } from 'native-base';
import { TouchableHighlight, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { useNavigation } from '@react-navigation/native';

import Styles from './style';

import ArrowIcon from '../../assets/icons/flecha.svg';
import InterestsIcon from '../../assets/icons/icono-inicio-05.svg';
import MultipleSelect from '../../components/common/MultipleSelect';
import MultipleInterestSelect from '../../components/common/MultipleInterestSelect';
import { useDispatch, useSelector } from 'react-redux';
import { AppRootReducer } from '../../reducers';
import { getAll } from '../../reducers/interest.reducer';
import { updateStarterSettings, updateStarterUser } from '../../reducers/login.reducer';
import { customTraslate } from '../../utils/language';

export default function ChooseInterest(): JSX.Element {
  const [interests, setInterests] = useState<number[]>([]);
  /**
   * mock
   */
  const [listMock] = useState<any[]>([
  {id:1,description:'Ocio',language:'ocio'},
  {id:2,description:'Conversación',language:'conversacion'},
  {id:3,description:'Laboral y educativo',language:'laboralYEducativo'},
  {id:4,description:'Animales',language:'animales'},
  {id:5,description:'Comunitaria',language:'comunitaria'},
  {id:6,description:'Covid-19',language:'covid19'},
  {id:7,description:'Diversidad funcional',language:'diversidadFuncional'},
  {id:8,description:'Desplazamiento',language:'desplazamiento'},
  {id:9,description:'Apoyo emocional',language:'apoyoEmocional'},
  {id:10,description:'Legal y administrativo',language:'legalYadministrativo'},
  {id:11,description:'Alojamiento',language:'alojamiento'},
  {id:12,description:'Familiar',language:'familiar'},
  {id:13,description:'Sanitario',language:'sanitario'},
  {id:14,description:'Pérdidas',language:'perdidas'},
  {id:15,description:'Otros',language:'otros'}]);
  const {
    interestReducer: { list },
    loginReducer: { 
      starterSettings,
      user: { id }  
    },
  } = useSelector((state: AppRootReducer) => state);
  const dispatch = useDispatch();
  const navigation = useNavigation();

  useEffect(() => {
    dispatch(getAll());
  }, [dispatch]);

  const handleChooseInterest = () => {
    dispatch(updateStarterSettings({ ...starterSettings, interests }));
    navigation.navigate('ChooseSettings');
  };

  const handleBack = () => {
    navigation.navigate('ChooseGender');
  };

  const handleDashboard = () => {
    const data = {
      location: 0,
      profile: 0,
      limit_distance: 50,
      first_time: false,
      languages: [],
      nationalities: [],
      nationality_id: null,
      interests: [],
    };
    dispatch(updateStarterUser(id!, data));
    navigation.navigate('TabNavigator');
  };

  const onChangeLanguages = (arrayList: number[]) => {
    setInterests(arrayList);
  };

  const getLabel = (object: { language: string }) => {
    return customTraslate(object.language);
  };

  return (
    <Container style={Styles.root}>
      <Header style={Styles.headerStyles}>
        <View style={Styles.backButtonContainer}>
          <TouchableHighlight onPress={handleBack} underlayColor="transparent">
            <ArrowIcon width={25} height={25}/>
          </TouchableHighlight>
        </View>
      </Header>
      <Grid>
        <Row size={5} style={Styles.rowIconContainer}>
          <View style={Styles.iconContainer}>
            <InterestsIcon width={39} height={39} />
          </View>
        </Row>
        <Row size={5} style={Styles.middleTitleTextContainer}>
          <Text style={Styles.middleText}>{customTraslate('tusIntereses')}</Text>
        </Row>
        <Row size={40} style={Styles.middleTextContainer}>
          <Card style={Styles.cardContainer}>
            <Text style={Styles.languagesTitle}>{customTraslate('seleccionaTusIntereses')}</Text>
            <MultipleInterestSelect
              onChange={onChangeLanguages}
              list={listMock}
              icon
              getLabel={getLabel}
              selectedList={
                starterSettings.interests && starterSettings.interests.length > 0
                  ? starterSettings.interests
                  : []
              }
            />
          </Card>
        </Row>
        <Row size={5} style={Styles.startButtonContainer}>
          <TouchableHighlight
            underlayColor="#007E86"
            style={Styles.startButton}
            onPress={handleChooseInterest}
          >
            <Text style={Styles.startButtonText}>{customTraslate('siguiente').toUpperCase()}</Text>
          </TouchableHighlight>
        </Row>
        <Row size={2} style={Styles.startButtonContainer}>
          <View style={Styles.iconStepsContainer}>
            <Icon size={14} color="#007E86" name="circle" solid />
            <Icon style={Styles.iconStepStyle} solid size={14} color="#007E86" name="circle" />
            <Icon size={14} color="#007E86" name="circle" solid />
            <Icon style={Styles.iconStepStyle} solid size={14} color="#007E86" name="circle" />
            <Icon size={14} color="white" name="circle" solid />
          </View>
        </Row>
        <Row size={5} style={Styles.startButtonContainer}>
          <Text style={Styles.introText} onPress={handleDashboard}>
            {customTraslate('omitirIntroduccion')}
          </Text>
        </Row>
      </Grid>
    </Container>
  );
}
