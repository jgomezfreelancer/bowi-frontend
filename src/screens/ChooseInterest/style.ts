import { StyleSheet } from 'react-native';
import { FONT } from '../../styles/colors';

export default StyleSheet.create({
  root: { backgroundColor: '#d7fcfe' },
  headerStyles: {
    backgroundColor: '#d7fcfe',
    elevation: 0,
    borderBottomColor: '#c8c8c8',
    paddingLeft: 10,
    paddingRight: 0,
  },
  backButtonContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  middleTitleTextContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 40,
    paddingRight: 40,
    paddingTop: 5,
    paddingBottom: 10
  },
  middleTextContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 40,
    paddingRight: 40,
  },
  middleText: { 
    textAlign: 'center', 
    color: '#007E86', 
    fontSize: 22, 
    fontWeight: '700',
    fontFamily: FONT
  },
  middleText2: {
    textAlign: 'center',
    color: '#007E86',
    fontSize: 16,
    fontStyle: 'italic',
  },
  startButtonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10,
    paddingLeft: 100,
    paddingRight: 100,
  },
  startButton: {
    borderRadius: 30,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    margin: 5,
    backgroundColor: '#007E86',
    borderColor: '#007E86',
    padding: 10,
  },
  startButtonText: { color: 'white', fontWeight: 'bold' },
  iconStepsContainer: { justifyContent: 'space-around', flexDirection: 'row' },
  iconStepStyle: {
    marginLeft: 10,
    marginRight: 10,
  },
  rowIconContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 81,
    height: 81,
    backgroundColor: '#ffffff',
    borderRadius: 81
  },
  languagesTitle: {
    fontSize: 14,
    marginTop: 15,
    padding: 10,
    paddingBottom: 10,
    paddingLeft: 0,
    marginLeft: '10%',
    fontWeight: '700',
    color: '#404040',
  },
  cardContainer: { 
    borderRadius: 8, 
    elevation: 0,
    width: '100%'
  },
  introText: {
  color: '#007E86',
  textDecorationLine: 'underline'
  }
});
