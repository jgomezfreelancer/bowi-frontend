import React from 'react';
import Modal from 'react-native-modal';
import { View } from 'react-native';

import Styles from './style';

interface Iprops {
  status: boolean;
  children: JSX.Element;
  handleStatus: (status: boolean) => void;
}

const BottomModal: React.FC<Iprops> = ({ status, children, handleStatus }): JSX.Element => {
  return (
    <View style={Styles.centeredRankingView}>
      <Modal
        backdropColor="#95a5a6"
        useNativeDriver
        isVisible={status}
        onBackdropPress={() => handleStatus(false)}
      >
        <View style={Styles.centeredRankingView}>
          <View style={Styles.modalView}>
            <View style={Styles.content}>
              <View style={Styles.dividerContainer}>
                <View style={Styles.divider} />
              </View>
              {children}
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
};

export { BottomModal };
