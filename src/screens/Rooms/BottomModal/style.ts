import { StyleSheet, Dimensions } from 'react-native';

const { width } = Dimensions.get('window');

export default StyleSheet.create({
  centeredRankingView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    alignSelf: 'center',
    bottom: -20,
    paddingLeft: 0,
  },
  content: {
    backgroundColor: 'white',
    padding: 20,
    flexWrap: 'nowrap',
    width,
    justifyContent: 'center',
    flexDirection: 'column',
    paddingTop: 5,
  },
  divider: {
    borderWidth: 2,
    width: '15%',
    borderColor: 'grey',
    borderRadius: 15,
  },
  dividerContainer: {
    width: '100%',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  modalView: {
    backgroundColor: 'white',
    padding: 10,
    paddingBottom: 0,
    alignItems: 'center',
    borderWidth: 2,
    borderTopColor: '#f1f1f1',
    borderBottomColor: 'white',
  },
  optionTitle: {
    fontSize: 16,
    margin: 10,
  },
});
