import React from 'react';

import { Grid, Col, ListItem } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { Image, Text, TouchableHighlight } from 'react-native';
import { IUser } from '../../reducers/login.reducer';
import { IUserRooms } from '../../reducers/room.reducer';
import Style from './style';
import { IMessage } from '../../reducers/message.reducer';
import { BLUE, GREY } from '../../styles/colors';

interface Iprops {
  handle: (room: IUserRooms, sender: IUser) => void;
  handleOption: (room: IUserRooms) => void;
  room: IUserRooms;
}

function ItemRoom(props: Iprops): JSX.Element {
  const { handle, room, handleOption } = props;
  const user: IUser = room.user;
  const lastMessage: IMessage = room.lastMessage;
  const counter = room.pendingViews;
  const userCirleColor = room.disable ? '#7f8c8d' : room.petition_id ? '#F34900' : BLUE;
  return (
    <ListItem style={Style.listItemRoomContainer}>
      <Grid style={Style.roomContainer}>
        {counter > 0 && <Text style={Style.counterPendingMessages}>{counter}</Text>}
        <Col size={2} style={Style.userIconContainer}>
          {room.userImage ? (
            <Image
              source={{ uri: room.userImage }}
              style={{ ...Style.imageProfile, borderColor: userCirleColor }}
            />
          ) : (
            <Icon size={40} color={userCirleColor} name="user-circle" />
          )}
        </Col>
        <Col size={9} style={Style.userContainer}>
          <Text style={Style.name} onPress={() => handle(room, user)}>
            {user.first_name} {user.last_name?.charAt(0)}
          </Text>
          <Text numberOfLines={1} style={Style.message}>
            {lastMessage && lastMessage.isMultimedia
              ? 'Photo'
              : lastMessage && lastMessage.description
              ? lastMessage.description
              : ''}
          </Text>
        </Col>
        {!room.disable && (
          <Col size={2} style={Style.optionContainer}>
            <TouchableHighlight
              onPress={() => handleOption(room)}
              underlayColor="transparent"
              style={Style.optionButton}
            >
              <Icon size={30} color={GREY} name="ellipsis-v" solid />
            </TouchableHighlight>
          </Col>
        )}
      </Grid>
    </ListItem>
  );
}

export default ItemRoom;
