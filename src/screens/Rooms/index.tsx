import { useNavigation } from '@react-navigation/native';
import { Container, Content, List, ListItem, Left, Right } from 'native-base';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome';

import CustomHeader from '../../components/common/Header';
import { AppRootReducer } from '../../reducers';
import {
  disablePetition,
  getServerDate,
  getUserPetition,
  IUser,
} from '../../reducers/login.reducer';
import { setNotificationIcon } from '../../reducers/notification.reducer';
import { getUserRooms, IUserRooms, setRoomOptionsModal } from '../../reducers/room.reducer';
import ItemRoom from './ItemRoom';
import { Text, View } from 'react-native';
import moment, { Moment } from 'moment';
import BottomModal from './BottomModal';
import { MainOption } from './MainOption/MainOoption';
import { customTraslate } from '../../utils/language';
import Styles from './style';

function Rooms(): JSX.Element {
  const [modalBottomCompoent, setModalBottomComponent] = useState<JSX.Element>(<MainOption />);
  const [counterTime, setCounterTime] = useState<Moment | null>(null);
  const navigation = useNavigation();
  const {
    loginReducer: {
      user: { id },
      serverDate,
      userPetition,
    },
    roomReducer: {
      userRooms,
      roomModalOptions: { statusModal },
    },
  } = useSelector((state: AppRootReducer) => state);
  const dispatch = useDispatch();
  const [counter, setCounter] = useState({
    hour: 0,
    minute: 0,
    second: 0,
  });

  useEffect(() => {
    dispatch(getUserRooms(id!));
    dispatch(setNotificationIcon(false));
    dispatch(getServerDate());
    dispatch(getUserPetition(id!));
  }, [dispatch, id]);

  const handleChat = (room: IUserRooms, sender: IUser) => {
    navigation.navigate('Chat', { room, sender });
  };

  useEffect(() => {
    if (Object.keys(userPetition).length > 0) {
      const expiration = moment(userPetition.created).add(1, 'hour');
      const expirationHour = expiration.format('HH:mm:ss');
      const serverHour = moment(serverDate).format('HH:mm:ss');
      const start = moment(expirationHour, 'HH:mm:ss');
      const end = moment(serverHour, 'HH:mm:ss');
      const startMinuteTime = moment.duration(start - end);
      const initialTime = `00:${startMinuteTime.minutes()}:${startMinuteTime.seconds()}`;
      setCounterTime(moment(`2020-01-01 ${initialTime}`));
    }
  }, [userPetition, serverDate]);

  useEffect(() => {
    if (Object.keys(userPetition).length > 0 && counterTime) {
      const interval = setInterval(() => {
        const valueCounter = counterTime;
        const petitionCounter = moment(valueCounter).subtract(1, 'second');
        const minute = petitionCounter.minute();
        setCounterTime(petitionCounter);
        setCounter({
          ...counter,
          hour: 0,
          minute: minute.toString().length === 1 ? `0${minute}` : minute,
          second: petitionCounter.second(),
        });
        if (petitionCounter.minute() === 0 && petitionCounter.second() === 0) {
          disablePetition(userPetition.id!)(dispatch)
            .then(() => {
              dispatch(getUserPetition(id!));
              setCounter({
                hour: 0,
                minute: 0,
                second: 0,
              });
              setCounterTime(null);
            })
            .catch(err => console.log(err));
          return () => clearInterval(interval);
        }
      }, 1000);
      return () => clearInterval(interval);
    }
  }, [counterTime, counter, userPetition]);

  const handlePetition = () => {
    navigation.navigate('PetitionDetail');
  };

  const handleOption = (optionRoom: IUserRooms) => {
    dispatch(
      setRoomOptionsModal({
        statusModal: true,
        selectedRoom: optionRoom,
      }),
    );
  };

  const handleStatusModal = (status: boolean) => {
    dispatch(
      setRoomOptionsModal({
        statusModal: status,
        selectedRoom: {},
      }),
    );
  };

  return (
    <Container>
      <CustomHeader title="Chats" />
      <Content>
        <View style={Styles.view} />
        {Object.keys(userPetition).length > 0 && (
          <List style={{ marginLeft: -15 }}>
            <ListItem style={Styles.listItemContainer} onPress={handlePetition}>
              <Left style={Styles.expirationIconContainer}>
                <Icon size={30} color="#f24a23" name="exclamation" />
                <Text style={Styles.expirationContainer}>
                  {counter.hour.toString().length > 1 ? counter.hour : `0${counter.hour}`}:
                  {counter.minute},<Text style={Styles.secondText}>{counter.second}</Text> {'\n'}
                  <Text style={Styles.activeRequestText}>
                    {customTraslate('peticionActiva').toUpperCase()}
                  </Text>
                </Text>
              </Left>
              <Right>
                <Icon size={20} color="#989898" name="chevron-right" />
              </Right>
            </ListItem>
          </List>
        )}
        <List>
          {userRooms.map((element: IUserRooms, i: number) => (
            <ItemRoom key={i} room={element} handle={handleChat} handleOption={handleOption} />
          ))}
        </List>
        <BottomModal
          handleStatus={handleStatusModal}
          children={modalBottomCompoent}
          status={statusModal}
        />
      </Content>
    </Container>
  );
}

export default Rooms;
