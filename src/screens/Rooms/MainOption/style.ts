import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  blockOptionsContainer: {
    justifyContent: 'space-around',
    flexDirection: 'row',
    marginBottom: 10,
  },
  blockOptionText: {
    padding: 10,
  },
  complaintContainer: { justifyContent: 'center', marginTop: 10 },
  complaintContainerResponse: { justifyContent: 'center', marginTop: 20, marginBottom: 80 },
  divider: {
    borderWidth: 1,
    width: '100%',
    borderColor: '#bdc3c7',
    borderRadius: 15,
  },
  dividerContainer: {
    width: '120%',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 10,
    marginLeft: -20,
  },
  optionTitle: {
    fontSize: 16,
    margin: 10,
  },
  optionComplaintTitle: { textAlign: 'center', fontWeight: 'bold', marginTop: 17 },
  optionBlockTitle: { textAlign: 'center', fontWeight: 'bold', marginTop: 17, marginBottom: 20 },
  optionComplaintText: {},
  title: { textAlign: 'center', fontWeight: 'bold' },
});
