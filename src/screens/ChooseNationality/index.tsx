import React, { useEffect, useState, useRef } from 'react';

import {
  Container,
  Grid,
  Row,
  Text,
  Header,
  Card,
  ListItem,
  Body,
  CheckBox,
  Content,
} from 'native-base';
import { TouchableHighlight, View, TextInput, Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { useNavigation } from '@react-navigation/native';

import Styles from './style';
import ArrowIcon from '../../assets/icons/flecha.svg';
import WorldIcon from '../../assets/icons/icono-inicio-03.svg';
import { useDispatch, useSelector } from 'react-redux';
import { AppRootReducer } from '../../reducers';
import { getAll } from '../../reducers/nationality.reducer';
import { updateStarterSettings, updateStarterUser } from '../../reducers/login.reducer';
//import FlagLogo from '../../components/FlagLogo';
import { BLUE } from '../../styles/colors';
import { customTraslate } from '../../utils/language';
import { ENV } from '../../../env';

export default function ChooseNationality(): JSX.Element {
  const [nationality, setNationality] = useState<number | null>(null);
  const {
    nationalityReducer: { list },
    loginReducer: { starterSettings,
      user: { id }  
    },
  } = useSelector((state: AppRootReducer) => state);
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const inputRef = useRef<TextInput | null>(null);
  const [filteredNationality, setFilteredNationality] = useState<any[]>([]);
  const [textValue, setTextValue] = useState<string>('');
  const condition = (item: string, value: string) => {return item.toLowerCase().includes(value.toLowerCase())};
  const getFilteredNationality = () => {
    return textValue.length > 0 ? filteredNationality : list;
  };

  useEffect(() => {
    dispatch(getAll());
  }, [dispatch]);

  const onFilterChange = (text: string) => {
    setTextValue(text);
    let filter = list.filter((item) => condition(item.description, text));
    setFilteredNationality(filter);
  };

  const handleChooseSettings = () => {
    dispatch(updateStarterSettings({ ...starterSettings, nationality_id: nationality! }));
    navigation.navigate('ChooseGender');
  };

  const handleBack = () => {
    navigation.navigate('ChooseLanguage');
  };

  const handleDashboard = () => {
    const data = {
      location: 0,
      profile: 0,
      limit_distance: 50,
      first_time: false,
      languages: [],
      nationalities: [],
      nationality_id: null,
      interests: [],
    };
    dispatch(updateStarterUser(id!, data));
    navigation.navigate('TabNavigator');
  };

  const onSelect = (id: number) => setNationality(id);

  const getList = (element: any, i: number) => {
    const current = nationality === element.id;
    const checked = !!current;
    //const nameFlag = 'samji_illustrator.jpeg';
    //const pathFlag = 'https://cdn.dribbble.com/users/3281732/profile/masthead_image/' + nameFlag;
    const pathFlag = ENV.server.url + '/api/v1/nationality/flags/' + element.image;
    return (
      <ListItem style={Styles.listItem} key={i} onPress={() => onSelect(element.id)}>
        <Image source={{ uri: pathFlag }} style={{ width: 40, height: 40 }} />
        {/* <FlagLogo name={element.slug} /> */}
        <Body>
          <Text style={Styles.listText}>{customTraslate(element.language)}</Text>
        </Body>
        <CheckBox
          style={Styles.checkbox}
          color={BLUE}
          checked={checked}
          onPress={() => onSelect(element.id)}
        />
      </ListItem>
    );
  };

  return (
    <Container style={Styles.root}>
      <Header style={Styles.headerStyles}>
        <View style={Styles.backButtonContainer}>
          <TouchableHighlight onPress={handleBack} underlayColor="transparent">
            <ArrowIcon width={25} height={25}/>
          </TouchableHighlight>
        </View>
      </Header>
      <Grid>
        <Row size={5} style={Styles.rowIconContainer}>
          <View style={Styles.iconContainer}>
            <WorldIcon width={50} height={50} />
          </View>
        </Row>
        <Row size={9} style={Styles.middleTextContainer}>
          <Text style={Styles.middleText}>{customTraslate('deDondeEres')}</Text>
        </Row>
     
        <Row size={35} style={Styles.middleTextContainer}>
          <Card style={Styles.cardContainer}>
            <Text style={Styles.selectNationalityTitle}>
              {customTraslate('seleccionaTuNacionalidad')}
            </Text>
            <TextInput
              ref={inputRef}
              onChangeText={onFilterChange}
              style={Styles.textArea}
              placeholder={`${customTraslate('buscar')}`}
            />
            <Content>
              {getFilteredNationality().length > 0 && getFilteredNationality().map(getList)}
            </Content>
          </Card>
        </Row>
        <Row size={8} style={Styles.startButtonContainer}>
          <TouchableHighlight
            underlayColor="#007E86"
            style={Styles.startButton}
            onPress={handleChooseSettings}
          >
            <Text style={Styles.startButtonText}> {customTraslate('siguiente')}</Text>
          </TouchableHighlight>
        </Row>
        <Row size={3} style={Styles.startButtonContainer}>
          <View style={Styles.iconStepsContainer}>
            <Icon size={14} color="#007E86" name="circle" solid />
            <Icon style={Styles.iconStepStyle} solid size={14} color="#007E86" name="circle" />
            <Icon size={14} color="white" name="circle" solid />
            <Icon style={Styles.iconStepStyle} solid size={14} color="white" name="circle" />
            <Icon size={14} color="white" name="circle" solid />
          </View>
        </Row>
        <Row size={5} style={Styles.startButtonContainer}>
          <Text style={Styles.introText} onPress={handleDashboard}>
            {customTraslate('omitirIntroduccion')}
          </Text>
        </Row>
      </Grid>
    </Container>
  );
}
