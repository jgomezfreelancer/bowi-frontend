import { useNavigation } from '@react-navigation/native';
import { Container } from 'native-base';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import CustomHeader from '../../components/common/Header';
import MultipleSelect from '../../components/common/MultipleSelect';
import { AppRootReducer } from '../../reducers';
import { updateLangNationalities } from '../../reducers/login.reducer';
import { getAll } from '../../reducers/language.reducer';
import { customTraslate } from '../../utils/language';

export default function Languages(): JSX.Element {
  const [selection, setSelection] = useState<number[]>([]);
  const [textValue, setTextValue] = useState<string>('');
  const condition = (item: string, value: string) => {return item.toLowerCase().includes(value.toLowerCase())};
  const [filteredLanguage, setFilteredLanguage] = useState<any[]>([]);
  const getFilteredLanguage = () => {
    return textValue.length > 0 ? filteredLanguage : list;
  };
  const {
    loginReducer: {
      user: { languages: selectedList, id, nationality_id },
    },
    languageReducer: { list },
  } = useSelector((state: AppRootReducer) => state);
  const dispatch = useDispatch();
  const navigation = useNavigation();

  useEffect(() => {
    dispatch(getAll());
  }, [dispatch]);

  const handleBack = async () => {
    if (selection.length > 0) {
      await dispatch(
        updateLangNationalities(id!, {
          languages: selection,
          nationalities: [],
          nationality_id: nationality_id!,
        }),
      );
    }
    navigation.navigate('Ajustes');
  };

  const onChange = (languageList: number[]) => {
    setSelection(languageList);
  };

  const onFilter = (val: string) => {
    setTextValue(val);
    let filter = list.filter((item) => condition(item.description, val));
    setFilteredLanguage(filter);
  };

  return (
    <Container>
      <CustomHeader
        leftIcon="arrow-left"
        leftColor="#989898"
        handleLeft={handleBack}
        title={customTraslate('idiomas')}
        isInput={true}
        onChangeText={onFilter}
      />
      <MultipleSelect
        onChange={onChange}
        list={getFilteredLanguage()}
        icon={false}
        selectedList={selectedList ? selectedList : []}
      />
    </Container>
  );
}
