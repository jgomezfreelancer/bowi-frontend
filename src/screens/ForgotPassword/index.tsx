import React from 'react';
import { Text, TouchableHighlight, View, ImageBackground } from 'react-native';
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Container, Header } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';

import Input from '../../components/common/form/Input2';
import Button from '../../components/common/form/Button';
import { useNavigation } from '@react-navigation/native';
import Styles from './style';
import { AppRootReducer } from '../../reducers';
import { forgotPassword } from '../../reducers/login.reducer';
import { customTraslate } from '../../utils/language';
// const backgroundImage = require('../../assets/images/fondo-regístrate-03.png');
import ArrowIcon from '../../assets/icons/flecha.svg';
import BowiLogoIcon from '../../assets/images/bowi-logo.svg';

type FormData = {
  username: string;
};

export type IFB = {
  email: string;
  first_name: string;
  last_name: string;
};

function ForgotPassword(): JSX.Element {
  const { handleSubmit, control, errors } = useForm<FormData>();
  const dispatch = useDispatch();
  const {
    loginReducer: { loading },
  } = useSelector((state: AppRootReducer) => state);
  const navigation = useNavigation();

  const onSubmit = async (form: FormData) => {
    dispatch(forgotPassword(form.username));
  };

  const handleBack = () => {
    navigation.navigate('Login');
  };

  return (
    <Container>
      <Header style={Styles.headerStyles}>
        <View style={Styles.backButtonContainer}>
          <TouchableHighlight onPress={handleBack} underlayColor="transparent">
            <ArrowIcon width={25} height={25}/>
          </TouchableHighlight>
        </View>
      </Header>
      {/* <ImageBackground resizeMode="stretch" source={backgroundImage} style={Styles.backgroundImage}>
            </ImageBackground> */}
        <View style={Styles.loginContainer}>
          <View style={Styles.logoContainer}>
            <BowiLogoIcon style={Styles.logo}/>
          </View>
          <View>
            <Text style={Styles.loginText}>{customTraslate('recuperarContrasena')}</Text>
          </View>
          <View style={Styles.fieldContainer}>
            <Input
              control={control}
              name="username"
              required
              error={errors.username ? errors.username.message : null}
              placeholder={customTraslate('correoElectronico')}
            />
          </View>
          <View style={Styles.fieldContainer}>
            <Button
              loading={loading}
              handleSubmit={handleSubmit(onSubmit)}
              title={customTraslate('enviar')}
            />
          </View>
          <View style={Styles.fieldContainer}>
            <Text style={Styles.infoText}>{customTraslate('recuperarContrasenaMensaje')}</Text>
          </View>
        </View>
    </Container>
  );
}

export default ForgotPassword;
