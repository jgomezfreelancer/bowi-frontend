import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  root: {
    width: '100%',
    padding: 20,
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
  },
  headerStyles: {
    backgroundColor: 'white',
    elevation: 0,
    borderBottomColor: '#c8c8c8',
    paddingLeft: 10,
    paddingRight: 0,
  },
  backButtonContainer: {
    paddingStart: 10,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  loginContainer: {
    justifyContent: 'center',
    paddingLeft: 50,
    paddingRight: 50,
  },
  fieldContainer: { marginTop: 15 },
  infoText: { color: '#9F9F9F', textAlign: 'center', fontSize: 11, fontWeight: '400' },
  loginText: { 
    fontSize: 17,
    color: '#9C9C9C', 
    fontWeight: '700', 
    textAlign: 'center', 
    paddingTop: 43,
    paddingBottom: 30,
  },
  backgroundImage: {
    width: '100%',
    flex: 1,
  },
  logoContainer: {
    width: '100%',
    justifyContent: 'center',
    flexDirection: 'row'
  },
  logo: {
    height: 100,
    width: 100,
  }
});
