import React, { FunctionComponent, useEffect, useState } from 'react';
import { View, Dimensions, ScrollView, Switch } from 'react-native';
import { Container, Content, Text, Left, List, ListItem, Grid, Right } from 'native-base';

import Styles from './style';
import Icon from 'react-native-vector-icons/FontAwesome';
import isEmpty from 'lodash/isEmpty';

import CustomHeader from '../../components/common/Header';
import { useDispatch, useSelector } from 'react-redux';
import { getUsersByLocation, logout, updateUserSettings } from '../../reducers/login.reducer';
import { AppRootReducer } from '../../reducers';
import { setCamera, setLocation as customSetLocation } from '../../reducers/location.reducer';
import { BLUE } from '../../styles/colors';
import { customTraslate } from '../../utils/language';

const { width } = Dimensions.get('screen');

type Props = {
  navigation: any;
};

type FormData = {
  location: boolean;
  profile: boolean;
};

const ChangeSettings: FunctionComponent<Props> = ({ navigation }) => {
  const [location, setLocation] = useState<boolean>(false);
  const [profile, setProfile] = useState<boolean>(false);
  const [sliderValue, setSliderValue] = useState(50);
  const dispatch = useDispatch();

  const {
    loginReducer: { user },
    locationReducer: { location: customLocation },
  } = useSelector((state: AppRootReducer) => state);

  useEffect(() => {
    if (!isEmpty(user)) {
      const { location, profile, limit_distance } = user;
      setProfile(profile === 1);
      setLocation(location === 1);
      if (limit_distance! > 0) {
        setSliderValue(limit_distance!);
      }
    }
  }, [user]);

  const handleLanguages = () => navigation.navigate('Languages');

  const getSliderTextValue = (): number => {
    return sliderValue >= 0 ? 130 : sliderValue >= 50 ? 80 : 0;
  };

  const value = getSliderTextValue();
  const left = (sliderValue * (width - 100)) / 100 - value;

  const onLocationChange = (valueLocation: boolean) => {
    setLocation(valueLocation);
    dispatch(updateUserSettings(user.id!, { location: valueLocation ? 1 : 0 }));
  };

  const handleBack = async () => {
    navigation.navigate('Ajustes');
  };

  const getZoom = () => {
    const currentKm = sliderValue * 1000;
    let zoom = 18;

    if (currentKm >= 1000 && currentKm <= 1999) zoom = 14;
    if (currentKm >= 2000 && currentKm <= 6999) zoom = 12;
    if (currentKm >= 7000 && currentKm <= 29999) zoom = 10;
    if (currentKm >= 30000 && currentKm <= 69999) zoom = 8;
    if (currentKm >= 70000) zoom = 8;
    dispatch(setCamera(zoom));
  };

  useEffect(() => {
    getZoom();
  }, [sliderValue]);

  return (
    <Container>
      <CustomHeader 
        title={customTraslate('configuraciones')}
        leftIcon="arrow-left"
        leftColor="#989898"
        handleLeft={handleBack}
        isInput={false}
      />
      <Content>
        <ScrollView>
          <List>
            <ListItem style={Styles.listItemsContainer}>
              <Grid style={Styles.gridContainer}>
                <Left>
                  <Text style={Styles.textField}>{customTraslate('ubicacion')}</Text>
                </Left>
                <Right>
                  <Switch
                    thumbColor="#fff"
                    trackColor={{ true: BLUE, false: 'grey' }}
                    onValueChange={onLocationChange}
                    value={location}
                  />
                </Right>
              </Grid>
              <Grid style={Styles.gridContainer}>
                <Text style={Styles.switchContainerText}>
                  {' '}
                  {customTraslate('ubicacionSubtitulo')}
                </Text>
              </Grid>
            </ListItem>
            <ListItem style={Styles.listItemsContainer} onPress={handleLanguages}>
              <Grid style={Styles.gridContainer}>
                <Left>
                  <Text style={Styles.textField}>{customTraslate('idioma')}</Text>
                </Left>
                <Right>
                  <Icon
                    style={Styles.settingIcon}
                    size={20}
                    color={'#989898'}
                    name="chevron-right"
                  />
                </Right>
              </Grid>
              <Grid style={Styles.gridContainer}>
                <Text style={Styles.switchContainerText}>
                  {' '}
                  {customTraslate('idiomaSubtitulo')}
                </Text>
              </Grid>
            </ListItem>
          </List>
        </ScrollView>
      </Content>
    </Container>
  );
};

export default ChangeSettings;
