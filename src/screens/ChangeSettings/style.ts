import { StyleSheet, Dimensions } from 'react-native';
import { BLUE, GREY } from '../../styles/colors';

const { width } = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    marginTop: 20,
  },
  textField: {
    color: '#000000',
  },
  gridContainer: {
    width: '100%',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  listItemsContainer: {
    flexDirection: 'column',
  },
  switchContainerText: {
    color: '#949494',
    fontSize: 13,
    width: '90%',
  },
  buttonContainer: {
    position: 'relative',
    alignSelf: 'center',
    width: width - 70,
    marginTop: 20,
  },
  button: {
    borderRadius: 15,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: BLUE,
    color: 'white',
    paddingLeft: '45%',
    overflow: 'hidden',
  },
  icon: {
    position: 'absolute',
    color: '#fff',
    bottom: '23%',
    left: '30%',
  },
  settingIcon: { marginRight: 10 },
  slider: {
    width: '100%',
    height: 40,
    color: 'red',
    transform: [{ scaleX: 1.1 }, { scaleY: 1.1 }],
    borderColor: 'blue',
  },
  sliderContainer: { width: '100%', marginLeft: -13, marginTop: 10 },
  sliderText: {
    width: 50,
    textAlign: 'center',
    color: BLUE,
    fontWeight: 'bold',
    marginTop: -5,
    fontSize: 14,
  },
  logoutText: {
    color: GREY,
    textDecorationLine: 'underline',
    textAlign: 'center',
    marginTop: 15,
    marginBottom: 15,
  },
  traslateIcon: { marginLeft: -5 },
});
