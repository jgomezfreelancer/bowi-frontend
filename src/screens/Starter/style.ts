import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  root: { backgroundColor: '#d7fcfe' },
  headerStyles: {
    backgroundColor: '#d7fcfe',
    elevation: 0,
    borderBottomColor: '#c8c8c8',
    paddingLeft: 10,
    paddingRight: 0,
  },
  backButtonContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  rowImageContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: '10%'
  },
  textImage: { textAlign: 'center' },
  middleTextContainer: {
    justifyContent: 'center',
    paddingLeft: 40,
    paddingRight: 40,
    paddingBottom: 10
  },
  middleText: { textAlign: 'center', color: '#007E86', fontSize: 20 },
  middleText2: {
    textAlign: 'center',
    color: '#007E86',
    fontSize: 20,
    fontWeight: 'bold',

  },
  startButtonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 100,
    paddingRight: 100,
  },
  startButton: {
    borderRadius: 30,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    margin: 5,
    backgroundColor: '#007E86',
    borderColor: '#007E86',
    padding: 10
  },
  startButtonText: { color: 'white', fontWeight: 'bold' },
  introText: {
    color: '#007E86',
    textDecorationLine: 'underline'
  },
  logo: {
    height: 200,
    width: 200,
  }
});
