import React from 'react';

import { Container, Grid, Row, Text, Header } from 'native-base';
import { TouchableHighlight, Dimensions,View } from 'react-native';

import Styles from './style';
import ArrowIcon from '../../assets/icons/flecha.svg';
import { useNavigation } from '@react-navigation/native';
import StarterImage from '../../assets/images/starter-image.svg';
import { useDispatch, useSelector } from 'react-redux';
import { AppRootReducer } from '../../reducers';
import { updateStarterUser } from '../../reducers/login.reducer';
import { customTraslate } from '../../utils/language';
import CustomHeader from '../../components/common/Header';

const { width } = Dimensions.get('screen');

export default function Starter(): JSX.Element {
  const navigation = useNavigation();
  const {
    loginReducer: {
      user: { id },
    },
  } = useSelector((state: AppRootReducer) => state);
  const dispatch = useDispatch();

  const handleChooseLanguage = () => {
    navigation.navigate('ChooseLanguage');
  };
  const sizeImage = width - 90;

  const handleBack = async () => {
    navigation.navigate('Login');
  };

  const handleDashboard = () => {
    const data = {
      location: 0,
      profile: 0,
      limit_distance: 50,
      first_time: false,
      languages: [],
      nationalities: [],
      nationality_id: null,
      interests: [],
    };
    dispatch(updateStarterUser(id!, data));
    navigation.navigate('TabNavigator');
  };

  return (
    <Container style={Styles.root}>
      <Header style={Styles.headerStyles}>
        <View style={Styles.backButtonContainer}>
          <TouchableHighlight onPress={handleBack} underlayColor="transparent">
            <ArrowIcon width={25} height={25}/>
          </TouchableHighlight>
        </View>
      </Header>
      <Grid>
        <Row size={25} style={Styles.rowImageContainer}>
          <Text style={Styles.textImage}>
            <StarterImage style={Styles.logo}/>
          </Text>
        </Row>
        <Row size={12} style={Styles.middleTextContainer}>
          <Text style={Styles.middleText}>
            <Text style={Styles.middleText2}>{customTraslate('pideAyuda')}</Text>
            {'\n'}{customTraslate('starterTitulo1')}
          </Text>
        </Row>
        <Row size={10} style={Styles.middleTextContainer}>
          <Text style={Styles.middleText}>
            <Text style={Styles.middleText2}>{customTraslate('starterTitulo2')}&nbsp;</Text>
            {customTraslate('starterTitulo3')}
          </Text>
        </Row>
        <Row size={10} style={Styles.startButtonContainer}>
          <TouchableHighlight
            underlayColor="#007E86"
            style={Styles.startButton}
            onPress={() => handleChooseLanguage()}
          >
            <Text style={Styles.startButtonText}>¡{customTraslate('empezar')}!</Text>
          </TouchableHighlight>
        </Row>
        <Row size={10} style={Styles.startButtonContainer}>
          <Text style={Styles.introText} onPress={handleDashboard}>
            {customTraslate('omitirIntroduccion')}
          </Text>
        </Row>
      </Grid>
    </Container>
  );
}
