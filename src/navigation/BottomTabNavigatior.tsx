import React from 'react';
import {
  BottomTabNavigationOptions,
  createBottomTabNavigator,
} from '@react-navigation/bottom-tabs';
// import Icon from 'react-native-vector-icons/FontAwesome';  
import Settings from '../screens/Settings';
import Location from '../screens/Location/Location';
import Rooms from '../screens/Rooms';
import { useSelector } from 'react-redux';
import { AppRootReducer } from '../reducers';
import MessageIconActive from '../assets/icons/icono-menu-04.svg';
import MessageIconInactive from '../assets/icons/icono-menu-07.svg';

import LocationIconActive from '../assets/icons/icono-menu-03.svg';
import LocationIconInactive from '../assets/icons/icono-menu-06.svg';

import AjustesIconActive from '../assets/icons/icono-menu-02.svg';
import AjustesIconInactive from '../assets/icons/icono-menu-05.svg';

import { BLUE } from '../styles/colors';

const Tab = createBottomTabNavigator();

function Tabs(): JSX.Element {
  const {
    notificationReducer: { statusIconNotification },
  } = useSelector((state: AppRootReducer) => state);

  const getRoomOptions = () => {
    const options: BottomTabNavigationOptions = {
      title: 'Chat',
      tabBarIcon: ({ color }) => {
        const MessageIcon = color === BLUE ? MessageIconActive : MessageIconInactive;
        return <MessageIcon width={30} height={30} />;
      },
    };
    if (statusIconNotification) {
      options.tabBarBadge = '';
    }
    return options;
  };

  const getLocationOptions = () => {
    const options: BottomTabNavigationOptions = {
      title: 'Location',
      tabBarIcon: ({ color }) => {
        const MessageIcon = color === BLUE ? LocationIconActive : LocationIconInactive;
        return <MessageIcon width={30} height={30} />;
      },
    };
    return options;
  };

  const getAjustesOptions = () => {
    const options: BottomTabNavigationOptions = {
      title: 'Ajustes',
      tabBarIcon: ({ color }) => {
        const MessageIcon = color === BLUE ? AjustesIconActive : AjustesIconInactive;
        return <MessageIcon width={30} height={30} />;
      },
    };
    return options;
  };

  return (
    <Tab.Navigator
      initialRouteName="Location"
      tabBarOptions={{
        showLabel: false,
        activeTintColor: BLUE,
      }}
    >
      <Tab.Screen
        options={getAjustesOptions()}
        name="Ajustes"
        component={Settings}
      />
      <Tab.Screen
        name="Location"
        options={getLocationOptions()}
        component={Location}
      />
      <Tab.Screen name="Rooms" options={getRoomOptions()} component={Rooms} />
    </Tab.Navigator>
  );
}

function BottomTabNavigator(): JSX.Element {
  return <Tabs />;
}

export default BottomTabNavigator;
