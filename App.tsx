import React from 'react';
import { Provider } from 'react-redux';
import firebase from '@react-native-firebase/app';

import CreateStore from './src/config/store';
import { Route } from './src/config/Route';
import { ENV } from './env';

const store = CreateStore();

const App = () => {
  return (
    <Provider store={store}>
      <Route />
    </Provider>
  );
};

export default App;
