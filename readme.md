# React Native App

## Features

- ReactJS-Typescript with Hooks
- Interfaces and Types
- Auth Login with API REST
- Forms validations with Hooks
- Redux and Redux-Thunk


## Install
In the project directory, you can run:
```sh
yarn 
```
## Environment

### copy env and set the values according your needs
### execute command:
```sh
cp env-example.ts env.ts
```

## Run Android
Open android studio and wait for the sync process of gradle dependencies.
In the project directory, you can run:
```sh
yarn android && yarn start 
```

## Run IOS
Have the [cocoapods][cocoapods] installed.
In the project (ios path) directory run:
```sh
cd ios
pod deintegrate
pod install
```

In the project (root path) directory run:
```sh
yarn ios && yarn start
```

Or you can directly from Xcode run build on simulator. (Optional)

[//]: #
   [cocoapods]: <https://cocoapods.org/>